/************************************************************************************************
* @file Main.cpp
* Test driver entry point.
*/

#include "windows.h"
//#include "FreeImage.h"
#include "stdio.h"

//#include "../Common/Sobel_Test_Color_Weighted_Gray_3x3_2.h"
//#include "../Common/ColorSpace_To_Grayscale_2.h"
#include "../Common/Subimage_enhance.h"


/* Macro to suppress unreferenced variable warning */
#define UNREFERENCED_VARIABLE(var) var = var

int main(int argc, char **argv) {
	UNREFERENCED_VARIABLE(argc);
	UNREFERENCED_VARIABLE(argv);

	int
		nRes;

	//const char inputFileName[] = "../Common/ButterFly_Test_Image.jpg";
	//const char inputFileName[] = "../Common/01-203-0-RMLO_Neg_c_abX_c_TX.png";
	//const char inputFileName[] = "../Common/01-001-0-RMLO_c.jpg";
	//const char inputFileName[] = "../Common/0010001_2.jpg";
	const char inputFileName[] = "../Common/Mouse8_post120mins_scan1_26kV_Ag_Orig.png";


	//PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 sParameters_Sobel_3x3_With_Preprocessing_1;
	PARAMETERS_IMAGE_PREPROCESSING_1 sParameters_Image_Preprocessing_1;

	Image image_ForGrayToGray_in;
	image_ForGrayToGray_in.read(inputFileName);

	Image image_ForGrayToGray_out;

	nRes = doImage_Preprocessing_1(

		image_ForGrayToGray_in, // Image& image_in,, //const char *inputFileNamef,

		&sParameters_Image_Preprocessing_1, // 

		image_ForGrayToGray_out); // Image& image_out);

	//printf("\n Debug 1");

	if (nRes == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An UNSUCCESSFUL_RETURN from 'doImage_Preprocessing_1': please press any key"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n A SUCCESSFUL_RETURN from 'doImage_Preprocessing_1': please press any key"); getchar();
		printf("\n\n A SUCCESSFUL_RETURN from 'doImage_Preprocessing_1': ");

#endif // #ifndef COMMENT_OUT_ALL_PRINTS
	}//

//////////////////////////////////////////////////////////////

	//bool status = image_out.write("../Common/01-203-0-RMLO_Neg_c_abX_c_TX_Gray.png");
	//bool status = image_out.write("../Common/01-001-0-RMLO_c_Mask.png");
	//bool status = image_out.write("../Common/0010001_2_Mask.png");

	bool status = image_ForGrayToGray_out.write("../Common/Mouse_UnsharpMask_Rational_Gray.png");

	if (status)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Writing the processed image: a SUCCESSFUL_RETURN; please press any key"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		//return SUCCESSFUL_RETURN;
	}
	else {
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Writing the processed image: an UNSUCCESSFUL_RETURN; please press any key"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		return UNSUCCESSFUL_RETURN;
	}//else {

	Image image_ForFiltering_in; // image_ForGrayToGray_out
		//image_ForFiltering_in.read("ButterFly_Test_Image.jpg");

	image_ForFiltering_in = image_ForGrayToGray_out;

	Image image_ForFiltering_out;

	PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 sParameters_Sobel_3x3_With_Preprocessing_1;

	//printf("\n Debug 2");

	nRes = doSobel_3x3_With_Preprocessing_1(

		image_ForFiltering_in, // Image& image_in,, //const char *inputFileNamef,
		//image_ForGrayToGray_out, // Image& image_in, //const char *inputFileNamef,

		sParameters_Sobel_3x3_With_Preprocessing_1, // const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 *sParameters_Sobel_3x3_With_Preprocessing_1f); // const float fSE_Weight_Bluef)

		image_ForFiltering_out); // Image& image_out);

	//printf("\n Debug 3");

/*
//2nd iteration for doSobel_3x3_With_Preprocessing_1
	image_ForFiltering_in = image_ForFiltering_out;
	nRes = doSobel_3x3_With_Preprocessing_1(

		image_ForFiltering_in, // Image& image_in,, //const char *inputFileNamef,
		//image_ForGrayToGray_out, // Image& image_in, //const char *inputFileNamef,

		sParameters_Sobel_3x3_With_Preprocessing_1, // const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 *sParameters_Sobel_3x3_With_Preprocessing_1f); // const float fSE_Weight_Bluef)

		image_ForFiltering_out); // Image& image_out);
*/
	if (nRes == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An UNSUCCESSFUL_RETURN in 'doSobel_3x3_With_Preprocessing_1'; please press any key"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nRes == UNSUCCESSFUL_RETURN)

	//image_ForFiltering_out.write("DogLegTornACL_SobelProcessed.png");

	//image_ForFiltering_out.write("./The_mouse/Mouse8_post120mins_scan1_26kV_Ag_Sobel_Proc.png");
	image_ForFiltering_out.write("../Common/Mouse_Ration_Sobel_Proc.png");
	
	Image image_AoI_AfterFiltering_out;
	
	if (ADDITIONAL_AOI_OUTPUT_FOR_FILTER == true)
	{
		nRes = AoI_From_AnImage(

			image_ForFiltering_out, //const Image& image_in,

			nWid_AoI_UpperLeft, //int nWid_AoI_UpperLeftf, //
			nLen_AoI_UpperLeft, //int nLen_AoI_UpperLeftf,

			nWid_AoI_BottomRight, //int nWid_AoI_BottomRightf,
			nLen_AoI_BottomRight, //int nLen_AoI_BottomRightf,

			image_AoI_AfterFiltering_out); // Image& AoI_image_out);


		if (nRes == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An UNSUCCESSFUL_RETURN in 'AoI_From_AnImage'; please press any key"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nRes == UNSUCCESSFUL_RETURN)

		//image_AoI_AfterFiltering_out.write("./Images_Torn_Ligaments/Sophie/Sophie_ACL Rupture_SobelProc_AoI.png");

		image_AoI_AfterFiltering_out.write("../Common/Mouse_Ration_Sobel_Proc_AoI.png");
		//image_AoI_AfterFiltering_out.write("Sophie_ACL Rupture_SobelProc_AoI.png");

	}//if (ADDITIONAL_AOI_OUTPUT_FOR_FILTER == true)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After the Sobel filter: the grayscale image has been saved");
	printf("\n\n Please press any key to exit"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	system("pause");
	return 0;
}