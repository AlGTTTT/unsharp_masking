

//#include "Removing_Labels_Fr_Color_Images_2.h"
#include "Subimage_enhance.h"

using namespace imago;

int
	nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob = 0, // no printing
	iIterGlob,
	nWidGlob;

FILE *fout0;

float
	fL_Min_Glob = fLarge,
	fL_Max_Glob = -fLarge,

	fA_Min_Glob = fLarge,
	fA_Max_Glob = -fLarge,

	fB_Min_Glob = fLarge,
	fB_Max_Glob = -fLarge;

int compare(const void * a, const void * b)
{
	return (*(int*)a - *(int*)b);
}// int compare(const void * a, const void * b)

int doSobel_3x3_With_Preprocessing_1(
	const Image& image_in,

	const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,

	Image& image_out)
{

	int Initializing_GRAYSCALE_To_CurSize(
		const int nWidthImagef,
		const int nLenImagef,

		GRAYSCALE_IMAGE *sGRAYSCALE_Imagef);

	int Histogram_Statistics_For_Orig_Image(

		GRAYSCALE_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

		int &nObjectAreaf,
		float &fPercentageOfobjectAreaf,
		int nHistogramArrf[], //[nNumOfHistogramBinsStat]
		float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

	void Sobel_Filtering_3x3_With_Preprocessing_1(
		const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,
		const GRAYSCALE_IMAGE *sOne_Orig_Imagef,
		GRAYSCALE_IMAGE *sOne_Filtered_Imagef);

	int HistogramEqualization(

		GRAYSCALE_IMAGE *sGrayscale_Image);

	void Copying_Grayscale_To_Grayscale(

		const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
		GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);

	int MedianFilter_Image(

		const int nRadius_MedianFilterf,
		const int nDimOfArr_MedianFilterf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		GRAYSCALE_IMAGE *sGrayscale_ImageByMedianFilterf);

	int
		iWid,
		iLen,

		//////////////////////
		nWidthImage, // is actually 'nLenImage' comparing with the "Cimage" implementation.
		nLenImage,

		nObjectArea,
		nHistogramArr[nNumOfHistogramBinsStat], //[nNumOfHistogramBinsStat]

#ifndef COMMENT_OUT_ALL_PRINTS
		iHistogramBin,
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		nDiffOfIntensityAndThreshf,

		nIntensityBeforeExpf, 

		iIters_ForMedianFilterf,

		nIndexCurSizef,
		nNumOfPixelsAboveIntensityThreshold = 0,
		nRes;

	float
		fSumOfPercentsf,
		fDeviationOfSumFrom_100percentsf,

		fExpOfDiff,

		fPercentageOfobjectArea,
		fPercentagesInHistogramArr[nNumOfHistogramBinsStat]; //[nNumOfHistogramBinsStat]

#ifndef COMMENT_OUT_ALL_PRINTS
/*
	fout0 = fopen("wMain_SubimageEnhancenent.txt", "w");
	if (fout0 == NULL)
	{
		printf("\n\n fout0 == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout0 == NULL)
*/
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	//printf("\n Debug 2_1");

//////////////////////////////////////////////////////////////////////////

	fSumOfPercentsf = sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Redf + sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Greenf + sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Bluef;
	fDeviationOfSumFrom_100percentsf = (float)(100.0) - fSumOfPercentsf;

	if (fDeviationOfSumFrom_100percentsf < 0.0)
		fDeviationOfSumFrom_100percentsf = -fDeviationOfSumFrom_100percentsf;

	if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error: the sum of the color weights deviates too much from 100 percents");
		printf("\n\n Please adjust the color weights");

		fprintf(fout0, "\n\n An error: the sum of the color weights deviates too much from 100 percents");
		fprintf(fout0, "\n\n Please adjust the color weights");

		//printf("\n\n Please press any key to exit:"); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)
 ///////////////////////////////////////////////////////////////////////////

	// size of image
	nLenImage = image_in.width();
	nWidthImage = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n nWidthImage = %d, nLenImage = %d", nWidthImage, nLenImage);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	if (nLenImage > nLenMax || nLenImage < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image width: nLenImage = %d", nLenImage);
		fprintf(fout0, "\n\n An error in reading the image width: nLenImage = %d", nLenImage);
		//printf("\n\n Please press any key to exit");getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
		//getchar(); exit(1);
	} //if (nLenImage > nLenMax || nLenImage < nLenMin)

	if (nWidthImage > nWidMax || nWidthImage < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nWidthImage = %d", nWidthImage);
		fprintf(fout0, "\n\n An error in reading the image height: nWidthImage = %d", nWidthImage);

		//printf("\n\n Please press any key to exit"); ; getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nWidthImage > nWidMax || nWidthImage < nWidMin)

	//printf("\n\n Please press any key:"); getchar();
	//printf("\n Debug 2_2");

	//////////////////////////////////////////////////////////////////////////

	GRAYSCALE_IMAGE
		sOne_Orig_Image_WeightedGray;
	
	nRes = Initializing_GRAYSCALE_To_CurSize(
		nWidthImage, //const int nWidthImagef,
		nLenImage, //const int nLenImagef,

		&sOne_Orig_Image_WeightedGray); // GRAYSCALE_IMAGE *sOne_Orig_Imagef);	//[]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
//the arrays have not been initialized
		//delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
		//delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

		return UNSUCCESSFUL_RETURN;
	}//if (nRes == UNSUCCESSFUL_RETURN)
	// use 'nIntensityThreshold' to change the number of object pixels!

	int
		nIntensity_Read_Test_ImageMax = -nLarge,
		nRed,
		nGreen,
		nBlue;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout0, "\n\n Input data in 'doSobel_3x3_With_Preprocessing_1'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (iWid = 0; iWid < nWidthImage; iWid++) //j
	{
		for (iLen = 0; iLen < nLenImage; iLen++) //i
		{
			nIndexCurSizef = iLen + iWid*nLenImage;

			nRed = image_in(iWid, iLen, R);
			nGreen = image_in(iWid, iLen, G);
			nBlue = image_in(iWid, iLen, B);

			if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iWid = %d, iLen = %d", nRed, nGreen, nBlue, iWid, iLen);
				fprintf(fout0, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iWid = %d, iLen = %d", nRed, nGreen, nBlue, iWid, iLen);
				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
				delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 1: nIndexCurSizef = %d, iLen = %d, iWid = %d, nRed = %d", nIndexCurSizef, iLen, iWid, nRed);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		} // for (iLen = 0; iLen < nLenImage; iLen++)

	}//for (iWid = 0; iWid < nWidthImage; iWid++)

	//printf("\n Debug 2_3");

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout0, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (iWid = 0; iWid < nWidthImage; iWid++) //j
	{
		for (iLen = 0; iLen < nLenImage; iLen++) //i
		{
			nIndexCurSizef = iLen + iWid * nLenImage;

			nRed = image_in(iWid, iLen, R);
			nGreen = image_in(iWid, iLen, G);
			nBlue = image_in(iWid, iLen, B);

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				nRed = nRed / 256;

				nGreen = nGreen / 256;
				nBlue = nBlue / 256;
			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)

			  //Converting to decimals
			sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef] = (int)(sParameters_Sobel_3x3_With_Preprocessing_1f.fFadingFactorf*(sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Redf*(float)(nRed)+
				sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Greenf*(float)(nGreen)+
				sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_Bluef*(float)(nBlue)) / 100.0);

			if (sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef] > nIntensityStatMax)
			{
				sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;

			} //if (sOne_Orig_Image_Red.nGrayScale_Arr[nIndexMaxSize] > nIntensityStatMin)

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef] > nIntensityStatMin)
			{
				nNumOfPixelsAboveIntensityThreshold += 1;
				//fprintf(fout0, "\n\n A new nNumOfPixelsAboveIntensityThreshold = %d, iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[%d] = %d", 
					//nNumOfPixelsAboveIntensityThreshold,iWid, iLen, nIndexMaxSize, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexMaxSize]);

			} //if (sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexMaxSize] > nIntensityStatMin)

#ifndef COMMENT_OUT_ALL_PRINTS
#ifdef PRINTING_SUBIMAGE
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout0, "\n\n An initial subimage iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[%d] = %d",
					iWid, iLen, nIndexCurSizef, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef]);

				fprintf(fout0, "\n nRed = %d, nGreen = %d, nBlue = %d, Gray = %d", nRed, nGreen, nBlue, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
#endif // #ifdef PRINTING_SUBIMAGE
#endif // #ifndef COMMENT_OUT_ALL_PRINTS


#ifndef COMMENT_OUT_ALL_PRINTS

			//fprintf(fout0, "\n\n 1: iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[%d] = %d",
				//iWid, iLen, nIndexCurSizef, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef]);

			//fprintf(fout0, "\n nRed = %d, nGreen = %d, nBlue = %d, Gray = %d", nRed, nGreen, nBlue, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef]); 
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		} // for (iLen = 0; iLen < nLenImage; iLen++) //i

	}//for (iWid = 0; iWid < nWidthImage; iWid++) //j

	//printf("\n Debug 2_4");

	 ///////////////////////////////////////////////////////////////////////////

	//printf("\n\n After reading the image: please press any key to continue"); fflush(fout0);  getchar();

	///////////////////////////////////
	nRes = Histogram_Statistics_For_Orig_Image(
		&sOne_Orig_Image_WeightedGray, // GRAYSCALE_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

		nObjectArea, //int &nObjectAreaf,

		fPercentageOfobjectArea, //float &fPercentageOfobjectAreaf,
		nHistogramArr, //int nHistogramArrf[], //[nNumOfHistogramBinsStat]
		fPercentagesInHistogramArr); // float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

	//printf("\n Debug 2_4_1");

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
		delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;
		
		return UNSUCCESSFUL_RETURN;
	}//if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n Gray conversion inside 'Sobel_Filtering': nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);
	fprintf(fout0, "\n\n Gray conversion: nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)
	{
		printf("\n Gray inside 'Sobel_Filtering': nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);

		fprintf(fout0, "\n Gray inside 'Sobel_Filtering': nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);
	} //for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)

	//printf("\n\n After 'Histogram_Statistics_For_Orig_Image()' Gray conversion: please press any key"); fflush(fout0); getchar(); //exit(1);

	printf("\n\n After 'Histogram_Statistics_For_Orig_Image()' Gray conversion"); fflush(fout0);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	//printf("\n Debug 2_5");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	GRAYSCALE_IMAGE sOne_Filtered_Image;

	nRes = Initializing_GRAYSCALE_To_CurSize(
		nWidthImage, //const int nWidthImagef,
		nLenImage, //const int nLenImagef,

		&sOne_Filtered_Image); // GRAYSCALE_IMAGE *sOne_Orig_Imagef);	//[]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
		delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

		//delete[] sOne_Filtered_Image.nGrayScale_Arr;
		//delete[] sOne_Filtered_Image.nPixel_ValidOrNotArr;

		return UNSUCCESSFUL_RETURN;
	}//if (nRes == UNSUCCESSFUL_RETURN)

	GRAYSCALE_IMAGE sGrayscale_ImageByMedianFilter;

	nRes = Initializing_GRAYSCALE_To_CurSize(
		nWidthImage, //const int nWidthImagef,
		nLenImage, //const int nLenImagef,

		&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sOne_Orig_Imagef);	//[]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
		delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

		delete[] sOne_Filtered_Image.nGrayScale_Arr;
		delete[] sOne_Filtered_Image.nPixel_ValidOrNotArr;

		//delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
		//delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

		return UNSUCCESSFUL_RETURN;
	}//if (nRes == UNSUCCESSFUL_RETURN)

	//printf("\n Debug 2_6");

///////////////////////////////////////////////////////////////////////////////
	Sobel_Filtering_3x3_With_Preprocessing_1(
			sParameters_Sobel_3x3_With_Preprocessing_1f, //const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,

		&sOne_Orig_Image_WeightedGray, //const GRAYSCALE_IMAGE *sOne_Orig_Imagef,
		&sOne_Filtered_Image); // GRAYSCALE_IMAGE *sOne_Filtered_Image)

	//printf("\n Debug 2_7");

	if (sParameters_Sobel_3x3_With_Preprocessing_1f.bHIST_EQUAL_AFTER_SOBELf == true)
	{
		nRes = HistogramEqualization(

			&sOne_Filtered_Image); // GRAYSCALE_IMAGE *sGrayscale_Image);

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
			delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

			delete[] sOne_Filtered_Image.nGrayScale_Arr;
			delete[] sOne_Filtered_Image.nPixel_ValidOrNotArr;

			delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
			delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf( "\n\n An error in 'HistogramEqualization' after 'Sobel_Filtering_3x3_With_Preprocessing_1'");
			fprintf(fout0, "\n\n An error in 'HistogramEqualization' after 'Sobel_Filtering_3x3_With_Preprocessing_1'");

			//printf("\n\n Please press any key:"); getchar();

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nRes == UNSUCCESSFUL_RETURN)

	} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bHIST_EQUAL_AFTER_SOBELf == true)

	//printf("\n Debug 2_8");

	if (sParameters_Sobel_3x3_With_Preprocessing_1f.bIS_MEDIAN_FILTER_TO_FILTERED_IMAGE_INCLUDEDf == true)
	{
		/*
							Copying_Grayscale_To_Grayscale(

								//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

								&sGrayImage_ByUnsharpMaskFin, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

								&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);
				*/

		for (iIters_ForMedianFilterf = 0; iIters_ForMedianFilterf < sParameters_Sobel_3x3_With_Preprocessing_1f.nNumofIters_ForMedianFilter_After_Filteringf; iIters_ForMedianFilterf++)
		{
			nRes = MedianFilter_Image(

				sParameters_Sobel_3x3_With_Preprocessing_1f.nRadius_MedianFilterf, //const int nRadius_MedianFilterf,
				sParameters_Sobel_3x3_With_Preprocessing_1f.nDimOfArr_MedianFilterf, //const int nDimOfArr_MedianFilterf,

				//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
				&sOne_Filtered_Image, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,

				&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sGrayscale_ImageByMedianFilterf);

			if (nRes == UNSUCCESSFUL_RETURN)
			{
				delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
				delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

				delete[] sOne_Filtered_Image.nGrayScale_Arr;
				delete[] sOne_Filtered_Image.nPixel_ValidOrNotArr;

				delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
				delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
			}//if (nRes == UNSUCCESSFUL_RETURN)

			Copying_Grayscale_To_Grayscale(

				&sGrayscale_ImageByMedianFilter,  //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

//				&sGrayscale_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);
				&sOne_Filtered_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);

		} //for (iIters_ForMedianFilter = 0; iIters_ForMedianFilter < sParameters_Sobel_3x3_With_Preprocessing_1f.nNumofIters_ForMedianFilter_After_Filteringf; iIters_ForMedianFilter++)

	}//if (sParameters_Sobel_3x3_With_Preprocessing_1f.bIS_MEDIAN_FILTER_TO_FILTERED_IMAGE_INCLUDEDf == true)
	//printf("\n Debug 2_9");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   //printf("\n\n Before drawing pixels of edges: nWidthImage = %d, nLenImage = %d, please press any key", nWidthImage, nLenImage); getchar();

	int bytesOfWidth = image_in.pitchInBytes();

//	int nStep = image_in.pitchInBytes() / (image_in.width() * sizeof(unsigned char));

	// Save to file
	Image imageToSave(nLenImage, nWidthImage, bytesOfWidth);

	//printf("\n Debug 2_10");

	//initial shading of the image 
	for (iWid = 0; iWid < nWidthImage; iWid++) //j
	{

#ifndef COMMENT_OUT_ALL_PRINTS
#ifdef PRINTING_SUBIMAGE

		if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)
		{
			fprintf(fout0, "\n\n Filtered: iWid = %d\n", iWid);
		} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)

#endif // #ifdef PRINTING_SUBIMAGE
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		for (iLen = 0; iLen < nLenImage; iLen++) //i
		{
			nIndexCurSizef = iLen + (iWid*nLenImage);

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n Filtered: iWid = %d, iLen = %d, sOne_Filtered_Image.nGrayScale_Arr[%d] = %d", iWid, iLen, nIndexCurSizef, sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
#ifdef PRINTING_SUBIMAGE
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout0, "\n 2:  iWid = %d, iLen = %d, sOne_Filtered_Image.nGrayScale_Arr[%d] = %d, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[%d] = %d",
					iWid, iLen, nIndexCurSizef, sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef], nIndexCurSizef, sOne_Orig_Image_WeightedGray.nGrayScale_Arr[nIndexCurSizef]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)

#endif // #ifdef PRINTING_SUBIMAGE
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true)
			{
				if (sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] < nIntensityStatMin)
				{
					imageToSave(iWid, iLen, R) = 0;
					imageToSave(iWid, iLen, G) = 0;
					imageToSave(iWid, iLen, B) = 0;

				} // if (sOne_Orig_Image.nGrayScale_Arr[nIndexCurSizef] < nIntensityStatMin)
				else if (sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] >= nIntensityStatMin)
				{
					imageToSave(iWid, iLen, R) = nIntensityStatMax;
					imageToSave(iWid, iLen, G) = nIntensityStatMax;
					imageToSave(iWid, iLen, B) = nIntensityStatMax;
				} // else if (sOne_Orig_Image.nGrayScale_Arr[nIndexCurSizef] >= nIntensityStatMin)

			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) 

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == false)
			{

				if (sParameters_Sobel_3x3_With_Preprocessing_1f.bINCLUDE_EXP_INTO_SOBEL_FILTERf == true)
				{
					nIntensityBeforeExpf = sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef];

					nDiffOfIntensityAndThreshf = sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] - sParameters_Sobel_3x3_With_Preprocessing_1f.nThreshPixelIntensityForExp_SobelFilterf;

					fExpOfDiff = exp((float)(nDiffOfIntensityAndThreshf)*sParameters_Sobel_3x3_With_Preprocessing_1f.fConstForExp_SobelFilterf);

					sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] = (int)((float)(sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef])*fExpOfDiff);

#ifndef COMMENT_OUT_ALL_PRINTS
					//if (iWid >= nWid_AoI_UpperLeft && iWid <= nWid_AoI_BottomRight && iLen >= nLen_AoI_UpperLeft && iLen <= nLen_AoI_BottomRight)
					if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
					{
						fprintf(fout0, "\n 3: iWid = %d, iLen = %d, sOne_Filtered_Image.nGrayScale_Arr[%d] = %d, nIntensityBeforeExpf = %d",
							iWid, iLen, nIndexCurSizef, sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef], nIntensityBeforeExpf);

					}// if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bINCLUDE_EXP_INTO_SOBEL_FILTERf == true)

				if (sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] > nIntensityStatMax)
					sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;


				//imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayscale_Arr[nIndexOfPixelMax];
				imageToSave(iWid, iLen, R) = sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef];
				imageToSave(iWid, iLen, G) = sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef];
				imageToSave(iWid, iLen, B) = sOne_Filtered_Image.nGrayScale_Arr[nIndexCurSizef];

			} //if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == false) {

			imageToSave(iWid, iLen, A) = image_in(iWid, iLen, A);
		} //for (iLen = 0; iLen < nLength; iLen++)

	} //for (iWid = 0; iWid < nWidthImage; iWid++)

	//printf("\n Debug 2_11");

////////////////////////////////////////////////////////////////////////////////
	//imageToSave.write(outputFileNamef);

	image_out = imageToSave;
	/////////////////////////////////////////////////////////////////////////////////
	delete[] sOne_Orig_Image_WeightedGray.nGrayScale_Arr;
	delete[] sOne_Orig_Image_WeightedGray.nPixel_ValidOrNotArr;

	delete[] sOne_Filtered_Image.nGrayScale_Arr;
	delete[] sOne_Filtered_Image.nPixel_ValidOrNotArr;

	delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
	delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

	/////////////////////////////////////////////////////////////////////////////////

 #ifndef COMMENT_OUT_ALL_PRINTS
	fclose(fout0);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int doSobel_3x3_With_Preprocessing_1(...)

//////////////////////////////////////////////////////////////////////////////////////
int AoI_From_AnImage(

	const Image& image_in,

	int nWid_AoI_UpperLeftf, //
	int nLen_AoI_UpperLeftf, 

	int nWid_AoI_BottomRightf,
	int nLen_AoI_BottomRightf,

	//const GRAYSCALE_IMAGE *sGrayscale_Image,

	Image& AoI_image_out)
{
	int
		iLenf,
		iWidf,
		nIndexOfPixelCurf,
		nIntensityCurf,

		nLenOfAoif = nLen_AoI_BottomRightf - nLen_AoI_UpperLeftf + 1,
		nWidOfAoif = nWid_AoI_BottomRightf - nWid_AoI_UpperLeftf + 1,

		nLenImagef = image_in.width(),
		nWidthImagef = image_in.height();

	if (nWid_AoI_UpperLeftf >= nWid_AoI_BottomRightf || nWid_AoI_UpperLeftf >= nWidthImagef)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'AoI_From_AnImage': nWid_AoI_UpperLeftf = %d >= nWid_AoI_BottomRightf = %d, ...", nWid_AoI_UpperLeftf, nWid_AoI_BottomRightf);

		fprintf(fout0, "\n\n An error in 'AoI_From_AnImage': nWid_AoI_UpperLeftf = %d >= nWid_AoI_BottomRightf = %d, ...", nWid_AoI_UpperLeftf, nWid_AoI_BottomRightf);
		//printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nWid_AoI_UpperLeftf >= nWid_AoI_BottomRightf || ...)

	if (nLen_AoI_UpperLeftf >= nLen_AoI_BottomRightf || nLen_AoI_UpperLeftf >= nLenImagef)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'nWid_AoI_UpperLeftf': nLen_AoI_UpperLeftf = %d >= nLen_AoI_BottomRightf = %d, ...", nLen_AoI_UpperLeftf, nLen_AoI_BottomRightf);

		fprintf(fout0, "\n\n An error in 'AoI_From_AnImage': nLen_AoI_UpperLeftf = %d >= nLen_AoI_BottomRightf = %d, ...", nLen_AoI_UpperLeftf, nLen_AoI_BottomRightf);
	//	printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (nLen_AoI_UpperLeftf >= nLen_AoI_BottomRightf || ...)

	//printf("\n\n 'AoI_From_AnImage' 1");

//////////////////////////////////////////////////////////////////////////
	int bytesOfWidth; // = image_in.pitchInBytes();

	//bytesOfWidth = 4 * nWidOfAoif;
	//Image imageToSave(nWidOfAoif, nLenOfAoif, bytesOfWidth);

	//int nStep = image_in.pitchInBytes() / (image_in.width() * sizeof(unsigned char));

	//Image imageToSave;
	// Save to file
	bytesOfWidth = 4 * nLenOfAoif;
	Image imageToSave(nLenOfAoif,nWidOfAoif, bytesOfWidth);

////////////////////////////////////////////////////////////////////////////
			//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	for (iWidf = nWid_AoI_UpperLeftf; iWidf <= nWid_AoI_BottomRightf; iWidf++)
	{
		for (iLenf = nLen_AoI_UpperLeftf; iLenf <= nLen_AoI_BottomRightf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLen_CoToGr_Max);

			if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'AoI_From_AnImage': nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIndexOfPixelCurf, nImageSizeMax_CoToGr);

				fprintf(fout0, "\n\n An error in 'AoI_From_AnImage':  nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIndexOfPixelCurf, nImageSizeMax_CoToGr);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
				//getchar(); exit(1);
			} // if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)

			//if (sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] < nIntensityForAoI_Minf || sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] > nIntensityForAoI_Maxf)
				//continue;

			//nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];
//for a grayscale image, image_in(iWidf, iLenf, R) = image_in(iWidf, iLenf, G) = image_in(iWidf, iLenf, B)
			nIntensityCurf = image_in(iWidf, iLenf, R); 

#ifndef COMMENT_OUT_ALL_PRINTS
			//printf("\n 'AoI_From_AnImage': nIntensityCurf = %d, iWidf = %d, iLenf = %d", nIntensityCurf, iWidf, iLenf);
			//fprintf(fout0, "\n 'AoI_From_AnImage': nIntensityCurf = %d, iWidf = %d, iLenf = %d", nIntensityCurf, iWidf, iLenf);

			fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
			if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'AoI_From_AnImage': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

				fprintf(fout0, "\n\n An error in 'AoI_From_AnImage': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
				printf("\n\n Please press any key to exit:"); getchar(); exit(1);  fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
				//getchar(); exit(1);
			} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)
*/

//imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayscale_Arr[nIndexOfPixelMax];

			imageToSave(iWidf - nWid_AoI_UpperLeftf, iLenf - nLen_AoI_UpperLeftf, R) = nIntensityCurf;
			imageToSave(iWidf - nWid_AoI_UpperLeftf, iLenf - nLen_AoI_UpperLeftf, G) = nIntensityCurf;
			imageToSave(iWidf - nWid_AoI_UpperLeftf, iLenf - nLen_AoI_UpperLeftf, B) = nIntensityCurf;

			imageToSave(iWidf - nWid_AoI_UpperLeft, iLenf - nLen_AoI_UpperLeftf, A) = image_in(iWidf, iLenf, A);

			//printf("\n\n 'AoI_From_AnImage' 2_3");

		} //for (iLenf = nLen_AoI_UpperLeftf; iLenf <= nLen_AoI_BottomRightf; iLenf++)
	} //for (iWidf = nWid_AoI_UpperLeftf; iWidf <= nWid_AoI_BottomRightf; iWidf++)

	AoI_image_out = imageToSave;

	return SUCCESSFUL_RETURN;
}// int AoI_From_AnImage(...

  /////////////////////////////////////////////////////////////////////////////////////////////////////
int Histogram_Statistics_For_Orig_Image(

	GRAYSCALE_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

	int &nObjectAreaf,
	float &fPercentageOfobjectAreaf,
	int nHistogramArrf[], //[nNumOfHistogramBinsStat]
	float fPercentagesInHistogramArrf[]) //[nNumOfHistogramBinsStat]
{
	int
		nWidthImagef = sOne_Orig_Imagef->nWidth,
		nLenImagef = sOne_Orig_Imagef->nLength,

		nAreaOfTheWholeImagef,
		nWidthOfABinInHistogramf = (nIntensityStatMax - nIntensityStatMin) / nNumOfHistogramBinsStat,

		nNumOfHistogramBinCurf,

		nIntensityCurf,
		nIndexCurSizef,

		iHistogramBinf,
		iLenf,
		iWidf;

	if (nLenImagef < 1 || nLenImagef > nLenMax || nWidthImagef < 1 || nWidthImagef > nWidMax)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nLenImagef = %d, nLenMax = %d, nWidthImagef = %d, nWidMax = %d",
			nLenImagef, nLenMax, nWidthImagef, nWidMax);

		fprintf(fout0, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nLenImagef = %d, nLenMax = %d, nWidthImagef = %d, nWidMax = %d",
			nLenImagef, nLenMax, nWidthImagef, nWidMax);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		//printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nLenImagef < 1 || nLenImagef > nLenMax || nWidthImagef < 1 || nWidthImagef > nWidMax)

	//printf("\n Debug 2_4_2");

	  /////////////////////////////////////////////////

	nAreaOfTheWholeImagef = nWidthImagef * nLenImagef;

	if (nAreaOfTheWholeImagef <= 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		fprintf(fout0, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		//printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} // if (nAreaOfTheWholeImagef <= 0)

	//printf("\n Debug 2_4_3");

	nObjectAreaf = 0;

	//memset(nHistogramArrf, 0, sizeof(nHistogramArrf));
	//memset(fPercentagesInHistogramArrf, 0.0, sizeof(fPercentagesInHistogramArrf));

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		nHistogramArrf[iHistogramBinf] = 0;
		fPercentagesInHistogramArrf[iHistogramBinf] = 0.0;

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	//printf("\n Debug 2_4_4");

	  //////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout0, "\n\n Inside 'Histogram_Statistics_For_Orig_Image'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
	{
/*
#ifndef COMMENT_OUT_ALL_PRINT

		if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax) // && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
		{
			fprintf(fout0, "\n\n iWidf = %d: ", iWidf);

		} //if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax) 

#endif // #ifndef COMMENT_OUT_ALL_PRINT
*/

		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);

			nIntensityCurf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizef];

#ifndef COMMENT_OUT_ALL_PRINTS
			if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
			{
				fprintf(fout0, " %d:%d %d,", iWidf, iLenf, nIntensityCurf);
			} //
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			if (nIntensityCurf >= nIntensityStatMin)
			{
				nObjectAreaf += 1;

				nNumOfHistogramBinCurf = (nIntensityCurf - nIntensityStatMin) / nWidthOfABinInHistogramf;

				if (nNumOfHistogramBinCurf >= nNumOfHistogramBinsStat)
					nNumOfHistogramBinCurf = nNumOfHistogramBinsStat - 1;

				nHistogramArrf[nNumOfHistogramBinCurf] += 1;
			} //if (nIntensityCurf >= nIntensityStatMin)

		} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

	} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

	fPercentageOfobjectAreaf = (float)(nObjectAreaf) / (float)(nAreaOfTheWholeImagef);

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		if (nObjectAreaf > 0)
		{
			fPercentagesInHistogramArrf[iHistogramBinf] = (float)(nHistogramArrf[iHistogramBinf]) / (float)(nObjectAreaf);
		} //if (nObjectAreaf > 0)
		else if (nObjectAreaf == 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			printf("\n Please try decreasing 'nIntensityStatMin'");

			fprintf(fout0, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			fprintf(fout0, "\n Please try decreasing 'nIntensityStatMin'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			//printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		} // else if (nObjectAreaf == 0)

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	//sOne_Orig_Imagef->nAreaForStat = nObjectAreaf;
	//sOne_Orig_Imagef->fPercentageOfobjectArea = fPercentageOfobjectAreaf;

	return SUCCESSFUL_RETURN;
} //Histogram_Statistics_For_Orig_Image(...

 //////////////////////////////////////////////////////////////////////////////////////
void Sobel_Filtering_3x3_With_Preprocessing_1(

	const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,
	const GRAYSCALE_IMAGE *sOne_Orig_Imagef,
	GRAYSCALE_IMAGE *sOne_Filtered_Imagef)
{
/*
	int Initializing_GRAYSCALE_To_CurSize(
		const int nWidthImagef,
		const int nLenImagef,

		GRAYSCALE_IMAGE *sGRAYSCALE_Imagef);
*/

	int

		nIndexCurSizef,
		nIndexCurSizeOfANeighborf,

		nSumLenf,
		nSumWidf,

		nLenNeighf,
		nWidNeighf,

		nWidthImagef = sOne_Orig_Imagef->nWidth,
		nLenImagef = sOne_Orig_Imagef->nLength,

		nIntensityNeighf,

		nSumOfABS_Of_nSumLenf_and_nSumWidf = 0,
//		nResf,
		iWidf,
		iLenf;

	//3x3 Sobel filter
//#ifdef DIM3x3_SOBEL_FILTER

	// use 'nIntensityThreshold' to change the number of object pixels!
	for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidth; iWidf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout0, "\n 'Sobel_Filtering_3x3_With_Preprocessing_1': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLength; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);

			if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidth - 1)
			{
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
				continue;
			} // if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidth - 1)

			if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLength - 1)
			{
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
				continue;
			} // if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLength - 1)

			nSumLenf = 0;
			nSumWidf = 0;

			nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
			/////////////////////////////////////////////////////////////////////
			//1 -- addition for nSumLenf
			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			//bVERSION_2_OF_SOBEL_3x3f = true
			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf += nIntensityNeighf;

				nSumWidf -= nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf += 3 * nIntensityNeighf;

				nSumWidf -= 3 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n\n 1: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
			//fprintf(fout0, "\n nWidNeighf = %d, nLenNeighf = %d, sOne_Orig_Imagef->nGrayScale_Arr[%d] = %d",
				//nWidNeighf, nLenNeighf, nIndexCurSizeOfANeighborf,sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			/////////////////////////////////////////////////////////////////////
			//2 -- addition for nSumLenf
			nLenNeighf = iLenf;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf += 2 * nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)


			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf += 10 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout0, "\n 2: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//3 -- addition for nSumLenf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf += nIntensityNeighf;

				nSumWidf += nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)


			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf += 3 * nIntensityNeighf;

				nSumWidf += 3 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout0, "\n 3: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//4 -- subtraction for nSumLenf

			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf -= nIntensityNeighf;
				nSumWidf -= nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf -= 3 * nIntensityNeighf;

				nSumWidf -= 3 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout0, "\n 4: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//5 -- subtraction for nSumLenf
			nLenNeighf = iLenf;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf -= 2 * nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf -= 10 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout0, "\n 5: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//6 -- subtraction for nSumLenf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumLenf -= nIntensityNeighf;

				nSumWidf += nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				nSumLenf -= 3 * nIntensityNeighf;

				nSumWidf += 3 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout0, "\n 6: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//7 -- addition  for nSumWidf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				//addition  for nSumWidf
				nSumWidf += 2 * nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) {
				//addition  for nSumWidf
				nSumWidf += 10 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

		//	fprintf(fout0, "\n 7: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//8 -- subtraction for nSumWidf
			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
			nIntensityNeighf = sOne_Orig_Imagef->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)
			{
				nSumWidf -= 2 * nIntensityNeighf;
			}  // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true)
			{
				nSumWidf -= 10 * nIntensityNeighf;
			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true)
			{
				//fprintf(fout0, "\n 8: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

				///////////////////////////////////////////////////////////////////////////////////
				nSumLenf = (int)((float)(nSumLenf) / sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_For_2nd_Versionf); //fSE_Weight_For_2nd_Version);
				nSumWidf = (int)((float)(nSumWidf) / sParameters_Sobel_3x3_With_Preprocessing_1f.fSE_Weight_For_2nd_Versionf); // fSE_Weight_For_2nd_Version);

			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bVERSION_2_OF_SOBEL_3x3f == true) 
			/////////////////////////////////////////////////////////////////////

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSE_SUMS_OF_IMAGE_GRADIENTSf == true) {

				//The sum of absolute values
				if (nSumLenf < 0)
				{
					nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumLenf);
				} //if (nSumLenf < 0)
				else if (nSumLenf > 0)
				{

					nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumLenf;
				} //else if (nSumLenf > 0)

				if (nSumWidf < 0)
				{
					nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumWidf);
				} //if (nSumWidf < 0)
				else if (nSumWidf > 0)
				{
					nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumWidf;
				} //else if (nSumWidf > 0)

				if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {

					if (nSumOfABS_Of_nSumLenf_and_nSumWidf > sParameters_Sobel_3x3_With_Preprocessing_1f.nIntensityThresholdForSobelMaxf)
						nSumOfABS_Of_nSumLenf_and_nSumWidf = nIntensityStatMax;

					if (nSumOfABS_Of_nSumLenf_and_nSumWidf < sParameters_Sobel_3x3_With_Preprocessing_1f.nIntensityThresholdForSobelMinf)
						nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
				} //if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {

				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = nSumOfABS_Of_nSumLenf_and_nSumWidf;

			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSE_SUMS_OF_IMAGE_GRADIENTSf == true) 

			////////////////////////////////////////////////////////////////////////////////////////////////////////////

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSE_SUMS_OF_IMAGE_GRADIENTSf == false)
			{
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = (int)(sqrt((float)(nSumLenf*nSumLenf + nSumWidf * nSumWidf) / sParameters_Sobel_3x3_With_Preprocessing_1f.fNormalizingf));
				//sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = (int)(sqrt( (float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) ));


				if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {
					if (sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] >= sParameters_Sobel_3x3_With_Preprocessing_1f.nIntensityThresholdForSobelMaxf)
						sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = 0;
					else
						sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = 255;
				} //if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {

			} // if (sParameters_Sobel_3x3_With_Preprocessing_1f.bUSE_SUMS_OF_IMAGE_GRADIENTSf == false) 

			// The histogram threshold could be applied here
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] > nIntensityStatMax)
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
			else if (sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] < 0)
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = 0;

			if (sParameters_Sobel_3x3_With_Preprocessing_1f.bBLACK_BACKGROUND_WHITE_CONTOURSf == false)
			//if (sParameters_Sobel_3x3_With_Preprocessing_1f.bBLACK_BACKGROUND_WHITE_CONTOURSf == true)
			{
				sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax - sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef];
			} //if (sParameters_Sobel_3x3_With_Preprocessing_1f.bBLACK_BACKGROUND_WHITE_CONTOURSf == false)

			//fprintf(fout0, "\n iWidf = %d, iLenf = %d, sOne_Filtered_Imagef->nGrayScale_Arr[%d] = %d", iWidf, iLenf, nIndexCurSizef, sOne_Filtered_Imagef->nGrayScale_Arr[nIndexCurSizef]);
		} //for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLength; iLenf++)

	} //for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidth; iWidf++)

//#endif // #ifdef DIM3x3_SOBEL_FILTER

	//return SUCCESSFUL_RETURN;
	} //void Sobel_Filtering_3x3_With_Preprocessing_1(...
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Wikipedia, Gaussian blur

	void ConvolutionMatrixFor_UnSharp_Masking(
		const int nRadius_ForUnsharp_Maskf,
		const float fStDev_ForUnsharp_Maskf,

		float fConvmatrixf[])
	{
		int
			iWidOfSqf,
			iLenOfSqf,
			nIndexOfSqf,

			nDiffWidOfSqf,

			nDiffLenOfSqf,

			nWidCentreOfSqf = nRadius_ForUnsharp_Maskf,
			nLenCentreOfSqf = nRadius_ForUnsharp_Maskf,

			nSumOfSquaresOfDiffWid_Lenf,

			nDimOfSquaref = 2 * nRadius_ForUnsharp_Maskf + 1;

		float
			fPowerOfExpf,

			f2Variancesf = (float)(2.0)*(fStDev_ForUnsharp_Maskf * fStDev_ForUnsharp_Maskf);

		for (iWidOfSqf = 0; iWidOfSqf < nDimOfSquaref; iWidOfSqf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'ConvolutionMatrixFor_UnSharp_Masking': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			for (iLenOfSqf = 0; iLenOfSqf < nDimOfSquaref; iLenOfSqf++)
			{
				nDiffWidOfSqf = iWidOfSqf - nWidCentreOfSqf;

				nDiffLenOfSqf = iLenOfSqf - nLenCentreOfSqf;

				nSumOfSquaresOfDiffWid_Lenf = (nDiffWidOfSqf*nDiffWidOfSqf) + (nDiffLenOfSqf*nDiffLenOfSqf);

				fPowerOfExpf = -(float)(nSumOfSquaresOfDiffWid_Lenf) / f2Variancesf;

				nIndexOfSqf = iLenOfSqf + (iWidOfSqf*nDimOfSquaref);

				fConvmatrixf[nIndexOfSqf] = exp(fPowerOfExpf) / ((float)(pi)*f2Variancesf);

			} //for (iLenOfSqf = 0; iLenOfSqf < nDimOfSquaref; iLenOfSqf++)

		} //for (iWidOfSqf = 0; iWidOfSqf < nDimOfSquaref; iWidOfSqf++)

	}//void ConvolutionMatrixFor_UnSharp_Masking(
//////////////////////////////////////////////////////////////////////////////////////////

	void ConvolutionForOnePixel_UnSharp_Masking(
		const int nRadius_ForUnsharp_Maskf,
		const float fStDev_ForUnsharp_Maskf,

		const float fConvmatrixf[],

		const int nWidOfPixelf,
		const int nLenOfPixelf,

		const GRAYSCALE_IMAGE *sGray_Image_Initf,
		GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf)

	{
		int
			iWidOfSqf,
			iLenOfSqf,
			nIndexOfSqf,

			nWidthImagef = sGray_Image_Initf->nWidth,
			nLenImagef = sGray_Image_Initf->nLength,

			nIndexOfImagef,

			nIntensityCurf,

			nWidCentreOfSqf = nRadius_ForUnsharp_Maskf,
			nLenCentreOfSqf = nRadius_ForUnsharp_Maskf,

			nDimOfSquaref = 2 * nRadius_ForUnsharp_Maskf + 1,

			nWidInImageCurf,
			nLenInImageCurf;


		float
			fConvValueCurf,
			fSumf = 0.0;

		for (iWidOfSqf = 0; iWidOfSqf < nDimOfSquaref; iWidOfSqf++)
		{
			nWidInImageCurf = nWidOfPixelf + (iWidOfSqf - nRadius_ForUnsharp_Maskf);

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n '': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			for (iLenOfSqf = 0; iLenOfSqf < nDimOfSquaref; iLenOfSqf++)
			{
//Image
				nLenInImageCurf = nLenOfPixelf + (iLenOfSqf - nRadius_ForUnsharp_Maskf);

				nIndexOfImagef = nLenInImageCurf + (nWidInImageCurf*nLenImagef);

				nIntensityCurf = sGray_Image_Initf->nGrayScale_Arr[nIndexOfImagef];

				if (nIntensityCurf < nIntensityForAoI_Min) // the background
				{
					//sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;

					continue;
				}//if (nIntensityCurf < nIntensityForAoI_Min)
//Square

				nIndexOfSqf = iLenOfSqf + (iWidOfSqf*nDimOfSquaref);
				fConvValueCurf = fConvmatrixf[nIndexOfSqf];

				fSumf += fConvValueCurf * (float)(nIntensityCurf);

			} //for (iLenOfSqf = 0; iLenOfSqf < nDimOfSquaref; iLenOfSqf++)

		} //for (iWidOfSqf = 0; iWidOfSqf < nDimOfSquaref; iWidOfSqf++)

		nIndexOfImagef = nLenOfPixelf + (nWidOfPixelf*nLenImagef);
		sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexOfImagef] = (int)(fSumf);

	}//void ConvolutionForOnePixel_UnSharp_Masking(...

/////////////////////////////////////////////////////////////////////////
	void Is_Pixel_Suitable_For_Unsharp_Mask(

		const int nDist_Unsharp_maskf,
		const int nIntensity_Threshold_Unsharp_maskf,
		const int nNumOfThreshViolationsMin_Unsharp_maskf,

		const int nWidOfPixelf,
		const int nLenOfPixelf,

		GRAYSCALE_IMAGE *sGray_Image_Initf,
		bool bIs_Pixel_Suitablef)
	{

		int
			nIndexCurSizef,
			nIndexCurSizeOfANeighborf,

			nNumOfThreshViolations_Totf = 0,

			nLenNeighf,
			nWidNeighf,

			nWidthImagef = sGray_Image_Initf->nWidth,
			nLenImagef = sGray_Image_Initf->nLength,

			nIntensityNeighf,
			nIntensityInitf = -1;

		////////////////////////////////////////////////////////////////////////////
		// use 'nIntensityThreshold' to change the number of object pixels!
		
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Is_Pixel_Suitable_For_Unsharp_Mask': nWidOfPixelf = %d, nLenOfPixelf = %d", nWidOfPixelf,nLenOfPixelf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		nIndexCurSizef = nLenOfPixelf + (nWidOfPixelf*nLenImagef);

		nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

		/////////////////////////////////////////////////////////////////////

//1
		nLenNeighf = nLenOfPixelf - nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf + nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)

/////////////////////////////////////////////////////////////////////
//2 
		nLenNeighf = nLenOfPixelf;
		nWidNeighf = nWidOfPixelf + nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		/////////////////////////////////////////////////////////////////////

//3
		nLenNeighf = nLenOfPixelf + nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf + nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
/////////////////////////////////////////////////////////////////////
//4
		nLenNeighf = nLenOfPixelf - nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf - nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
/////////////////////////////////////////////////////////////////////
//5
		nLenNeighf = nLenOfPixelf;
		nWidNeighf = nWidOfPixelf - nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
/////////////////////////////////////////////////////////////////////
//6
		nLenNeighf = nLenOfPixelf + nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf - nDist_Unsharp_maskf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
/////////////////////////////////////////////////////////////////////
//7
		nLenNeighf = nLenOfPixelf + nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)

		/////////////////////////////////////////////////////////////////////
//8
		nLenNeighf = nLenOfPixelf - nDist_Unsharp_maskf;
		nWidNeighf = nWidOfPixelf;

		nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
		nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

		if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
		{
			nNumOfThreshViolations_Totf += 1;
		} // if (nIntensityInitf - nIntensityNeighf >= nIntensity_Threshold_Unsharp_maskf || nIntensityNeighf - nIntensityInitf >= nIntensity_Threshold_Unsharp_maskf)
///////////////////////////////////////////////////////////////////////////////
		
		if (nNumOfThreshViolations_Totf >= nNumOfThreshViolationsMin_Unsharp_maskf)
			bIs_Pixel_Suitablef = true;
		else
			bIs_Pixel_Suitablef = false;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
		//fprintf(fout0, "\n nWidOfPixelf = %d, nLenOfPixelf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
			//nWidOfPixelf, nLenOfPixelf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	//return SUCCESSFUL_RETURN;
	} //void Is_Pixel_Suitable_For_Unsharp_Mask(...

/////////////////////////////////////////////////////////////////////////////////////////
//Wikipedia, "Unsharp masking"
	void Unsharp_masking_3x3(

		const float fCoefOfSharpening_Inside3x3f,
		const int nDiffOfIntensities_Threshold_Unsharp_maskf,

			const bool bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf,
		GRAYSCALE_IMAGE *sGray_Image_Initf,
		GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf)
	{
		
		int

			nIndexCurSizef,
			nIndexCurSizeOfANeighborf,

			nLenNeighf,
			nWidNeighf,

			nWidthImagef = sGray_Image_Initf->nWidth,
			nLenImagef = sGray_Image_Initf->nLength,

			nIntensityNeighf,
			nIntensityInitf = -1,

			nSumOfABS_Of_nSumLenf_and_nSumWidf = 0,
			//		nResf,
			iWidf,
			iLenf;

		//3x3 Sobel filter
	//#ifdef DIM3x3_SOBEL_FILTER

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout0, "\n\n 'Unsharp_masking_3x3', nWidthImagef = %d, nLenImagef = %d, sGrayImage_ByUnsharpMaskf->nWidth = %d, sGrayImage_ByUnsharpMaskf->nLength = %d", 
						nWidthImagef, nLenImagef, sGrayImage_ByUnsharpMaskf->nWidth, sGrayImage_ByUnsharpMaskf->nLength);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////////////////////////////
		// use 'nIntensityThreshold' to change the number of object pixels!
		for (iWidf = 1; iWidf < nWidthImagef - 1; iWidf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_3x3': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			for (iLenf = 1; iLenf < nLenImagef - 1; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = 0;
				nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
				/////////////////////////////////////////////////////////////////////
/*
				//1 -- addition for nSumLenf
				nLenNeighf = iLenf - 1;
				nWidNeighf = iWidf + 1;

*/
			/////////////////////////////////////////////////////////////////////
				//2 -- addition for nSumLenf
				nLenNeighf = iLenf;
				nWidNeighf = iWidf + 1;

				nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
				nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

				nSumOfABS_Of_nSumLenf_and_nSumWidf += (int)((-fCoefOfSharpening_Inside3x3f)*(float)(nIntensityNeighf));
				/////////////////////////////////////////////////////////////////////
				
/*
				nLenNeighf = iLenf + 1;
				nWidNeighf = iWidf + 1;
*/

				/////////////////////////////////////////////////////////////////////
/*
				nLenNeighf = iLenf - 1;
				nWidNeighf = iWidf - 1;
*/
	
				/////////////////////////////////////////////////////////////////////
				
				nLenNeighf = iLenf;
				nWidNeighf = iWidf - 1;

				nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
				nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

				nSumOfABS_Of_nSumLenf_and_nSumWidf += (int)((-fCoefOfSharpening_Inside3x3f)*(float)(nIntensityNeighf));

				/////////////////////////////////////////////////////////////////////
/*
				nLenNeighf = iLenf + 1;
				nWidNeighf = iWidf - 1;
*/
	
				/////////////////////////////////////////////////////////////////////
				nLenNeighf = iLenf + 1;
				nWidNeighf = iWidf;

				nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
				nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

				nSumOfABS_Of_nSumLenf_and_nSumWidf += (int)((-fCoefOfSharpening_Inside3x3f)*(float)(nIntensityNeighf));

				/////////////////////////////////////////////////////////////////////
				nLenNeighf = iLenf - 1;
				nWidNeighf = iWidf;

				nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
				nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

				nSumOfABS_Of_nSumLenf_and_nSumWidf += (int)( (-fCoefOfSharpening_Inside3x3f)*(float)(nIntensityNeighf) );

				///////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////
				nLenNeighf = iLenf;
				nWidNeighf = iWidf;

				nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nLenImagef);
				nIntensityNeighf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizeOfANeighborf];

				//nSumOfABS_Of_nSumLenf_and_nSumWidf += 5 * (int)((fCoefOfSharpening_Inside3x3f)*(float)(nIntensityNeighf));
				nSumOfABS_Of_nSumLenf_and_nSumWidf += 5 * nIntensityNeighf;

				if (nSumOfABS_Of_nSumLenf_and_nSumWidf < 0)
					nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;

				if (nSumOfABS_Of_nSumLenf_and_nSumWidf > nIntensityStatMax)
					nSumOfABS_Of_nSumLenf_and_nSumWidf = nIntensityStatMax;

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nSumOfABS_Of_nSumLenf_and_nSumWidf;

/*
				if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] - nIntensityInitf >= nDiffOfIntensities_Threshold_Unsharp_maskf ||
					nIntensityInitf - sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] >= nDiffOfIntensities_Threshold_Unsharp_maskf)
				{
					continue;
				}//if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] - nIntensityInitf >= nDiffOfIntensities_Threshold_Unsharp_maskf ||
				else
				{
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;
				} // else
*/
				////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout0, "\n iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
					iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 1; iLenf < nLenImagef - 1; iLenf++)

		} //for (iWidf = 1; iWidf < nWidthImagef - 1; iWidf++)

///////////////////////////////////////////////////////

//the white color to the edges

		if (bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf == true)
		{
			
#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout0, "\n 'Unsharp_masking_3x3', the top: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			iWidf = 0;
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		
#ifndef COMMENT_OUT_ALL_PRINTS
				//	fprintf(fout0, "\n 'Unsharp_masking_3x3', the bottom: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			iWidf = nWidthImagef - 1;
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

			//fixed iLenf
			iLenf = 0;
			for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
			}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

			iLenf = nLenImagef - 1;
			for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;
			}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		} //if (bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKING == true)

		if (bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf == false)
		{
	
#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout0, "\n 'Unsharp_masking_3x3', the top: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			iWidf = 0;
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout0, "\n The top: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
					iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			
#ifndef COMMENT_OUT_ALL_PRINTS
				//	fprintf(fout0, "\n 'Unsharp_masking_3x3', the bottom: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			iWidf = nWidthImagef - 1;
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout0, "\n The bottom: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
					iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

			//fixed iLenf
			iLenf = 0;
			for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout0, "\n The left: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
					iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

			iLenf = nLenImagef - 1;
			for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);
				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];
#ifndef COMMENT_OUT_ALL_PRINTS	
				fprintf(fout0, "\n The right: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
					iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		} // if (bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf == false)

	//return SUCCESSFUL_RETURN;
	} //void Unsharp_masking_3x3(...

/////////////////////////////////////////////////////////////////////////////////

//Wikipedia, "Unsharp masking"
	int Unsharp_masking_VariableRadius(

		const int nRadius_ForUnsharp_Maskf,
		const float fStDev_ForUnsharp_Maskf,
		const int nDiffOfIntensities_Threshold_Unsharp_maskf,

		const float fAmount_UnsharpMaskf,

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		const GRAYSCALE_IMAGE *sGray_Image_Initf,
		GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf)
	{
		void Is_Pixel_Suitable_For_Unsharp_Mask(

			const int nDist_Unsharp_maskf,
			const int nIntensity_Threshold_Unsharp_maskf,
			const int nNumOfThreshViolationsMin_Unsharp_maskf,

			const int nWidOfPixelf,
			const int nLenOfPixelf,

			GRAYSCALE_IMAGE *sGray_Image_Initf,
			bool bIs_Pixel_Suitablef);

		void ConvolutionMatrixFor_UnSharp_Masking(
			const int nRadius_ForUnsharp_Maskf,
			const float fStDev_ForUnsharp_Maskf,

			float fConvmatrixf[]);

		void ConvolutionForOnePixel_UnSharp_Masking(
			const int nRadius_ForUnsharp_Maskf,
			const float fStDev_ForUnsharp_Maskf,

			const float fConvmatrixf[],

			const int nWidOfPixelf,
			const int nLenOfPixelf,

			const GRAYSCALE_IMAGE *sGray_Image_Initf,
			GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

		int

			nIndexCurSizef,

			nWidthImagef = sGray_Image_Initf->nWidth,
			nLenImagef = sGray_Image_Initf->nLength,

			nNumOfPixelsInImageTotf = nWidthImagef * nLenImagef,

			nIntensityInitf = -1,

			nDimOfSquaref = 2 * nRadius_ForUnsharp_Maskf + 1,

			nNumOfPixelsInSqf = nDimOfSquaref* nDimOfSquaref,
			nDiffOfIntensitiesf,
			nNumOfChangedPixelsByUnSharpMaskingTotf = 0,

			nDiffOfChangedPixelsAverf = 0,

			nIntensityAfterUnsharpMaskingf,

			iWidf,
			iLenf;

		float * fConvmatrixf;
		fConvmatrixf = new float[nNumOfPixelsInSqf];

		if (fConvmatrixf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in dynamic memory allocation");
			fprintf(fout0, "\n\n An error in dynamic memory allocation");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (fConvmatrixf == nullptr)

		ConvolutionMatrixFor_UnSharp_Masking(
			nRadius_ForUnsharp_Maskf, //const int nRadius_ForUnsharp_Maskf,
			fStDev_ForUnsharp_Maskf, //const float StDevf,

			fConvmatrixf); // float fConvmatrixf[]);

#ifndef COMMENT_OUT_ALL_PRINTS
		for (iWidf = 0; iWidf < nDimOfSquaref; iWidf++)
		{
			fprintf(fout0, "\n\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);

			for (iLenf = 0; iLenf < nDimOfSquaref; iLenf++)
			{
				nIndexCurSizef = iLenf + iWidf * nDimOfSquaref;

				fprintf(fout0, "\n fConvmatrixf[%d] = %E, iWidf = %d, iLenf = %d", nIndexCurSizef, fConvmatrixf[nIndexCurSizef], iWidf, iLenf);

			} //for (iLenf = 0; iLenf < nDimOfSquaref; iLenf++)
		} // for (iWidf = 0; iWidf < nDimOfSquaref; iWidf++)

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////////////////////////////////////////////
//The pixels close to the image boundaries are just copied

//the top boundary area
		for (iWidf = 0; iWidf < nRadius_ForUnsharp_Maskf; iWidf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf; //

			////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout0, "\n 1: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
					//iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nRadius_ForUnsharp_Maskf; iWidf++)
////////////////////////////////////////////////////////////////////////////////////////

//the bottom boundary area
		for (iWidf = nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef; iWidf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf; //
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
			//	fprintf(fout0, "\n 2: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
				//	iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef; iWidf++)
///////////////////////////////////////////////////////////////////////////////

//the left boundary area
		for (iWidf = 0; iWidf < nWidthImagef; iWidf++) //some pixels are copied again
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < nRadius_ForUnsharp_Maskf; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf; //

			////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout0, "\n 3: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
					//iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 0; iLenf < nRadius_ForUnsharp_Maskf; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

///////////////////////////////////////////////////////////////////////////////
//the right boundary area
		for (iWidf = 0; iWidf < nWidthImagef; iWidf++) //some pixels are copied again
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = nLenImagef - nRadius_ForUnsharp_Maskf; iLenf < nLenImagef; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf; //

			////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout0, "\n 4: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d", 
					//iWidf, iLenf, nIntensityInitf,nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = nLenImagef - nRadius_ForUnsharp_Maskf; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

////////////////////////////////////////////////////////////////////////////

//Area where the unsharp masking can be applied (away enough from the boundaries)
		for (iWidf = nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_VariableRadius': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = nRadius_ForUnsharp_Maskf; iLenf < nLenImagef - nRadius_ForUnsharp_Maskf; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

/*
void Is_Pixel_Suitable_For_Unsharp_Mask(

		const int nDist_Unsharp_maskf,
		const int nIntensity_Threshold_Unsharp_maskf,
		const int nNumOfThreshViolationsMin_Unsharp_maskf,

		const int nWidOfPixelf,
		const int nLenOfPixelf,

		GRAYSCALE_IMAGE *sGray_Image_Initf,
		bool bIs_Pixel_Suitablef);
*/
				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				if (nIntensityInitf < nIntensityForAoI_Min)
				{
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;

					continue;
				}//if (nIntensityInitf < nIntensityForAoI_Min)

				ConvolutionForOnePixel_UnSharp_Masking(
					nRadius_ForUnsharp_Maskf, //const int nRadius_ForUnsharp_Maskf,
					fStDev_ForUnsharp_Maskf, //const float fStDev_ForUnsharp_Maskf,

					fConvmatrixf, //const float fConvmatrixf[],

					iWidf, //const int nWidOfPixelf,
					iLenf, //const int nLenOfPixelf,

					sGray_Image_Initf, //const GRAYSCALE_IMAGE *sGray_Image_Initf,
					sGrayImage_ByUnsharpMaskf); // GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf)

#ifndef COMMENT_OUT_ALL_PRINTS	
				//fprintf(fout0, "\n 5: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
					//iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

/*
				if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] - nIntensityInitf >= nDiffOfIntensities_Threshold_Unsharp_maskf ||
					nIntensityInitf - sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] >= nDiffOfIntensities_Threshold_Unsharp_maskf)
				{
					continue;
				}//if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] - nIntensityInitf >= nDiffOfIntensities_Threshold_Unsharp_maskf ||
				else
				{
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;
				} // else
*/
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			} //for (iLenf = nRadius_ForUnsharp_Maskf; iLenf < nLenImagef - nRadius_ForUnsharp_Maskf; iLenf++)

		} //for (iWidf = nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf++)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for (iWidf = nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf++)
		{
			for (iLenf = nRadius_ForUnsharp_Maskf; iLenf < nLenImagef - nRadius_ForUnsharp_Maskf; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				if (nIntensityInitf < nIntensityForAoI_Min)
				{
				//	sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;

					continue;
				}//if (nIntensityInitf < nIntensityForAoI_Min)

				nDiffOfIntensitiesf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef] - sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef];

#ifndef COMMENT_OUT_ALL_PRINTS

				//fprintf(fout0, "\n\n 'Unsharp_masking_VariableRadius': nDiffOfIntensitiesf = %d, iWidf = %d, iLenf = %d",
					//nDiffOfIntensitiesf, iWidf, iLenf);

				//fprintf(fout0, "\n sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef] = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = %d",
					//sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef], sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS


				if (nDiffOfIntensitiesf >= nDiffOfIntensities_Threshold_Unsharp_maskf || (-nDiffOfIntensitiesf) >= nDiffOfIntensities_Threshold_Unsharp_maskf)
				{
					nNumOfChangedPixelsByUnSharpMaskingTotf += 1;

					nIntensityAfterUnsharpMaskingf = (int)(sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMaskf*(float)(sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef])) + 
							(int)(sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf*(float)(nDiffOfIntensitiesf));

					if (nDiffOfIntensitiesf > 0)
						nDiffOfChangedPixelsAverf += nDiffOfIntensitiesf;
					else if ((-nDiffOfIntensitiesf) > 0)
						nDiffOfChangedPixelsAverf += (-nDiffOfIntensitiesf);

#ifndef COMMENT_OUT_ALL_PRINTS

					fprintf(fout0, "\n\n 'Unsharp_masking_VariableRadius': nIntensityAfterUnsharpMaskingf = %d, nNumOfChangedPixelsByUnSharpMaskingTotf = %d",
						nIntensityAfterUnsharpMaskingf, nNumOfChangedPixelsByUnSharpMaskingTotf);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				}//if (nDiffOfIntensitiesf >= nDiffOfIntensities_Threshold_Unsharp_maskf || (-nDiffOfIntensitiesf) >= nDiffOfIntensities_Threshold_Unsharp_maskf)
				else
				{
					nIntensityAfterUnsharpMaskingf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];
				} // else

				if (nIntensityAfterUnsharpMaskingf < 0)
					nIntensityAfterUnsharpMaskingf = 0;

				if (nIntensityAfterUnsharpMaskingf > nIntensityStatMax)
					nIntensityAfterUnsharpMaskingf = nIntensityStatMax;

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityAfterUnsharpMaskingf;
				
				//imageToSave(iWid, iLen, R) = imageToSave(iWid, iLen, G) = imageToSave(iWid, iLen, B) = nIntensityAfterUnsharpMaskingf;
			} //for (iLenf = nRadius_ForUnsharp_Maskf; iLenf < nLenImagef - nRadius_ForUnsharp_Maskf; iLenf++)

		} //for (iWidf = nRadius_ForUnsharp_Maskf; iWidf < nWidthImagef - nRadius_ForUnsharp_Maskf; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS

		if (nNumOfChangedPixelsByUnSharpMaskingTotf > 0)
		{
			nDiffOfChangedPixelsAverf = nDiffOfChangedPixelsAverf / nNumOfChangedPixelsByUnSharpMaskingTotf;
		}//if (nNumOfChangedPixelsByUnSharpMaskingTotf > 0)

		printf("\n\n The percentage of changed pixels = %E, nDiffOfChangedPixelsAverf = %d",
			(float)(nNumOfChangedPixelsByUnSharpMaskingTotf) / ((float)(nNumOfPixelsInImageTotf) ), nDiffOfChangedPixelsAverf);

		printf("\n\n sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf = %E, sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMaskf = %E", sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf, sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMaskf);

		printf("\n nRadius_ForUnsharp_Maskf = %d, nDiffOfIntensities_Threshold_Unsharp_mask = %d, nIntensityForAoI_Min = %d", nRadius_ForUnsharp_Maskf, nDiffOfIntensities_Threshold_Unsharp_maskf, nIntensityForAoI_Min);

		fprintf(fout0, "\n\n The percentage of changed pixels = %E, nDiffOfChangedPixelsAverf = %d",
			(float)(nNumOfChangedPixelsByUnSharpMaskingTotf) / ((float)(nNumOfPixelsInImageTotf)), nDiffOfChangedPixelsAverf);

		fprintf(fout0, "\n\n sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf = %E, sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMaskf = %E", sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf, sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMaskf);

		fprintf(fout0, "\n nRadius_ForUnsharp_Maskf = %d, nDiffOfIntensities_Threshold_Unsharp_mask = %d, nIntensityForAoI_Min = %d", nRadius_ForUnsharp_Maskf, nDiffOfIntensities_Threshold_Unsharp_maskf, nIntensityForAoI_Min);

		fflush(fout0);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
		fflush(fout0);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	delete [] fConvmatrixf;

	return SUCCESSFUL_RETURN;
	} //int Unsharp_masking_VariableRadius(...

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//A rational unsharp masking technique (Giovanni Ramponi, etc., 1997)
	int Unsharp_masking_rational(

		const float fK_coeff,
		const float fH_coeff,

		const float fGamma_coeff,

			const float fCoefGray_UnsharpMask_Rationf,

		GRAYSCALE_IMAGE *sGray_Image_Initf,
		GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf)
	{

		int

			nIndexCurSizef,

			nIndexOfANeighbor_1f,
			nIntensityNeigh_1f,

			nIndexOfANeighbor_2f,
			nIntensityNeigh_2f,

			//nLenNeighf,
			//nWidNeighf,

			nWidthImagef = sGray_Image_Initf->nWidth,
			nLenImagef = sGray_Image_Initf->nLength,

			nSizeOfImagef = nWidthImagef * nLenImagef,

			nIntensityInitf = -1,

			nDiffOfIntensitiesf,
			//		nResf,
			iWidf,
			iLenf;

		float
			fDenomf,
			fTempf;
		////////////////////////////////////////////////////

		int * nGx_Arrf;
		nGx_Arrf = new int[nSizeOfImagef];

		if (nGx_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'nGx_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'nGx_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nGx_Arrf == nullptr)
//////////////////////////////////////////////
		int * nGy_Arrf;
		nGy_Arrf = new int[nSizeOfImagef];

		if (nGy_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'nGy_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'nGy_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nGy_Arrf == nullptr)

////////////////////////////////////////////////////

		int * nZx_Arrf;
		nZx_Arrf = new int[nSizeOfImagef];

		if (nZx_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'nZx_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'nZx_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nZx_Arrf == nullptr)
//////////////////////////////////////////////
		int * nZy_Arrf;
		nZy_Arrf = new int[nSizeOfImagef];

		if (nZy_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'nZy_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'nZy_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nZy_Arrf == nullptr)

//////////////////////////////////////////////////
		float * fCx_Arrf;
		fCx_Arrf = new float[nSizeOfImagef];

		if (fCx_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'fCx_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'fCx_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (... == nullptr)
//////////////////////////////////////////////
		float * fCy_Arrf;
		fCy_Arrf = new float[nSizeOfImagef];

		if (fCy_Arrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'fCy_Arrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'fCy_Arrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (... == nullptr)


#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout0, "\n\n 'Unsharp_masking_rational', nWidthImagef = %d, nLenImagef = %d, sGrayImage_ByUnsharpMaskf->nWidth = %d, sGrayImage_ByUnsharpMaskf->nLength = %d",
			nWidthImagef, nLenImagef, sGrayImage_ByUnsharpMaskf->nWidth, sGrayImage_ByUnsharpMaskf->nLength);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////////////////////////////////////////////////
//the edges are first -- they remain unchanged


#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_rational', the top: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		iWidf = 0;
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);
			sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

			nGx_Arrf[nIndexCurSizef] = 0;
			nGy_Arrf[nIndexCurSizef] = 0;

			nZx_Arrf[nIndexCurSizef] = 0;
			nZy_Arrf[nIndexCurSizef] = 0;

			fCx_Arrf[nIndexCurSizef] = 0.0;
			fCy_Arrf[nIndexCurSizef] = 0.0;
				
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	
			//fprintf(fout0, "\n The top: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
				//iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

#ifndef COMMENT_OUT_ALL_PRINTS
				//	fprintf(fout0, "\n 'Unsharp_masking_rational', the bottom: iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		iWidf = nWidthImagef - 1;
		for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);
			sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

			nGx_Arrf[nIndexCurSizef] = 0;
			nGy_Arrf[nIndexCurSizef] = 0;

			nZx_Arrf[nIndexCurSizef] = 0;
			nZy_Arrf[nIndexCurSizef] = 0;

			fCx_Arrf[nIndexCurSizef] = 0.0;
			fCy_Arrf[nIndexCurSizef] = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS	
		//	fprintf(fout0, "\n The bottom: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
			//	iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
		} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

		//fixed iLenf
		iLenf = 0;
		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);
			sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];


			nGx_Arrf[nIndexCurSizef] = 0;
			nGy_Arrf[nIndexCurSizef] = 0;

			nZx_Arrf[nIndexCurSizef] = 0;
			nZy_Arrf[nIndexCurSizef] = 0;

			fCx_Arrf[nIndexCurSizef] = 0.0;
			fCy_Arrf[nIndexCurSizef] = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS	
		//	fprintf(fout0, "\n The left: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
			//	iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		iLenf = nLenImagef - 1;
		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nLenImagef);
			sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

			nGx_Arrf[nIndexCurSizef] = 0;
			nGy_Arrf[nIndexCurSizef] = 0;

			nZx_Arrf[nIndexCurSizef] = 0;
			nZy_Arrf[nIndexCurSizef] = 0;

			fCx_Arrf[nIndexCurSizef] = 0.0;
			fCy_Arrf[nIndexCurSizef] = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS	
		//	fprintf(fout0, "\n The right: iWidf = %d, iLenf = %d, nIntensityInitf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d",
			//	iWidf, iLenf, nIntensityInitf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n 'Unsharp_masking_rational':  fGamma_coeff = %E, fK_coeff = %E, fH_coeff = %E",	fGamma_coeff,fK_coeff, fH_coeff);

		fprintf(fout0, "\n\n 'Unsharp_masking_rational': fGamma_coeff = %E, fK_coeff = %E, fH_coeff = %E", fGamma_coeff, fK_coeff, fH_coeff);

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////////////////////////////
				// use 'nIntensityThreshold' to change the number of object pixels!
		for (iWidf = 1; iWidf < nWidthImagef - 1; iWidf++)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout0, "\n 'Unsharp_masking_rational': iWidf = %d", iWidf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			for (iLenf = 1; iLenf < nLenImagef - 1; iLenf++)
			{
				nIndexCurSizef = iLenf + (iWidf*nLenImagef);

				nIntensityInitf = sGray_Image_Initf->nGrayScale_Arr[nIndexCurSizef];

				if (nIntensityInitf <= nIntensityForAoI_Min)
				{
//the background remains the same
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityInitf;
					continue;
				}//if (nIntensityInitf <= nIntensityForAoI_Min)
//////////////////////////////////////////////////////////////////////////
//for nGx_Arrf
				nIndexOfANeighbor_1f = iLenf + (iWidf + 1)*nLenImagef;

				nIntensityNeigh_1f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_1f];

				nIndexOfANeighbor_2f = iLenf + (iWidf - 1)*nLenImagef;
				nIntensityNeigh_2f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_2f];

				nDiffOfIntensitiesf = nIntensityNeigh_1f - nIntensityNeigh_2f;

				nDiffOfIntensitiesf = nDiffOfIntensitiesf * nDiffOfIntensitiesf;

				nGx_Arrf[nIndexCurSizef] = nDiffOfIntensitiesf;
//////////////////////////////////////////////////////////////////////////
//for nGy_Arrf	

				nIndexOfANeighbor_1f = (iLenf + 1) + (iWidf*nLenImagef);

				nIntensityNeigh_1f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_1f];

				nIndexOfANeighbor_2f = (iLenf - 1) + (iWidf*nLenImagef);
				nIntensityNeigh_2f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_2f];

				nDiffOfIntensitiesf = nIntensityNeigh_1f - nIntensityNeigh_2f;

				nDiffOfIntensitiesf = nDiffOfIntensitiesf * nDiffOfIntensitiesf;

				nGy_Arrf[nIndexCurSizef] = nDiffOfIntensitiesf;
//////////////////////////////////////////////////////////////////////////
//for nZx_Arrf
				nIndexOfANeighbor_1f = iLenf + (iWidf + 1)*nLenImagef;

				nIntensityNeigh_1f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_1f];

				nIndexOfANeighbor_2f = iLenf + (iWidf - 1)*nLenImagef;
				nIntensityNeigh_2f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_2f];

				nZx_Arrf[nIndexCurSizef] = 2* nIntensityInitf - nIntensityNeigh_1f - nIntensityNeigh_2f;
//////////////////////////////////////////////////////////////////////////
//for nZy_Arrf
				nIndexOfANeighbor_1f = (iLenf + 1) + (iWidf*nLenImagef);

				nIntensityNeigh_1f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_1f];

				nIndexOfANeighbor_2f = (iLenf - 1) + (iWidf*nLenImagef);
				nIntensityNeigh_2f = sGray_Image_Initf->nGrayScale_Arr[nIndexOfANeighbor_2f];

				nZy_Arrf[nIndexCurSizef] = 2 * nIntensityInitf - nIntensityNeigh_1f - nIntensityNeigh_2f;
//////////////////////////////////////////////////////////////////////////
//for fCx_Arrf
				fDenomf = (fK_coeff*(float)( nGx_Arrf[nIndexCurSizef] * nGx_Arrf[nIndexCurSizef]) ) + fH_coeff;

				fCx_Arrf[nIndexCurSizef] = (float)(nGx_Arrf[nIndexCurSizef])/(fDenomf);
	
//////////////////////////////////////////////////////////////////////////
//for fCx_Arrf
				fDenomf = (fK_coeff*(float)(nGy_Arrf[nIndexCurSizef] * nGy_Arrf[nIndexCurSizef])) + fH_coeff;

				fCy_Arrf[nIndexCurSizef] = (float)(nGy_Arrf[nIndexCurSizef]) / (fDenomf);

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// for sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef]
//fGamma_coeff
				fTempf = (float)(nZx_Arrf[nIndexCurSizef])*fCx_Arrf[nIndexCurSizef] + (float)(nZy_Arrf[nIndexCurSizef])*fCy_Arrf[nIndexCurSizef];

				sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = (int)( (fCoefGray_UnsharpMask_Rationf*(float)(nIntensityInitf) ) + (fGamma_coeff*fTempf) );

				if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] < nIntensityForAoI_Min) // 0?
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityForAoI_Min;

				if (sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] > nIntensityStatMax) // nIntensityForAoI_Max?
					sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef] = nIntensityStatMax;

#ifndef COMMENT_OUT_ALL_PRINTS	

				//if ((iWidf / 100) * 100 == iWidf && (iLenf / 100) * 100 == iLenf)
				if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
				{
					printf( "\n\n iWidf = %d, iLenf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d, nIntensityInitf = %d, fTempf = %E, fGamma_coeff = %E",
						iWidf, iLenf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef], nIntensityInitf, fTempf, fGamma_coeff);

					fprintf(fout0, "\n\n iWidf = %d, iLenf = %d, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[%d] = %d, nIntensityInitf = %d, fTempf = %E, fGamma_coeff = %E",
						iWidf, iLenf, nIndexCurSizef, sGrayImage_ByUnsharpMaskf->nGrayScale_Arr[nIndexCurSizef], nIntensityInitf, fTempf, fGamma_coeff);
					
					printf( "\n fDenomf = %E, fCx_Arrf = %E, fCy_Arrf = %E, nZx_Arrf = %d, nZy_Arrf = %d, nGx_Arrf = %d, nGy_Arrf = %d",
						fDenomf, fCx_Arrf[nIndexCurSizef], fCy_Arrf[nIndexCurSizef],
						nZx_Arrf[nIndexCurSizef], nZy_Arrf[nIndexCurSizef],
						nGx_Arrf[nIndexCurSizef], nGy_Arrf[nIndexCurSizef]);					
					
					fprintf(fout0, "\n fDenomf = %E, fCx_Arrf = %E, fCy_Arrf = %E, nZx_Arrf = %d, nZy_Arrf = %d, nGx_Arrf = %d, nGy_Arrf = %d",
							fDenomf, fCx_Arrf[nIndexCurSizef], fCy_Arrf[nIndexCurSizef],
							nZx_Arrf[nIndexCurSizef], nZy_Arrf[nIndexCurSizef],
							nGx_Arrf[nIndexCurSizef], nGy_Arrf[nIndexCurSizef]);


				} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)

#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 1; iLenf < nLenImagef - 1; iLenf++)

		} //for (iWidf = 1; iWidf < nWidthImagef - 1; iWidf++)

///////////////////////////////////////////////////////

	delete[] nGx_Arrf;
	delete[] nGy_Arrf;

	delete[] nZx_Arrf;
	delete[] nZy_Arrf;

	delete[] fCx_Arrf;
	delete[] fCy_Arrf;

 #ifndef COMMENT_OUT_ALL_PRINTS

	printf( "\n\n The end of 'Unsharp_masking_rational'");
	fprintf(fout0, "\n\n The end of 'Unsharp_masking_rational'");
	fflush(fout0);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
	} //int Unsharp_masking_rational(...

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Image preprocessing

//The reference colors at D65 (p.102, D.Sundararajan)
	int doImage_Preprocessing_1(
		const Image& image_in,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		Image& image_out)
	{

		int Initializing_Color_To_CurSize(
			const int nWidthImagef,
			const int nLenImagef,

			COLOR_IMAGE *sColor_Imagef);

		int Initializing_HLS_To_CurSize(
			const int nWidthImagef,
			const int nLenImagef,

			HLS_IMAGE *sHLS_Imagef);

		int Initializing_GRAYSCALE_To_CurSize(
			const int nWidthImagef,
			const int nLenImagef,

			GRAYSCALE_IMAGE *sGRAYSCALE_Imagef);

		int Converting_AColorImageToAGrayscale_1(

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]
			GRAYSCALE_IMAGE *sGrayscale_Imagef); //[nImageSizeMax_CoToGr]

		int RGB_Fr_Weighted_HLS_1(
			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			HLS_IMAGE *sHLS_Imagef,

			COLOR_IMAGE *sColor_Imagef);

		int HistogramEqualization(

			GRAYSCALE_IMAGE *sGrayscale_Image);

		int HistogramEqualization_BasedOnAoI(

			const int nWid_AoI_UpperLeftf, // = 298;
			const int nLen_AoI_UpperLeftf, // = 1265;

			const int nWid_AoI_BottomRightf, // = 373;
			const int nLen_AoI_BottomRightf, // = 1325;

			const int nIntensityForAoI_Minf, // = 50;
			const int nIntensityForAoI_Maxf, // = 110;

			GRAYSCALE_IMAGE *sGrayscale_Image);

		void Unsharp_masking_3x3(

			const float fCoefOfSharpening_Inside3x3f,
			const int nDiffOfIntensities_Threshold_Unsharp_maskf,

				const bool bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf,

			GRAYSCALE_IMAGE *sGray_Image_Initf,
			GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

		int Unsharp_masking_VariableRadius(

			const int nRadius_ForUnsharp_Maskf,
			const float fStDev_ForUnsharp_Maskf,
			const int nDiffOfIntensities_Threshold_Unsharp_maskf,

			const float fAmount_UnsharpMaskf,

				const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			const GRAYSCALE_IMAGE *sGray_Image_Initf,
			GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

		int Unsharp_masking_rational(

			const float fK_coeff,
			const float fH_coeff,

			const float fGamma_coeff,

				const float fCoefGray_UnsharpMask_Rationf,

			GRAYSCALE_IMAGE *sGray_Image_Initf,
			GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

		void Copying_Grayscale_To_Grayscale(

			const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
			GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);

		int MedianFilter_Image(

			const int nRadius_MedianFilterf,
			const int nDimOfArr_MedianFilterf,

			const GRAYSCALE_IMAGE *sGrayscale_Imagef,

			GRAYSCALE_IMAGE *sGrayscale_ImageByMedianFilterf);

///////////////////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		fout0 = fopen("wMain_SubimageEnhancenent.txt", "w");

		if (fout0 == NULL)
		{
			printf("\n\n fout0 == NULL");

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} //if (fout0 == NULL)

#endif //#infdef COMMENT_OUT_ALL_PRINTS

		if (fabs(sParameters_Image_Preprocessing_1f->fWeight_Convert_RedToGrayscalef + sParameters_Image_Preprocessing_1f->fWeight_Convert_GreenToGrayscalef + sParameters_Image_Preprocessing_1f->fWeight_Convert_BlueToGrayscalef - 100.0) > feps)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error: percents for conversion to the grayscale image must add up to 100 ");
			fprintf(fout0, "\n\n An error: percents for conversion to the grayscale image must add up to 100 ");

			printf("\n\n Please reassign sParameters_Image_Preprocessing_1f->fWeight_Convert_RedToGrayscale, sParameters_Image_Preprocessing_1f->fWeight_Convert_GreenToGrayscalef and sParameters_Image_Preprocessing_1f->fWeight_Convert_BlueToGrayscalef");
			fflush(fout0);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} //if (fabs(sParameters_Image_Preprocessing_1f->fWeight_Convert_RedToGrayscalef+ sParameters_Image_Preprocessing_1f->fWeight_Convert_GreenToGrayscalef + sParameters_Image_Preprocessing_1f->fWeight_Convert_BlueToGrayscalef - 100.0) > feps)

		  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		int
			nRes,

			nIndexOfPixelCur,

			iLen,
			iWid,

			nRed,
			nGreen,
			nBlue,

			nIntensity_Read_Test_ImageMax = -nLarge,

			nLenImage = image_in.width(),
			nWidthImage = image_in.height();

		int
			nDiffOfIntensitiesf = 0, //init

			//iIterForUnsharp_maskingf,

			iIters_ForMedianFilter,
			nIntensityAfterUnsharpMaskingf,

			nNumOfChangedPixelsByUnSharpMaskingTotf = 0,
			nDiffOfIntensityAndThreshf,

			nDiffOfChangedPixelsAverf = 0;


		float
			fExpOfDiff;

		////////////////////////////////////////////////////////////////////////
/*
nLenImage = image_in.width();
	nLenImage = image_in.height();
*/
	
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n nLenImage = %d, nWidthImage = %d", nLenImage, nWidthImage);
		fprintf(fout0, "\n\n nLenImage = %d, nWidthImage = %d", nLenImage, nWidthImage);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nLenImage > nLen_CoToGr_Max || nLenImage < nLen_CoToGr_Min)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in reading the image width: nLenImage = %d", nLenImage);
			printf("\n\n Please press any key to exit");

			fprintf(fout0, "\n\n An error in reading the image width: nLenImage = %d", nLenImage);
			fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (nLenImage > nLen_CoToGr_Max || nLenImage < nLen_CoToGr_Min)

		if (nWidthImage > nWid_CoToGr_Max || nWidthImage < nWid_CoToGr_Min)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in reading the image height: nWidthImage = %d", nWidthImage);
			printf("\n\n Please press any key to exit");

			fprintf(fout0, "\n\n An error in reading the image height: nWidthImage = %d", nWidthImage);
			fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (nWidthImage > nWid_CoToGr_Max || nWidthImage < nWid_CoToGr_Min)

		COLOR_IMAGE sColor_Image; //

		nRes = Initializing_Color_To_CurSize(
			nWidthImage, //const int nLenImagef,
			nLenImage, //const int nLenImagef,

			&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

		if (nRes < 0)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			return UNSUCCESSFUL_RETURN;
		} // if (nRes < 0)

		GRAYSCALE_IMAGE sGrayscale_Image;

		nRes = Initializing_GRAYSCALE_To_CurSize(
			nWidthImage, //const int nWidthImagef,
			nLenImage, //const int nLenImagef,

			&sGrayscale_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)

		if (nRes < 0)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			delete[] sGrayscale_Image.nGrayScale_Arr;
			delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

			return UNSUCCESSFUL_RETURN;
		} //if (nRes < 0)

		//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
		//for (j = 0; j < nWidthImage; j++)
		for (iWid = 0; iWid < nWidthImage; iWid++)
		{
			//for (i = 0; i < nLenImage; i++)
			for (iLen = 0; iLen < nLenImage; iLen++)
			{
				//nIndexOfPixelCur = i + j * nLenImage;
				nIndexOfPixelCur = iLen + iWid * nLenImage;

				nRed = image_in(iWid, iLen, R);
				nGreen = image_in(iWid, iLen, G);
				nBlue = image_in(iWid, iLen, B);

				if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iLen = %d, iWid = %d", nRed, nGreen, nBlue, iLen, iWid);
					fprintf(fout0, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iLen = %d, j = %d", nRed, nGreen, nBlue, iLen, iWid);
					fflush(fout0);
					//printf("\n\n Please press any key to exit");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					delete[] sColor_Image.nRed_Arr;
					delete[] sColor_Image.nGreen_Arr;
					delete[] sColor_Image.nBlue_Arr;

					delete[] sColor_Image.nLenObjectBoundary_Arr;
					delete[] sColor_Image.nIsAPixelBackground_Arr;

					delete[] sGrayscale_Image.nGrayScale_Arr;
					delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

				if (nRed > nIntensity_Read_Test_ImageMax)
					nIntensity_Read_Test_ImageMax = nRed;

				if (nGreen > nIntensity_Read_Test_ImageMax)
					nIntensity_Read_Test_ImageMax = nGreen;

				if (nBlue > nIntensity_Read_Test_ImageMax)
					nIntensity_Read_Test_ImageMax = nBlue;
#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout0, "\n nGrayScale_Arr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nGrayScale_Arr[nIndexOfPixel], i, j);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			} // for (int i = 0; i < nLenImage; i++)

		}//for (int j = 0; j < nWidthImage; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
		fprintf(fout0, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		////////////////////////////////////////////////////////////////////////////////////////////

		for (iWid = 0; iWid < nWidthImage; iWid++)
		{
			for (iLen = 0; iLen < nLenImage; iLen++)
			{
				nIndexOfPixelCur = iLen + iWid * nLenImage;

				nRed = image_in(iWid, iLen, R);
				nGreen = image_in(iWid, iLen, G);
				nBlue = image_in(iWid, iLen, B);

				
				if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
				{
					//rescaling
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
				else
				{
					// no rescaling
					sColor_Image.nRed_Arr[nIndexOfPixelCur] = (int)(sParameters_Image_Preprocessing_1f->fFadingFactorf*float(nRed));

					if (sColor_Image.nRed_Arr[nIndexOfPixelCur] > nIntensityStatMax)
						sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;

					sColor_Image.nGreen_Arr[nIndexOfPixelCur] = (int)(sParameters_Image_Preprocessing_1f->fFadingFactorf*float(nGreen)); //;

					if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] > nIntensityStatMax)
						sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;

					sColor_Image.nBlue_Arr[nIndexOfPixelCur] = (int)(sParameters_Image_Preprocessing_1f->fFadingFactorf*float(nBlue)); //;

					if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] > nIntensityStatMax)
						sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;

				} //else

				  //////////////////////////////////////////////////////////////////////////////////////////////////////
				if (sParameters_Image_Preprocessing_1f->bSELECTING_COLOR_RANGES_AT_THE_READINGf == true)
				{
					if (sColor_Image.nRed_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nRedMinf || sColor_Image.nRed_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nRedMaxf)
					{
						sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						continue;
					} // if (sColor_Image.nRed_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nRedMinf || sColor_Image.nRed_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nRedMaxf)

					if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nGreenMinf || sColor_Image.nGreen_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nGreenMaxf)
					{
						sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						continue;
					} // if (sColor_Image.nGreen_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nGreenMinf || sColor_Image.nGreen_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nGreenMaxf)

					if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nBlueMinf || sColor_Image.nBlue_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nBlueMaxf)
					{
						sColor_Image.nRed_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nIntensityStatMax;
						sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nIntensityStatMax;

						continue;
					} // if (sColor_Image.nBlue_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nBlueMinf || sColor_Image.nBlue_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nBlueMaxf)

				} // if (sParameters_Image_Preprocessing_1f->bSELECTING_COLOR_RANGES_AT_THE_READINGf == true)

#ifndef COMMENT_OUT_ALL_PRINTS
				  //fprintf(fout0, "\n nGrayScale_Arr[%d] = %d, i = %d, j = %d", nIndexOfPixel, nGrayScale_Arr[nIndexOfPixel], i, j);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS
			} // for (iLen = 0; iLen < nLenImage; iLen++)

		}//for (int j = 0; j < nWidthImage; j++)

		 //printf("\n\n 1: please press any key"); getchar();

		HLS_IMAGE
			sHLS_Image_Before_Processing;

		nRes = Initializing_HLS_To_CurSize(
			nWidthImage, //const int nLenImagef,
			nLenImage, //const int nLenImagef,
			&sHLS_Image_Before_Processing); // COLOR_IMAGE *sColor_Imagef)

		if (nRes < 0)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			delete[] sHLS_Image_Before_Processing.fHue_Arr;
			delete[] sHLS_Image_Before_Processing.fLightness_Arr;
			delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

			delete[] sGrayscale_Image.nGrayScale_Arr;
			delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

			return UNSUCCESSFUL_RETURN;
		} //if (nRes < 0)

		if (sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf == true)
		{
			nRes = RGB_Fr_Weighted_HLS_1(

				sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

				&sHLS_Image_Before_Processing, //HLS_IMAGE *sHLS_Imagef,

				&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

			if (nRes < 0)
			{
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				delete[] sHLS_Image_Before_Processing.fHue_Arr;
				delete[] sHLS_Image_Before_Processing.fLightness_Arr;
				delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

				delete[] sGrayscale_Image.nGrayScale_Arr;
				delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
				//return -1;
			} //if (nRes < 0)

		} // if (sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf == true)

		  //printf("\n\n 2: please press any key"); getchar();

		  ///////////////////////////////////////////////////////////////////////////
		nRes = Converting_AColorImageToAGrayscale_1(

			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			&sColor_Image, //COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]
			&sGrayscale_Image); // GRAYSCALE_IMAGE *sGrayscale_Imagef) //[nImageSizeMax_CoToGr]


	/////////////////////////////////////////////////////////////////////////////////
//the whole image - based
		if (sParameters_Image_Preprocessing_1f->bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf == true)
		{
			nRes = HistogramEqualization(

				&sGrayscale_Image); // GRAYSCALE_IMAGE *sGrayscale_Image)

	//printf("\n\n After 'HistogramEqualization': please press any key to continue"); fflush(fout0); getchar();

			if (nRes == UNSUCCESSFUL_RETURN)
			{
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				delete[] sHLS_Image_Before_Processing.fHue_Arr;
				delete[] sHLS_Image_Before_Processing.fLightness_Arr;
				delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

				delete[] sGrayscale_Image.nGrayScale_Arr;
				delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
			} //if (nRes == UNSUCCESSFUL_RETURN)

		} // if (sParameters_Image_Preprocessing_1f->bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf == true)

	//the AoI - based
		if (sParameters_Image_Preprocessing_1f->bIS_HISTOGRAM_EQUALIZATION_FOR_AoIs_INCLUDEDf == true)
		{
			nRes = HistogramEqualization_BasedOnAoI(

				nWid_AoI_UpperLeft, //const int nWid_AoI_UpperLeftf, // = 298;
				nLen_AoI_UpperLeft, //const int nLen_AoI_UpperLeftf, // = 1265;

				nWid_AoI_BottomRight, //const int nWid_AoI_BottomRightf, // = 373;
				nLen_AoI_BottomRight, //const int nLen_AoI_BottomRightf, // = 1325;

				nIntensityForAoI_Min, //const int nIntensityForAoI_Minf, // = 50;
				nIntensityForAoI_Max, //const int nIntensityForAoI_Maxf, // = 110;

				&sGrayscale_Image); // GRAYSCALE_IMAGE *sGrayscale_Image)

			if (nRes == UNSUCCESSFUL_RETURN)
			{
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				delete[] sHLS_Image_Before_Processing.fHue_Arr;
				delete[] sHLS_Image_Before_Processing.fLightness_Arr;
				delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

				delete[] sGrayscale_Image.nGrayScale_Arr;
				delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
			} //if (nRes == UNSUCCESSFUL_RETURN)

	//printf("\n\n After 'HistogramEqualization': please press any key to continue"); fflush(fout0); getchar();
		} // if (sParameters_Image_Preprocessing_1f->bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf == true)


		GRAYSCALE_IMAGE
			sGrayImage_ByUnsharpMaskInit;

		nRes = Initializing_GRAYSCALE_To_CurSize(
			nWidthImage, //const int nWidthImagef,
			nLenImage, //const int nLenImagef,

			&sGrayImage_ByUnsharpMaskInit); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)


		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			delete[] sHLS_Image_Before_Processing.fHue_Arr;
			delete[] sHLS_Image_Before_Processing.fLightness_Arr;
			delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

			delete[] sGrayscale_Image.nGrayScale_Arr;
			delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

			delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
			delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

			return UNSUCCESSFUL_RETURN;
		}//if (nRes == UNSUCCESSFUL_RETURN)

		GRAYSCALE_IMAGE
			sGrayImage_ByUnsharpMaskFin;

		nRes = Initializing_GRAYSCALE_To_CurSize(
			nWidthImage, //const int nWidthImagef,
			nLenImage, //const int nLenImagef,

			&sGrayImage_ByUnsharpMaskFin); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			delete[] sHLS_Image_Before_Processing.fHue_Arr;
			delete[] sHLS_Image_Before_Processing.fLightness_Arr;
			delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

			delete[] sGrayscale_Image.nGrayScale_Arr;
			delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

			delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
			delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

			delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
			delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

			return UNSUCCESSFUL_RETURN;
		}//if (nRes == UNSUCCESSFUL_RETURN)
////////////////////////////////////////////////////////////////////////////////
		GRAYSCALE_IMAGE
			sGrayscale_ImageByMedianFilter;

		nRes = Initializing_GRAYSCALE_To_CurSize(
			nWidthImage, //const int nWidthImagef,
			nLenImage, //const int nLenImagef,

			&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sGRAYSCALE_Imagef)

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			delete[] sHLS_Image_Before_Processing.fHue_Arr;
			delete[] sHLS_Image_Before_Processing.fLightness_Arr;
			delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

			delete[] sGrayscale_Image.nGrayScale_Arr;
			delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

			delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
			delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

			delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
			delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

			return UNSUCCESSFUL_RETURN;
		}//if (nRes == UNSUCCESSFUL_RETURN)
////////////////////////////////////////////////////////////////////

		if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true)
		{
			//sGrayImage_ByUnsharpMaskInit = sGrayscale_Image;

			//for (iIterForUnsharp_maskingf = 0; iIterForUnsharp_maskingf < nNumOfItersFor_Unsharp_masking_3x3; iIterForUnsharp_maskingf++)
			{
				//printf("\n\n iIterForUnsharp_maskingf = %d",iIterForUnsharp_maskingf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout0,"\n\n Applying 'Unsharp_masking_3x3': ");
				printf("\n\n Applying 'Unsharp_masking_3x3'");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				Unsharp_masking_3x3(

					sParameters_Image_Preprocessing_1f->fCoefOfSharpening_Inside3x3f, //const float fCoefOfSharpening_Inside3x3f, ,
					sParameters_Image_Preprocessing_1f->nDiffOfIntensities_Threshold_Unsharp_maskf, //const int nDiffOfIntensities_Threshold_Unsharp_maskf,

					sParameters_Image_Preprocessing_1f->bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf, //const bool bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf,

					//&sGrayImage_ByUnsharpMaskInit, //GRAYSCALE_IMAGE *sGray_Image_Initf,
					&sGrayscale_Image, //GRAYSCALE_IMAGE *sGray_Image_Initf,
					&sGrayImage_ByUnsharpMaskFin); // GRAYSCALE_IMAGE sGrayImage_ByUnsharpMaskFinf);

				//sGrayImage_ByUnsharpMaskInit = sGrayImage_ByUnsharpMaskFin;
			} // for (iIterForUnsharp_maskingf = 0; iIterForUnsharp_maskingf < nNumOfItersFor_Unsharp_masking_3x3; iIterForUnsharp_maskingf++)

		}//if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true)
		
		if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true)
		{
			//sGrayImage_ByUnsharpMaskInit = sGrayscale_Image;
			Copying_Grayscale_To_Grayscale(

				&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
				&sGrayImage_ByUnsharpMaskInit); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);


#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout0, "\n\n Applying 'Unsharp_masking_VariableRadius': ");

			printf("\n\n Applying 'Unsharp_masking_VariableRadius'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nRes = Unsharp_masking_VariableRadius(

				sParameters_Image_Preprocessing_1f->nRadius_ForUnsharp_Maskf, //const int nRadius_ForUnsharp_Maskf,
				sParameters_Image_Preprocessing_1f->fStDev_ForUnsharp_Maskf, //const float fStDev_ForUnsharp_Maskf,

				sParameters_Image_Preprocessing_1f->nDiffOfIntensities_Threshold_Unsharp_maskf, //const int nDiffOfIntensities_Threshold_Unsharp_maskf,Grayscale' 2

				sParameters_Image_Preprocessing_1f->fAmount_UnsharpMaskf, //const float fAmount_UnsharpMaskf,

					sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

				&sGrayImage_ByUnsharpMaskInit, //const GRAYSCALE_IMAGE *sGray_Image_Initf,
				//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGray_Image_Initf,

				&sGrayImage_ByUnsharpMaskFin); // GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

			if (nRes == UNSUCCESSFUL_RETURN)
			{
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				delete[] sHLS_Image_Before_Processing.fHue_Arr;
				delete[] sHLS_Image_Before_Processing.fLightness_Arr;
				delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

				delete[] sGrayscale_Image.nGrayScale_Arr;
				delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

				delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
				delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

				delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
				delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

				delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
				delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

				
				return UNSUCCESSFUL_RETURN;
			}//if (nRes == UNSUCCESSFUL_RETURN)
/*
			Copying_Grayscale_To_Grayscale(

				&sGrayImage_ByUnsharpMaskFin,  //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
				&sGrayscale_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);
*/

		} //if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true)

		
		if (sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
		{
			//sGrayImage_ByUnsharpMaskInit = sGrayscale_Image;
			Copying_Grayscale_To_Grayscale(

				&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
				&sGrayImage_ByUnsharpMaskInit); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout0, "\n\n Applying 'Unsharp_masking_rational': ");

			printf("\n\n Applying 'Unsharp_masking_rational':");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nRes = Unsharp_masking_rational(

				sParameters_Image_Preprocessing_1f->fK_constf, //const float fK_coeff,
				sParameters_Image_Preprocessing_1f->fH_constf, //const float fH_coeff,

				sParameters_Image_Preprocessing_1f->fGammaf, //const float fGamma_coeff,

					sParameters_Image_Preprocessing_1f->fCoefGray_UnsharpMask_Rationf, //const float fCoefGray_UnsharpMask_Rationf,

				&sGrayImage_ByUnsharpMaskInit, //const GRAYSCALE_IMAGE *sGray_Image_Initf,
				//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGray_Image_Initf,

				&sGrayImage_ByUnsharpMaskFin); // GRAYSCALE_IMAGE *sGrayImage_ByUnsharpMaskf);

			if (nRes == UNSUCCESSFUL_RETURN)
			{
				delete[] sColor_Image.nRed_Arr;
				delete[] sColor_Image.nGreen_Arr;
				delete[] sColor_Image.nBlue_Arr;

				delete[] sColor_Image.nLenObjectBoundary_Arr;
				delete[] sColor_Image.nIsAPixelBackground_Arr;

				delete[] sHLS_Image_Before_Processing.fHue_Arr;
				delete[] sHLS_Image_Before_Processing.fLightness_Arr;
				delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

				delete[] sGrayscale_Image.nGrayScale_Arr;
				delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

				delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
				delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

				delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
				delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

				delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
				delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

				return UNSUCCESSFUL_RETURN;
			}//if (nRes == UNSUCCESSFUL_RETURN)
	
		} //if (sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)

		if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true || 
			sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
		{
			if (sParameters_Image_Preprocessing_1f->bIS_MEDIAN_FILTER_TO_FIN_UNSHARP_IMAGE_INCLUDEDf == true)
			{
				/*
							Copying_Grayscale_To_Grayscale(

								//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

								&sGrayImage_ByUnsharpMaskFin, //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

								&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);
				*/

				for (iIters_ForMedianFilter = 0; iIters_ForMedianFilter < sParameters_Image_Preprocessing_1f->nNumofIters_ForMedianFilter_After_Unsharp_Maskf; iIters_ForMedianFilter++)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout0,"\n\n iIters_ForMedianFilter = %d", iIters_ForMedianFilter);
					printf("\n\n iIters_ForMedianFilter = %d", iIters_ForMedianFilter);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nRes = MedianFilter_Image(

						sParameters_Image_Preprocessing_1f->nRadius_MedianFilterf, //const int nRadius_MedianFilterf,
						sParameters_Image_Preprocessing_1f->nDimOfArr_MedianFilterf, //const int nDimOfArr_MedianFilterf,

						//&sGrayscale_Image, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
						&sGrayImage_ByUnsharpMaskFin, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,

						&sGrayscale_ImageByMedianFilter); // GRAYSCALE_IMAGE *sGrayscale_ImageByMedianFilterf);

					if (nRes == UNSUCCESSFUL_RETURN)
					{
						delete[] sColor_Image.nRed_Arr;
						delete[] sColor_Image.nGreen_Arr;
						delete[] sColor_Image.nBlue_Arr;

						delete[] sColor_Image.nLenObjectBoundary_Arr;
						delete[] sColor_Image.nIsAPixelBackground_Arr;

						delete[] sHLS_Image_Before_Processing.fHue_Arr;
						delete[] sHLS_Image_Before_Processing.fLightness_Arr;
						delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

						delete[] sGrayscale_Image.nGrayScale_Arr;
						delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

						delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
						delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

						delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
						delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

						delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
						delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

						return UNSUCCESSFUL_RETURN;
					}//if (nRes == UNSUCCESSFUL_RETURN)

					Copying_Grayscale_To_Grayscale(

						&sGrayscale_ImageByMedianFilter,  //const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,

	//				&sGrayscale_Image); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);
						&sGrayImage_ByUnsharpMaskFin); // GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf);

				} //for (iIters_ForMedianFilter = 0; iIters_ForMedianFilter < sParameters_Image_Preprocessing_1f->nNumofIters_ForMedianFilter_After_Unsharp_Maskf; iIters_ForMedianFilter++)

			} //if (sParameters_Image_Preprocessing_1f->bIS_MEDIAN_FILTER_TO_FIN_UNSHARP_IMAGE_INCLUDEDf == true)

		} // if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true || ...)
		  //printf("\n\n 3: please press any key"); getchar();

		  /////////////////////////////////////////////////////////////////////////////////////////////

		int bytesOfWidth = image_in.pitchInBytes();
		//int nStep = image_in.pitchInBytes() / (image_in.width() * sizeof(unsigned char));

		// Save to file
		Image imageToSave(nLenImage, nWidthImage, bytesOfWidth);

#ifndef COMMENT_OUT_ALL_PRINTS

		printf( "\n\n nLenImage = %d, imageToSave.width() = %d, nWidthImage = %d, imageToSave.height() = %d, bytesOfWidth = %d",
			nLenImage, imageToSave.width(), nWidthImage, imageToSave.height(), bytesOfWidth);

		fprintf(fout0, "\n\n nLenImage = %d, imageToSave.width() = %d, nWidthImage = %d, imageToSave.height() = %d, bytesOfWidth = %d",
			nLenImage, imageToSave.width(), nWidthImage, imageToSave.height(), bytesOfWidth);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		//for (j = 0; j < imageToSave.height(); j++)
		for (iWid = 0; iWid < nWidthImage; iWid++)
		{
			//for (i = 0; i < imageToSave.width(); i++)
			for (iLen = 0; iLen < nLenImage; iLen++)
			{
				//iLen = i;
				//iWid = j;

				nIndexOfPixelCur = iLen + iWid * nLenImage;
				//nIndexOfPixelMax = iLen + (iWid*nLen_CoToGr_Max);

				//#ifdef INCLUDE_EXP
				//adjusting 'sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur]'

				if (sParameters_Image_Preprocessing_1f->bINCLUDE_EXP_INTO_COLOR_TO_GRAYf == true)
				{
					if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == false)
					{
						nDiffOfIntensityAndThreshf = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] - sParameters_Image_Preprocessing_1f->nThreshPixelIntensityForExp_ColorToGrayf;
					} //
					else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true || 
						sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
					{
						nDiffOfIntensityAndThreshf = sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] - sParameters_Image_Preprocessing_1f->nThreshPixelIntensityForExp_ColorToGrayf;

					}//if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true)

					//fExpOfDiff = exp((float)(nDiffOfIntensityAndThreshf)*fConstForExp);
					fExpOfDiff = exp((float)(nDiffOfIntensityAndThreshf)*sParameters_Image_Preprocessing_1f->fConstForExp_ColorToGrayf);

					if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == false && sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == false &&
						sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == false)
					{
						sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = (int)((float)(sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur])*fExpOfDiff);

						if (sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] > nIntensityStatMax)
							sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					} //
					else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true ||
						sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
					{
						sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] = (int)((float)(sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur])*fExpOfDiff);

						if (sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] > nIntensityStatMax)
							sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] = nIntensityStatMax;
					}//if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || ...

				} // if (sParameters_Image_Preprocessing_1f->bINCLUDE_EXP_INTO_COLOR_TO_GRAYf == true || ...

				  //#ifdef INCLUDE_PIXEL_INTENSITY_SUBSTITUTION
				if (sParameters_Image_Preprocessing_1f->bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf == true)
				{
					if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == false && sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == false &&
						sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == false)
					{
						if (sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nPixelIntensToBeSubstituted_Minf && sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nPixelIntensToBeSubstituted_Maxf)
							sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur] = sParameters_Image_Preprocessing_1f->nPixelIntensToSubstitutef;
					} //
					else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true ||
						sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
					{
						if (sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] > sParameters_Image_Preprocessing_1f->nPixelIntensToBeSubstituted_Minf && sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] < sParameters_Image_Preprocessing_1f->nPixelIntensToBeSubstituted_Maxf)
								sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur] = sParameters_Image_Preprocessing_1f->nPixelIntensToSubstitutef;

					}//else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || ...

				} // if (sParameters_Image_Preprocessing_1f->bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf == true)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == false && sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == false && 
					sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == false)
				{
							//imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur];

				imageToSave(iWid, iLen, R) = imageToSave(iWid, iLen, G) = imageToSave(iWid, iLen, B) = sGrayscale_Image.nGrayScale_Arr[nIndexOfPixelCur];

				} //if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == false && sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == false)
				
				else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true ||
					sParameters_Image_Preprocessing_1f->bRATIONAL_UNSHARPED_MASKING_IS_APPLIED == true)
				{
					nIntensityAfterUnsharpMaskingf = sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr[nIndexOfPixelCur];
					
					imageToSave(iWid, iLen, R) = imageToSave(iWid, iLen, G) = imageToSave(iWid, iLen, B) = nIntensityAfterUnsharpMaskingf;

				}//else if (sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_3x3_INCLUDEDf == true || sParameters_Image_Preprocessing_1f->bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf == true || ...
				
				imageToSave(iWid, iLen, A) = image_in(iWid, iLen, A);
						//imageToSave(j, i, A) = image_in(j, i, A);

			} // for (iLen = 0; iLen < nLenImage; iLen++)

		} // for (iWid = 0; iWid < nWidthImage; iWid++)
  ////////////////////////////////////////////////////////////////////////////

		imageToSave.write("imageSaved_Gray.png");
							//imageToSave.write(outputFileNamef);

		image_out = imageToSave;

		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		//printf("\n\n 'doImage_Preprocessing_1' 3");

		delete[] sHLS_Image_Before_Processing.fHue_Arr;
		delete[] sHLS_Image_Before_Processing.fLightness_Arr;
		delete[] sHLS_Image_Before_Processing.fSaturation_Arr;

		//printf("\n\n 'doImage_Preprocessing_1' 4");

		delete[] sGrayscale_Image.nGrayScale_Arr;
		delete[] sGrayscale_Image.nPixel_ValidOrNotArr;

		//printf("\n\n 'doImage_Preprocessing_1' 5");

		delete[] sGrayImage_ByUnsharpMaskInit.nGrayScale_Arr;
		delete[] sGrayImage_ByUnsharpMaskInit.nPixel_ValidOrNotArr;

		delete[] sGrayImage_ByUnsharpMaskFin.nGrayScale_Arr;
		delete[] sGrayImage_ByUnsharpMaskFin.nPixel_ValidOrNotArr;

		delete[] sGrayscale_ImageByMedianFilter.nGrayScale_Arr;
		delete[] sGrayscale_ImageByMedianFilter.nPixel_ValidOrNotArr;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n fL_Min_Glob = %E, fL_Max_Glob = %E, fA_Min_Glob = %E, fA_Max_Glob = %E, fB_Min_Glob = %E, fB_Max_Glob = %E",
			fL_Min_Glob, fL_Max_Glob, fA_Min_Glob, fA_Max_Glob, fB_Min_Glob, fB_Max_Glob);

		printf("\n\n 'doImage_Preprocessing_1': the grayscale image has been saved");
		//printf("\n\n Please press any key to proceed");	getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return SUCCESSFUL_RETURN;;
		//////////////////////////////////////////////////////////////////////////
	} //int doImage_Preprocessing_1(...

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Weighted_RGB_FrInitial_RGB_Colors_1(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf) //
	{

		sWeighted_RGB_Colorsf->nRed = (int)((float)(nRedf)*sParameters_Image_Preprocessing_1f->fWeight_Redf / 100.0);

		if (sWeighted_RGB_Colorsf->nRed > 255)
			sWeighted_RGB_Colorsf->nRed = 255;

		sWeighted_RGB_Colorsf->nGreen = (int)((float)(nGreenf)*sParameters_Image_Preprocessing_1f->fWeight_Greenf / 100.0);
		if (sWeighted_RGB_Colorsf->nGreen > 255)
			sWeighted_RGB_Colorsf->nGreen = 255;

		sWeighted_RGB_Colorsf->nBlue = (int)((float)(nBluef)*sParameters_Image_Preprocessing_1f->fWeight_Bluef / 100.0);
		if (sWeighted_RGB_Colorsf->nBlue > 255)
			sWeighted_RGB_Colorsf->nBlue = 255;

		//return 1;
	} //void Weighted_RGB_FrInitial_RGB_Colors_1(...

	void Weighted_CMYK_Fr_RGB_Colors_1(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf) //
	{
		int
			nCyanf,
			nMagentaf,
			nYellowf;

		float
			fCyanf,
			fMagentaf,
			fYellowf,

			fTempf,
			fKf = fLarge; //black

		fCyanf = (float)(1.0 - ((float)(nRedf) / 255.0));

		if (fCyanf < fKf)
			fKf = fCyanf;

		fMagentaf = (float)(1.0 - ((float)(nGreenf) / 255.0));
		if (fMagentaf < fKf)
			fKf = fMagentaf;

		fYellowf = (float)(1.0 - ((float)(nBluef) / 255.0));
		if (fYellowf < fKf)
			fKf = fYellowf;
		//////////////////////////////////////////////////////////
		if (fKf < feps)
			fTempf = 1.0;
		else
			fTempf = (float)(1.0 / (1.0 - fKf));
		////////////////////////////////////////////////////////

		nCyanf = (int)(255.0*(fCyanf - fKf)*fTempf);
		if (nCyanf < 0)
			nCyanf = 0;

		if (nCyanf > 255)
			nCyanf = 255;

		nMagentaf = (int)(255.0*(fMagentaf - fKf)*fTempf);
		if (nMagentaf < 0)
			nMagentaf = 0;

		if (nMagentaf > 255)
			nMagentaf = 255;

		nYellowf = (int)(255.0*(fYellowf - fKf)*fTempf);
		if (nYellowf < 0)
			nYellowf = 0;

		if (nYellowf > 255)
			nYellowf = 255;

		//////////////////////////////////////////////////////////////////////
		sWeighted_CMYK_Colorsf->nCyan = (int)((float)(nCyanf)*sParameters_Image_Preprocessing_1f->fWeight_Cyanf / 100.0);
		if (sWeighted_CMYK_Colorsf->nCyan > 255)
			sWeighted_CMYK_Colorsf->nCyan = 255;

		sWeighted_CMYK_Colorsf->nMagenta = (int)((float)(nMagentaf)*sParameters_Image_Preprocessing_1f->fWeight_Magentaf / 100.0);
		if (sWeighted_CMYK_Colorsf->nMagenta > 255)
			sWeighted_CMYK_Colorsf->nMagenta = 255;

		sWeighted_CMYK_Colorsf->nYellow = (int)((float)(nYellowf)*sParameters_Image_Preprocessing_1f->fWeight_Yellowf / 100.0);
		if (sWeighted_CMYK_Colorsf->nYellow > 255)
			sWeighted_CMYK_Colorsf->nYellow = 255;

		sWeighted_CMYK_Colorsf->nBlack = (int)(255.0*fKf);
		//////////////////////////////////////////////////////////////////////////

		//https://www.rapidtables.com/convert/color/cmyk-to-rgb.html
		sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan = (int)((255 - sWeighted_CMYK_Colorsf->nCyan)*(1.0 - fKf));

		if (sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan > 255)
			sWeighted_CMYK_Colorsf->nRed_FrWeightedCyan = 255;

		sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta = (int)((255 - sWeighted_CMYK_Colorsf->nMagenta)*(1.0 - fKf));
		if (sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta > 255)
			sWeighted_CMYK_Colorsf->nGreen_FrWeightedMagenta = 255;

		sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow = (int)((255 - sWeighted_CMYK_Colorsf->nYellow)*(1.0 - fKf));
		if (sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow > 255)
			sWeighted_CMYK_Colorsf->nBlue_FrWeightedYellow = 255;

		//return 1;
	} //void Weighted_CMYK_Fr_RGB_Colors_1(...

	  //////////////////////////////////////////////////////////////////////////////////////////
	  //D.Sundararajan
	  //Digital Image Processing (Springer, 2017)

	void XYZ_Fr_RGB_Colors(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		XYZ_COLORS *sXYZ_Colorsf) //
	{

		sXYZ_Colorsf->fX = (float)(0.411*(float)(nRedf)+0.342*(float)(nGreenf)+0.178*(float)(nBluef));

		sXYZ_Colorsf->fY = (float)(0.222*(float)(nRedf)+0.707*(float)(nGreenf)+0.071*(float)(nBluef));

		sXYZ_Colorsf->fZ = (float)(0.020*(float)(nRedf)+0.130*(float)(nGreenf)+0.939*(float)(nBluef));

		//return 1;
	} //void XYZ_Fr_RGB_Colors(...

	void XYZ_To_RGB_Colors(
		const XYZ_COLORS *sXYZ_Colorsf,

		int &nRed_FrXYZf,
		int &nGreen_FrXYZf,
		int &nBlue_FrXYZf)
	{

		nRed_FrXYZf = (int)(3.063*sXYZ_Colorsf->fX - 1.393*sXYZ_Colorsf->fY - 0.476*sXYZ_Colorsf->fZ);

		if (nRed_FrXYZf < 0)
			nRed_FrXYZf = 0;

		if (nRed_FrXYZf > 255)
			nRed_FrXYZf = 255;

		nGreen_FrXYZf = (int)(-0.969*sXYZ_Colorsf->fX + 1.876*sXYZ_Colorsf->fY + 0.042*sXYZ_Colorsf->fZ);

		if (nGreen_FrXYZf < 0)
			nGreen_FrXYZf = 0;

		if (nGreen_FrXYZf > 255)
			nGreen_FrXYZf = 255;

		nBlue_FrXYZf = (int)(0.068*sXYZ_Colorsf->fX - 0.229*sXYZ_Colorsf->fY + 1.069*sXYZ_Colorsf->fZ);
		if (nBlue_FrXYZf < 0)
			nBlue_FrXYZf = 0;

		if (nBlue_FrXYZf > 255)
			nBlue_FrXYZf = 255;

		//return 1;
	} //void XYZ_To_RGB_Colors(...

	  ///////////////////////////////////////////////////////////////////
	  //XYZ to Lab using the standard illuminant D65 (p.162, D.Sundararajan)
	double Funct_LAB_FrXYZ(double dTf)
	{
		if (dTf > 0.008856)
			return (cbrt(dTf));
		else
			return (7.787*dTf + (16.0 / 116.0));

	} //double Funct_LAB_FrXYZ(double dTf)

	  /////////////////////////////////////////////////////////////////////

	  //Lab to XYZ using the standard illuminant D65 (p.102, D.Sundararajan)
	double Funct_YYZ_FrLAB(double dTf)
	{
		double
			dCubef = dTf * dTf*dTf;

		if (dCubef > 0.008856)
			return (dCubef);
		else
			return ((dTf - (16.0 / 116.0)) / 7.787);

	} //double Funct_YYZ_FrLAB(double dTf)
	  ///////////////////////////////////////////////////////////////////////

	void XYZ_To_Weighted_LAB_Colors_1(
		const XYZ_COLORS *sXYZ_Colorsf,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf)
	{
		double Funct_LAB_FrXYZ(double dTf);

		//Normalized
		double
			dX_Normf = sXYZ_Colorsf->fX / dX_Referf,
			dY_Normf = sXYZ_Colorsf->fY / dY_Referf,
			dZ_Normf = sXYZ_Colorsf->fZ / dZ_Referf;

		double
			dX_Primef = Funct_LAB_FrXYZ(dX_Normf),

			dY_Primef = Funct_LAB_FrXYZ(dY_Normf),
			dZ_Primef = Funct_LAB_FrXYZ(dZ_Normf);

		sWeighted_Lab_Colorsf->fL_FrXYZf = (float)(116.0*dY_Primef - 16.0);

		sWeighted_Lab_Colorsf->fA_FrXYZf = (float)(500.0*(dX_Primef - dY_Primef));

		sWeighted_Lab_Colorsf->fB_FrXYZf = (float)(200.0*(dY_Primef - dZ_Primef));

		if (sWeighted_Lab_Colorsf->fL_FrXYZf < fL_Min_Glob)
			fL_Min_Glob = sWeighted_Lab_Colorsf->fL_FrXYZf;

		if (sWeighted_Lab_Colorsf->fL_FrXYZf > fL_Max_Glob)
			fL_Max_Glob = sWeighted_Lab_Colorsf->fL_FrXYZf;
		/////////////////////////////////////////////////////////////////


		if (sWeighted_Lab_Colorsf->fA_FrXYZf < fA_Min_Glob)
			fA_Min_Glob = sWeighted_Lab_Colorsf->fA_FrXYZf;

		if (sWeighted_Lab_Colorsf->fA_FrXYZf > fA_Max_Glob)
			fA_Max_Glob = sWeighted_Lab_Colorsf->fA_FrXYZf;
		//////////////////////////////////////////////////////////////

		if (sWeighted_Lab_Colorsf->fB_FrXYZf < fB_Min_Glob)
			fB_Min_Glob = sWeighted_Lab_Colorsf->fB_FrXYZf;

		if (sWeighted_Lab_Colorsf->fB_FrXYZf > fB_Max_Glob)
			fB_Max_Glob = sWeighted_Lab_Colorsf->fB_FrXYZf;

		//////////////////////////////////////////////////////////////////

		sWeighted_Lab_Colorsf->fWeighted_L = (float)(sParameters_Image_Preprocessing_1f->fWeight_Lf*sWeighted_Lab_Colorsf->fL_FrXYZf / 100.0);

		if (sWeighted_Lab_Colorsf->fWeighted_L < 0.0)
			sWeighted_Lab_Colorsf->fWeighted_L = 0.0;

		if (sWeighted_Lab_Colorsf->fWeighted_L > 100.0)
			sWeighted_Lab_Colorsf->fWeighted_L = 100.0;

		sWeighted_Lab_Colorsf->fWeighted_A = (float)(sParameters_Image_Preprocessing_1f->fWeight_Af*sWeighted_Lab_Colorsf->fA_FrXYZf / 100.0);

		if (sWeighted_Lab_Colorsf->fWeighted_A < -128.0)
			sWeighted_Lab_Colorsf->fWeighted_A = -128.0;

		if (sWeighted_Lab_Colorsf->fWeighted_A > 127.0)
			sWeighted_Lab_Colorsf->fWeighted_A = 127.0;

		sWeighted_Lab_Colorsf->fWeighted_B = (float)(sParameters_Image_Preprocessing_1f->fWeight_Bf*sWeighted_Lab_Colorsf->fB_FrXYZf / 100.0);

		if (sWeighted_Lab_Colorsf->fWeighted_B < -128.0)
			sWeighted_Lab_Colorsf->fWeighted_B = -128.0;

		if (sWeighted_Lab_Colorsf->fWeighted_B > 127.0)
			sWeighted_Lab_Colorsf->fWeighted_B = 127.0;

		//return 1;
	} //void XYZ_To_Weighted_LAB_Colors_1(...

	  //////////////////////////////////////////////////////////////////////////////////

	void Weighted_LAB_To_XYZ_Colors(
		const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

		XYZ_COLORS *sXYZ_Colorsf)
	{
		double Funct_YYZ_FrLAB(double dTf);

		double
			dY_Primef = (sWeighted_Lab_Colorsf->fWeighted_L + 16.0) / 116.0;

		sXYZ_Colorsf->fX = (float)(dX_Referf*Funct_YYZ_FrLAB((sWeighted_Lab_Colorsf->fWeighted_A / 500.0) + dY_Primef));

		sXYZ_Colorsf->fY = (float)(dY_Referf*Funct_YYZ_FrLAB(dY_Primef));

		sXYZ_Colorsf->fZ = (float)(dZ_Referf*Funct_YYZ_FrLAB(dY_Primef - (sWeighted_Lab_Colorsf->fWeighted_B / 200.0)));

		//return 1;
	} //void Weighted_LAB_To_XYZ_Colors(...

	  ////////////////////////////////////////////////////////////////////////////////

	void RGB_Fr_Weighted_LAB_Colors_1(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		int &nRed_Fr_LABf,
		int &nGreen_Fr_LABf,
		int &nBlue_Fr_LABf)
	{

		void XYZ_Fr_RGB_Colors(
			const int nRedf,
			const int nGreenf,
			const int nBluef,

			XYZ_COLORS *sXYZ_Colorsf);

		void XYZ_To_Weighted_LAB_Colors_1(
			const XYZ_COLORS *sXYZ_Colorsf,

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf);

		void Weighted_LAB_To_XYZ_Colors(
			const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

			XYZ_COLORS *sXYZ_Colorsf);

		void XYZ_To_RGB_Colors(
			const XYZ_COLORS *sXYZ_Colorsf,

			int &nRed_FrXYZf,
			int &nGreen_FrXYZf,
			int &nBlue_FrXYZf);

		///////////////////////////////////////////////////////
		XYZ_COLORS sXYZ_Colorsf;
		WEIGHTED_LAB_COLORS sWeighted_Lab_Colorsf;
		///////////////////////////////////////////////////
		int
			nRed_FrXYZf,
			nGreen_FrXYZf,
			nBlue_FrXYZf;

		XYZ_Fr_RGB_Colors(
			nRedf, //const int nRedf,
			nGreenf, //const int nGreenf,
			nBluef, //const int nBluef,

			&sXYZ_Colorsf); // XYZ_COLORS *sXYZ_Colorsf);

		XYZ_To_Weighted_LAB_Colors_1(
			&sXYZ_Colorsf, //const XYZ_COLORS *sXYZ_Colorsf,

			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			&sWeighted_Lab_Colorsf); // WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf);

		Weighted_LAB_To_XYZ_Colors(
			&sWeighted_Lab_Colorsf, //const WEIGHTED_LAB_COLORS *sWeighted_Lab_Colorsf,

			&sXYZ_Colorsf); // XYZ_COLORS *sXYZ_Colorsf);

		XYZ_To_RGB_Colors(
			&sXYZ_Colorsf, //const XYZ_COLORS *sXYZ_Colorsf,

			nRed_FrXYZf, //int &nRed_FrXYZf,
			nGreen_FrXYZf, //int &nGreen_FrXYZf,
			nBlue_FrXYZf); // int &nBlue_FrXYZf);

		nRed_Fr_LABf = nRed_FrXYZf;

		if (nRed_Fr_LABf > 255)
			nRed_Fr_LABf = 255;

		nGreen_Fr_LABf = nGreen_FrXYZf;
		if (nGreen_Fr_LABf > 255)
			nGreen_Fr_LABf = 255;

		nBlue_Fr_LABf = nBlue_FrXYZf;
		if (nBlue_Fr_LABf > 255)
			nBlue_Fr_LABf = 255;

		//return 1;
	} // void RGB_Fr_Weighted_LAB_Colors_1(...

	  ////////////////////////////////////////////////////////////////////////////////////////////
	void Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		int &nRed_Totalf,
		int &nGreen_Totalf,
		int &nBlue_Totalf)
	{
		void Weighted_RGB_FrInitial_RGB_Colors_1(
			const int nRedf,
			const int nGreenf,
			const int nBluef,

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf);

			void Weighted_CMYK_Fr_RGB_Colors_1(
				const int nRedf,
				const int nGreenf,
				const int nBluef,

				const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
				WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf);

			void RGB_Fr_Weighted_LAB_Colors_1(
				const int nRedf,
				const int nGreenf,
				const int nBluef,

				const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
				int &nRed_Fr_LABf,
				int &nGreen_Fr_LABf,
				int &nBlue_Fr_LABf);
		////////////////////////////////////////////////////

		WEIGHTED_RGB_COLORS sWeighted_RGB_Colorsf;

		WEIGHTED_CMYK_COLORS sWeighted_CMYK_Colorsf;
		///////////////////////////////////////////////////

		int
			nRed_Fr_LABf,
			nGreen_Fr_LABf,
			nBlue_Fr_LABf;
		////////////////////////////////////////////////////////////

		nRed_Totalf = 0;
		nGreen_Totalf = 0;
		nBlue_Totalf = 0;

		if (sParameters_Image_Preprocessing_1f->fWeight_Redf < feps && sParameters_Image_Preprocessing_1f->fWeight_Greenf < feps && sParameters_Image_Preprocessing_1f->fWeight_Bluef < feps)
		{
			sWeighted_RGB_Colorsf.nRed = 0;
			sWeighted_RGB_Colorsf.nGreen = 0;
			sWeighted_RGB_Colorsf.nBlue = 0;

			goto Mark_CMYK;
		} //if (sParameters_Image_Preprocessing_1f->fWeight_Redf < feps && sParameters_Image_Preprocessing_1f->fWeight_Greenf < feps && sParameters_Image_Preprocessing_1f->fWeight_Bluef < feps)

		Weighted_RGB_FrInitial_RGB_Colors_1(
			nRedf, //const int nRedf,
			nGreenf, //const int nGreenf,
			nBluef, //const int nBluef,

			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f)
			&sWeighted_RGB_Colorsf); // WEIGHTED_RGB_COLORS *sWeighted_RGB_Colorsf);

	Mark_CMYK: if (sParameters_Image_Preprocessing_1f->fWeight_Cyanf < feps && sParameters_Image_Preprocessing_1f->fWeight_Magentaf < feps && sParameters_Image_Preprocessing_1f->fWeight_Yellowf < feps)
	{
		sWeighted_CMYK_Colorsf.nRed_FrWeightedCyan = 0;
		sWeighted_CMYK_Colorsf.nGreen_FrWeightedMagenta = 0;
		sWeighted_CMYK_Colorsf.nBlue_FrWeightedYellow = 0;

		goto Mark_Lab;
	} // Mark_CMYK: if (sParameters_Image_Preprocessing_1f->fWeight_Cyanf < feps && sParameters_Image_Preprocessing_1f->fWeight_Magentaf < feps && sParameters_Image_Preprocessing_1f->fWeight_Yellowf < feps)

			   if (sParameters_Image_Preprocessing_1f->bONLY_RGB_WEIGHTSf == false)
			   {
				   Weighted_CMYK_Fr_RGB_Colors_1(
					   nRedf, //const int nRedf,
					   nGreenf, //const int nGreenf,
					   nBluef, //const int nBluef,

					   sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

					   &sWeighted_CMYK_Colorsf); // WEIGHTED_CMYK_COLORS *sWeighted_CMYK_Colorsf);

			   Mark_Lab: if (sParameters_Image_Preprocessing_1f->fWeight_Lf < feps && sParameters_Image_Preprocessing_1f->fWeight_Af < feps && sParameters_Image_Preprocessing_1f->fWeight_Bf < feps)
			   {
				   nRed_Fr_LABf = 0;
				   nGreen_Fr_LABf = 0;
				   nBlue_Fr_LABf = 0;

				   goto Mark_Total;
			   } //Mark_Lab: if (sParameters_Image_Preprocessing_1f->fWeight_Lf < feps && sParameters_Image_Preprocessing_1f->fWeight_Af < feps && sParameters_Image_Preprocessing_1f->fWeight_Bf < feps)

						 RGB_Fr_Weighted_LAB_Colors_1(
							 nRedf, //const int nRedf,
							 nGreenf, //const int nGreenf,
							 nBluef, //const int nBluef,

							 sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
							 nRed_Fr_LABf, //int &nRed_Fr_LABf,
							 nGreen_Fr_LABf, //int &nGreen_Fr_LABf,
							 nBlue_Fr_LABf); // int &nBlue_Fr_LABf);

					 Mark_Total: nRed_Totalf = sWeighted_RGB_Colorsf.nRed + sWeighted_CMYK_Colorsf.nRed_FrWeightedCyan + nRed_Fr_LABf;

						 nGreen_Totalf = sWeighted_RGB_Colorsf.nGreen + sWeighted_CMYK_Colorsf.nGreen_FrWeightedMagenta + nGreen_Fr_LABf;

						 nBlue_Totalf = sWeighted_RGB_Colorsf.nBlue + sWeighted_CMYK_Colorsf.nBlue_FrWeightedYellow + nBlue_Fr_LABf;

			   } //if (sParameters_Image_Preprocessing_1f->bONLY_RGB_WEIGHTSf == false)


			   if (sParameters_Image_Preprocessing_1f->bONLY_RGB_WEIGHTSf == true)
			   {
				   nRed_Totalf = sWeighted_RGB_Colorsf.nRed;

				   nGreen_Totalf = sWeighted_RGB_Colorsf.nGreen;

				   nBlue_Totalf = sWeighted_RGB_Colorsf.nBlue;
			   } //  if (sParameters_Image_Preprocessing_1f->bONLY_RGB_WEIGHTSf == true)

 //return SUCCESSFUL_RETURN;
	} //void Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(...

	  //////////////////////////////////////////////////////////////////////////
	int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
		const int nRedf,
		const int nGreenf,
		const int nBluef,

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		int &nRed_Averagef,
		int &nGreen_Averagef,
		int &nBlue_Averagef)
	{
		void Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
			const int nRedf,
			const int nGreenf,
			const int nBluef,

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			int &nRed_Totalf,
			int &nGreen_Totalf,
			int &nBlue_Totalf);

		///////////////////////////////////////////////////
		int
			nNumber_Of_Valid_Color_Spaces = 3, //RGB, CMYK and Lab
			nRed_Totalf,
			nGreen_Totalf,
			nBlue_Totalf;
		////////////////////////////////////////////////////////////
		if (sParameters_Image_Preprocessing_1f->fWeight_Redf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for RGB Red: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for RGB Red: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Redf < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Greenf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for RGB Green: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for RGB Green: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} //if (sParameters_Image_Preprocessing_1f->fWeight_Greenf < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Bluef < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for RGB Blue: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for RGB Blue: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Bluef < 0.0)

		  //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (sParameters_Image_Preprocessing_1f->fWeight_Cyanf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Cyan: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Cyan: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Cyanf < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Magentaf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Magenta: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Magenta: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Magentaf < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Yellowf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Yellow: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Yellow: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Yellowf < 0.0)

		  //////////////////////////////////////////////////////////////////////////////////////////////////////
		if (sParameters_Image_Preprocessing_1f->fWeight_Lf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Lab L: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Lab L: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Lf < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Af < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Lab A: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Lab A: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Af < 0.0)

		if (sParameters_Image_Preprocessing_1f->fWeight_Bf < 0.0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error for Lab B: a weight can not be less than zero");
			fprintf(fout0, "\n\n An error for Lab B: a weight can not be less than zero");

			printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (sParameters_Image_Preprocessing_1f->fWeight_Bf < 0.0)

		  //////////////////////////////////////////////////////////////////////////////////////////////////
		if (sParameters_Image_Preprocessing_1f->fWeight_Redf < feps && sParameters_Image_Preprocessing_1f->fWeight_Greenf < feps && sParameters_Image_Preprocessing_1f->fWeight_Bluef < feps)
			nNumber_Of_Valid_Color_Spaces -= 1;

		if (sParameters_Image_Preprocessing_1f->fWeight_Cyanf < feps && sParameters_Image_Preprocessing_1f->fWeight_Magentaf < feps && sParameters_Image_Preprocessing_1f->fWeight_Yellowf < feps)
			nNumber_Of_Valid_Color_Spaces -= 1;

		if (sParameters_Image_Preprocessing_1f->fWeight_Lf < feps && sParameters_Image_Preprocessing_1f->fWeight_Af < feps && sParameters_Image_Preprocessing_1f->fWeight_Bf < feps)
			nNumber_Of_Valid_Color_Spaces -= 1;

		if (nNumber_Of_Valid_Color_Spaces <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error: no color space is selected; all weights are equal to zero ");
			fprintf(fout0, "\n\n An error: no color space is selected; all weights are equal to zero ");

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} // if (nNumber_Of_Valid_Color_Spaces <= 0)
		  //////////////////////////////////////////////////////////////////////////////////////

		Total_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
			nRedf, //const int nRedf,
			nGreenf, //const int nGreenf,
			nBluef, //const int nBluef,

			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			nRed_Totalf, //int &nRed_Totalf,
			nGreen_Totalf, //int &nGreen_Totalf,
			nBlue_Totalf); // int &nBlue_Totalf);

		nRed_Averagef = (int)((float)(nRed_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));

		nGreen_Averagef = (int)((float)(nGreen_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));
		nBlue_Averagef = (int)((float)(nBlue_Totalf) / (float)(nNumber_Of_Valid_Color_Spaces));

		return SUCCESSFUL_RETURN;;
	} //int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(...

	  //////////////////////////////////////////////////////////////////////
	int Converting_AColorImageToAGrayscale_1(

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

		COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]
		GRAYSCALE_IMAGE *sGrayscale_Imagef) //[nImageSizeMax_CoToGr]
	{
		int Initializing_HLS_To_CurSize(
			const int nWidthImagef,
			const int nLenImagef,

			HLS_IMAGE *sHLS_Imagef);

		int Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
			const int nRedf,
			const int nGreenf,
			const int nBluef,
			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

			int &nRed_Averagef,
			int &nGreen_Averagef,
			int &nBlue_Averagef);

		int RGB_Fr_Weighted_HLS_1(

			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			HLS_IMAGE *sHLS_Imagef,

			COLOR_IMAGE *sColor_Imagef);


		int
			nResf,
			nIndexOfPixelCurf,
			iWidf,
			iLenf,
			nRedf,
			nGreenf,
			nBluef,

			nRed_Averagef = 0,
			nGreen_Averagef = 0,
			nBlue_Averagef = 0,

			nGrayIntensityf;

		sGrayscale_Imagef->nWidth = sColor_Imagef->nWidth;
		sGrayscale_Imagef->nLength = sColor_Imagef->nLength;

		if (sParameters_Image_Preprocessing_1f->bJUST_COPY_PIXEL_INTENSITIES_FOR_GRAY_INPUT_IMAGEf == true)
		{

			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLen_CoToGr_Max);

					nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
					nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
					nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

					nGrayIntensityf = (int)( ( (float)(nRedf) + (float)(nGreenf) + (float)(nBluef)) /3.0 );

					sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = nGrayIntensityf;

					sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1;

				} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
			} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

			return SUCCESSFUL_RETURN;
		}//if (sParameters_Image_Preprocessing_1f->bJUST_COPY_PIXEL_INTENSITIES_FOR_GRAY_INPUT_IMAGEf == true)

		if (sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == false)
		{
			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLen_CoToGr_Max);

					nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
					nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
					nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

					nResf = Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
						nRedf, //const int nRedf,
						nGreenf, //const int nGreenf,
						nBluef, //const int nBluef,
						sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

						nRed_Averagef, //int &nRed_Averagef,
						nGreen_Averagef, //int &nGreen_Averagef,
						nBlue_Averagef); // int &nBlue_Averagef);

					if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);
						fprintf(fout0, "\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);

						fprintf(fout0, "\n\n nRed_Averagef = %d, nGreen_Averagef = %d, nBlue_Averagef = %d", nRed_Averagef, nGreen_Averagef, nBlue_Averagef);
						printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
						//getchar(); exit(1);
					} // if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)

					nGrayIntensityf = (int)(((float)(nRed_Averagef)*sParameters_Image_Preprocessing_1f->fWeight_Convert_RedToGrayscalef / 100.0) + ((float)(nGreen_Averagef)*sParameters_Image_Preprocessing_1f->fWeight_Convert_GreenToGrayscalef / 100.0) +
						((float)(nBlue_Averagef)*sParameters_Image_Preprocessing_1f->fWeight_Convert_BlueToGrayscalef / 100.0));

					sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = nGrayIntensityf;

					sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					if (iWidf >= nWid_AoI_UpperLeft && iWidf <= nWid_AoI_BottomRight && iLenf >= nLen_AoI_UpperLeft && iLenf <= nLen_AoI_BottomRight)
					{
						fprintf(fout0, "\n\n 'Converting_AColorImageToAGrayscale_1' 1: sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = %d, iWidf = %d, iLenf = %d",
							sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf], iWidf, iLenf);
						fprintf(fout0, "\n nRedf = %d, nRed_Averagef = %d, nGreenf = %d, nGreen_Averagef = %d, nBluef = %d, nBlue_Averagef = %d", 
							nRedf, nRed_Averagef, nGreenf,nGreen_Averagef, nBluef,nBlue_Averagef);

					} // if (iWidf >= nWid_AoI_UpperLeft && iWidf <= nWid_AoI_BottomRight && iLenf >= nLen_AoI_UpperLeft && iLenf <= nLen_AoI_BottomRight)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


				} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
			} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		} // if ( sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == false)

		/////////////////////////////////////////////////////////////////////////
		HLS_IMAGE
			sHLS_ImageForAveragedColorsf;

		nResf = Initializing_HLS_To_CurSize(
			sGrayscale_Imagef->nWidth, //const int nWidthImagef,
			sGrayscale_Imagef->nLength, //const int nLenImagef,

			&sHLS_ImageForAveragedColorsf); // COLOR_IMAGE *sColor_Imagef);

		if (nResf < 0)
		{
			return UNSUCCESSFUL_RETURN;
			//return -1;
		} //if (nResf < 0)

		if (sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == true)
		{

			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLen_CoToGr_Max);

					nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
					nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
					nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

					nResf = Average_OnePixel_RGB_Fr_Weighted_RGB_CMYK_LAB_Colors_1(
						nRedf, //const int nRedf,
						nGreenf, //const int nGreenf,
						nBluef, //const int nBluef,
						sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

						nRed_Averagef, //int &nRed_Averagef,
						nGreen_Averagef, //int &nGreen_Averagef,
						nBlue_Averagef); // int &nBlue_Averagef);

					if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);
						fprintf(fout0, "\n\n An error: wrong values for the averaged colors, the pixel iWidf = %d, iLenf = %d", iWidf, iLenf);

						fprintf(fout0, "\n\n nRed_Averagef = %d, nGreen_Averagef = %d, nBlue_Averagef = %d", nRed_Averagef, nGreen_Averagef, nBlue_Averagef);
						printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
						//getchar(); exit(1);
					} // if (nRed_Averagef < 0 || nRed_Averagef > 255 || nGreen_Averagef < 0 || nGreen_Averagef > 255 || nBlue_Averagef < 0 || nBlue_Averagef > 255)

					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nRed_Averagef;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nGreen_Averagef;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nBlue_Averagef;

				} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
			} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

			nResf = RGB_Fr_Weighted_HLS_1(
				sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

				&sHLS_ImageForAveragedColorsf, //HLS_IMAGE *sHLS_Imagef,

				sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

			if (nResf < 0)
			{
				return UNSUCCESSFUL_RETURN;
			} //if (nResf < 0)
			  /////////////////////////////////////////////////////////////////////


			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLen_CoToGr_Max);

					nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
					nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
					nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

					nGrayIntensityf = (int)(((float)(nRedf)*sParameters_Image_Preprocessing_1f->fWeight_Convert_RedToGrayscalef / 100.0) + ((float)(nGreenf)*sParameters_Image_Preprocessing_1f->fWeight_Convert_GreenToGrayscalef / 100.0) +
						((float)(nBluef)*sParameters_Image_Preprocessing_1f->fWeight_Convert_BlueToGrayscalef / 100.0));

					sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = nGrayIntensityf;

					sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1;

				} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
			} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		} //if (sParameters_Image_Preprocessing_1f->bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf == true)

		delete[] sHLS_ImageForAveragedColorsf.fHue_Arr;
		delete[] sHLS_ImageForAveragedColorsf.fLightness_Arr;
		delete[] sHLS_ImageForAveragedColorsf.fSaturation_Arr;

		return SUCCESSFUL_RETURN;
	} //int Converting_AColorImageToAGrayscale_1(...

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int HistogramEqualization(

		GRAYSCALE_IMAGE *sGrayscale_Image)
	{
		int
			nNumOfValidPixelsTotf = 0,

			nLookUptableArrf[nNumOfHistogramBinsStat_CoToGr],
			nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr],

			nCumulatNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr],

			nWidthImagef = sGrayscale_Image->nWidth,
			nLenImagef = sGrayscale_Image->nLength,

			nIndexOfPixelCurf,

			nIntensityCurf = nLarge,

			nIntensityMinf = nLarge,
			nIntensityMaxf = -nLarge,

			iIntensityf,
			iWidf,
			iLenf;

		float
			fCumulProbOverIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr];

		for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{
			nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;
			nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

			fCumulProbOverIntensitiesArrf[iIntensityf] = 0.0;

			nLookUptableArrf[iIntensityf] = 0;

		} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
		fprintf(fout0, "\n\n Equalization: nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

		//printf("\n\n Please press any key to continue:");

		//fflush(fout0); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		
		//////////////////////////////////////////////////////////////////////////////
		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLen_CoToGr_Max);

				if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error: nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);

					fprintf(fout0, "\n\n An error:  nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);
					printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} // if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)

				if (sGrayscale_Image->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
					continue;

				nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

				if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

					fprintf(fout0, "\n\n An error: the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
					printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)

				if (nIntensityCurf < nIntensityMinf)
					nIntensityMinf = nIntensityCurf;

				if (nIntensityCurf > nIntensityMaxf)
					nIntensityMaxf = nIntensityCurf;

				nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

			} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)


		for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{

			nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] += nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf - 1] +
				nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

			nNumOfValidPixelsTotf += nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

		} // for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
		fprintf(fout0, "\n\n Equalization: nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//printf("\n\n Please press any key to continue:");	fflush(fout0); getchar();

		if (nNumOfValidPixelsTotf <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error: the number of valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);
			fprintf(fout0, "\n\n An error: the number  valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} //if (nNumOfValidPixelsTotf <= 0)

		for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{
			fCumulProbOverIntensitiesArrf[iIntensityf] = (float)(nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);

			nLookUptableArrf[iIntensityf] = (int)((fCumulProbOverIntensitiesArrf[iIntensityf])*(float)(nIntensityStatMax));

			if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in HistogramEqualization: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);
				fprintf(fout0, "\n\n An error in HistogramEqualization: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);

				printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
				//getchar(); exit(1);
			} // if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
			fprintf(fout0, "\n\n Equalization: nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

		  //printf("\n\n Please press any key to continue:");	fflush(fout0); getchar();
		  ///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		  //printf( "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
		  //fprintf(fout0, "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLen_CoToGr_Max);

				if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error 2: nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);

					fprintf(fout0, "\n\n An error 2:  nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);
					printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} // if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)

				if (sGrayscale_Image->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
					continue;

				nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

				sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] = nLookUptableArrf[nIntensityCurf];
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout0, "\n\n iWidf = %d, iLenf = %d, sGrayscale_Image->nGrayScale_Arr[%d] = %d",
					iWidf, iLenf, nIndexOfPixelCurf, sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf]);

				fprintf(fout0, "\n nIntensityCurf = %d, nLookUptableArrf[%d] = %d", nIntensityCurf, nIntensityCurf, nLookUptableArrf[nIntensityCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n The end of 'HistogramEqualization': please press any key to continue"); fflush(fout0); getchar();

		printf("\n\n The end of 'HistogramEqualization'"); fflush(fout0); 
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return SUCCESSFUL_RETURN;;
	} //int HistogramEqualization(....
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int HistogramEqualization_BasedOnAoI(

		const int nWid_AoI_UpperLeftf, // = 298;
		const int nLen_AoI_UpperLeftf, // = 1265;

		const int nWid_AoI_BottomRightf, // = 373;
		const int nLen_AoI_BottomRightf, // = 1325;

		const int nIntensityForAoI_Minf, // = 20; //50;
		const int nIntensityForAoI_Maxf, // = 180//110;

		GRAYSCALE_IMAGE *sGrayscale_Image)
	{
		int
			nNumOfValidPixelsTotf = 0,

			nLookUptableArrf[nNumOfHistogramBinsStat_CoToGr],
			nNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr],

			nCumulatNumOfValidPixelsForIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr],

			nWidthImagef = sGrayscale_Image->nWidth,
			nLenImagef = sGrayscale_Image->nLength,

			nIndexOfPixelCurf,

			nIntensityCurf = nLarge,

			nIntensityMinf = nLarge,
			nIntensityMaxf = -nLarge,

			iIntensityf,
			iWidf,
			iLenf;

		float
			fCumulProbOverIntensitiesArrf[nNumOfHistogramBinsStat_CoToGr];

			if (nWid_AoI_UpperLeftf >= nWid_AoI_BottomRightf || nWid_AoI_UpperLeftf >= nWidthImagef)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': nWid_AoI_UpperLeftf = %d >= nWid_AoI_BottomRightf = %d, ...", nWid_AoI_UpperLeftf, nWid_AoI_BottomRightf);

				fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI': nWid_AoI_UpperLeftf = %d >= nWid_AoI_BottomRightf = %d, ...", nWid_AoI_UpperLeftf, nWid_AoI_BottomRightf);
				printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nWid_AoI_UpperLeftf >= nWid_AoI_BottomRightf || ...)

			if (nLen_AoI_UpperLeftf >= nLen_AoI_BottomRightf || nLen_AoI_UpperLeftf >= nLenImagef)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': nLen_AoI_UpperLeftf = %d >= nLen_AoI_BottomRightf = %d, ...", nLen_AoI_UpperLeftf, nLen_AoI_BottomRightf);

				fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI': nLen_AoI_UpperLeftf = %d >= nLen_AoI_BottomRightf = %d, ...", nLen_AoI_UpperLeftf, nLen_AoI_BottomRightf);
				printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLen_AoI_UpperLeftf >= nLen_AoI_BottomRightf || ...)

			if (nIntensityForAoI_Minf >= nIntensityForAoI_Maxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': nIntensityForAoI_Minf = %d >= nIntensityForAoI_Maxf = %d", nIntensityForAoI_Minf, nIntensityForAoI_Maxf);

				fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI': nIntensityForAoI_Minf = %d >= nIntensityForAoI_Maxf = %d", nIntensityForAoI_Minf, nIntensityForAoI_Maxf);
				printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nIntensityForAoI_Minf >= nIntensityForAoI_Maxf)
/////////////////////////////////////////////////////////////////////////////////////////////
		for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{
			nNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;
			nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] = 0;

			fCumulProbOverIntensitiesArrf[iIntensityf] = 0.0;

			nLookUptableArrf[iIntensityf] = 0;

		} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);
		fprintf(fout0, "\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nWidthImagef = %d, nLenImagef = %d", nWidthImagef, nLenImagef);

		//printf("\n\n Please press any key to continue:");

		//fflush(fout0); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


		//////////////////////////////////////////////////////////////////////////////
		//for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		for (iWidf = nWid_AoI_UpperLeftf; iWidf <= nWid_AoI_BottomRightf; iWidf++)
		{
			for (iLenf = nLen_AoI_UpperLeftf; iLenf <= nLen_AoI_BottomRightf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLen_CoToGr_Max);

				if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);

					fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI':  nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);
					printf("\n\n Please press any key to exit:"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} // if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout0, "\n 'HistogramEqualization_BasedOnAoI': sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] = %d, iWidf = %d, iLenf = %d",
					sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf], iWidf, iLenf);

				fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] < nIntensityForAoI_Minf || sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] > nIntensityForAoI_Maxf)
					continue;

				nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

				if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);

					fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI': the grayscale intensity nIntensityCurf = %d", nIntensityCurf);
					printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} //if (nIntensityCurf < nIntensityStatMinForEqualization || nIntensityCurf >= nNumOfHistogramBinsStat_CoToGr)

				if (nIntensityCurf < nIntensityMinf)
					nIntensityMinf = nIntensityCurf;

				if (nIntensityCurf > nIntensityMaxf)
					nIntensityMaxf = nIntensityCurf;

				nNumOfValidPixelsForIntensitiesArrf[nIntensityCurf] += 1;

			} //for (iLenf = nLen_AoI_UpperLeftf; iLenf <= nLen_AoI_BottomRightf; iLenf++)
		} //for (iWidf = nWid_AoI_UpperLeftf; iWidf <= nWid_AoI_BottomRightf; iWidf++)

		for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{

			nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf] += nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf - 1] +
				nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

			nNumOfValidPixelsTotf += nNumOfValidPixelsForIntensitiesArrf[iIntensityf];

		} // for (iIntensityf = 1; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
		fprintf(fout0, "\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nIntensityMinf = %d, nIntensityMaxf = %d, nNumOfValidPixelsTotf = %d", nIntensityMinf, nIntensityMaxf, nNumOfValidPixelsTotf);
		fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//printf("\n\n Please press any key to continue:");	fflush(fout0); getchar();

		if (nNumOfValidPixelsTotf <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'HistogramEqualization_BasedOnAoI': the number of valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);
			fprintf(fout0, "\n\n An error in 'HistogramEqualization_BasedOnAoI': the number  valid pixels nNumOfValidPixelsTotf = %d is wrong", nNumOfValidPixelsTotf);

			printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
			//getchar(); exit(1);
		} //if (nNumOfValidPixelsTotf <= 0)

		for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)
		{
			fCumulProbOverIntensitiesArrf[iIntensityf] = (float)(nCumulatNumOfValidPixelsForIntensitiesArrf[iIntensityf]) / (float)(nNumOfValidPixelsTotf);

			nLookUptableArrf[iIntensityf] = (int)((fCumulProbOverIntensitiesArrf[iIntensityf])*(float)(nIntensityStatMax));

			if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in HistogramEqualization_BasedOnAoI: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);
				fprintf(fout0, "\n\n An error in HistogramEqualization_BasedOnAoI: nLookUptableArrf[%d] = %d > nIntensityStatMax = %d is wrong", iIntensityf, nLookUptableArrf[iIntensityf], nIntensityStatMax);

				printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
				//getchar(); exit(1);
			} // if (nLookUptableArrf[iIntensityf] > nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
			fprintf(fout0, "\n\n Equalization in 'HistogramEqualization_BasedOnAoI': nLookUptableArrf[%d] = %d", iIntensityf, nLookUptableArrf[iIntensityf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} // for (iIntensityf = 0; iIntensityf < nNumOfHistogramBinsStat_CoToGr; iIntensityf++)

		  //printf("\n\n Please press any key to continue:");	fflush(fout0); getchar();
		  ///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
		  //printf( "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
		  //fprintf(fout0, "\n\n nLookUptableArrf[0] = %d, nLookUptableArrf[51] = %d", nLookUptableArrf[0], nLookUptableArrf[51]);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef); // nLen_CoToGr_Max);

				if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error 2 in 'HistogramEqualization_BasedOnAoI': nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);

					fprintf(fout0, "\n\n An error 2 in 'HistogramEqualization_BasedOnAoI':  nIndexOfPixelCurf = %d >= nImageSizeMax_CoToGr = %d", nIntensityCurf, nImageSizeMax_CoToGr);
					printf("\n\n Please press any key to exit:");	fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
					//getchar(); exit(1);
				} // if (nIndexOfPixelCurf >= nImageSizeMax_CoToGr)

				if (sGrayscale_Image->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
					continue;

				nIntensityCurf = sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf];

				sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf] = nLookUptableArrf[nIntensityCurf];
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout0, "\n\n iWidf = %d, iLenf = %d, sGrayscale_Image->nGrayScale_Arr[%d] = %d",
					iWidf, iLenf, nIndexOfPixelCurf, sGrayscale_Image->nGrayScale_Arr[nIndexOfPixelCurf]);

				fprintf(fout0, "\n nIntensityCurf = %d, nLookUptableArrf[%d] = %d", nIntensityCurf, nIntensityCurf, nLookUptableArrf[nIntensityCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} //for (iLenf = 0; iLenf <nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n The end of 'HistogramEqualization_BasedOnAoI': please press any key to continue"); fflush(fout0); getchar();

		printf("\n\n The end of 'HistogramEqualization_BasedOnAoI'"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return SUCCESSFUL_RETURN;;
	} //int HistogramEqualization_BasedOnAoI(....

///////////////////////////////////////////////////////////////////////////

	  //From Wikipedia, https://en.wikipedia.org/wiki/HSL_and_HSV
	  //From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
	  // Alternative: Practical algorithms for image analysis, Michael Seul and others, p.53

	int HLS_Fr_RGB(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]

		HLS_IMAGE *sHLS_Imagef)
	{
		int
			nLenImagef = sColor_Imagef->nLength,
			nWidthImagef = sColor_Imagef->nWidth,

			//nIndexCurSizef, // = iLenf + (iWidf*nLenImagef);
			nIndexOfPixelCurf,
			nRedCurf,
			nGreenCurf,
			nBlueCurf,

			nColorMinf,
			nColorMaxf,

			iLenf,
			iWidf;

		float
			fCoefOfPixelIntensNormalizationf = (float)(1.0 / (float)(nIntensityStatMax)),

			//fHueNotNormalisedArrf[nImageSizeMax_CoToGr],

			//fPixelIntensitiesNormalizedArrf[nImageSizeMax_CoToGr],

			fOnePixelIntensNormalizedMinf,
			fOnePixelIntensNormalizedMaxf,

			fOnePixelIntensNormalizedRangef,

			fUNDEFINEDF = fHueUndefinedValue, //-1.0,

			fDiffCurf,

			fLightnessCurf,
			fSaturationCurf,
			fHueCurf,

			fRedNormCurf,
			fGreenNormCurf,
			fBlueNormCurf;

		/////////////////////////////////////////////////////////////////////////////////////////////

		sHLS_Imagef->nLength = nLenImagef;
		sHLS_Imagef->nWidth = nWidthImagef;

		//printf("\n\n An error in 'HLS_Fr_RGB': please choose only one way to calulate Hue");

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			//for (iLenf = 8; iLenf < nLenImagef; iLenf++)
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				//nIndexOfPixelCurf = iLenf + (iWidf*nLen_CoToGr_Max);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				fRedNormCurf = (float)(nRedCurf)*fCoefOfPixelIntensNormalizationf;

				fGreenNormCurf = (float)(nGreenCurf)*fCoefOfPixelIntensNormalizationf;
				fBlueNormCurf = (float)(nBlueCurf)*fCoefOfPixelIntensNormalizationf;
#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout0, "\n\n 'HLS_Fr_RGB': nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", nRedCurf, nGreenCurf, nBlueCurf);
				//fprintf(fout0, "\n iWidf = %d, iLenf = %d, fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", iWidf, iLenf, fRedNormCurf, fGreenNormCurf, fBlueNormCurf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				//min
				if (nRedCurf <= nGreenCurf)
				{
					if (nRedCurf <= nBlueCurf)
					{
						nColorMinf = nRedCurf;
						fOnePixelIntensNormalizedMinf = fRedNormCurf;
					} // if (nRedCurf <= nBlueCurf)
					else
					{
						nColorMinf = nBlueCurf;
						fOnePixelIntensNormalizedMinf = fBlueNormCurf;

					} //else
				} // if (nRedCurf <= nGreenCurf)
				else if (nRedCurf > nGreenCurf)
				{
					if (nGreenCurf <= nBlueCurf)
					{
						nColorMinf = nGreenCurf;
						fOnePixelIntensNormalizedMinf = fGreenNormCurf;
					} // if (nGreenCurf <= nBlueCurf)
					else
					{
						nColorMinf = nBlueCurf;
						fOnePixelIntensNormalizedMinf = fBlueNormCurf;
					} // else

				} // else if (nRedCurf > nGreenCurf)

				  //Max
				if (nRedCurf >= nGreenCurf)
				{
					if (nRedCurf >= nBlueCurf)
					{
						nColorMaxf = nRedCurf;
						fOnePixelIntensNormalizedMaxf = fRedNormCurf;
					} // if (nRedCurf >= nBlueCurf)
					else
					{
						nColorMaxf = nBlueCurf;
						fOnePixelIntensNormalizedMaxf = fBlueNormCurf;

					} //else
				} // if (nRedCurf >= nGreenCurf)
				else if (nRedCurf < nGreenCurf)
				{
					if (nGreenCurf >= nBlueCurf)
					{
						nColorMaxf = nGreenCurf;
						fOnePixelIntensNormalizedMaxf = fGreenNormCurf;
					} // if (nGreenCurf >= nBlueCurf)
					else
					{
						nColorMaxf = nBlueCurf;
						fOnePixelIntensNormalizedMaxf = fBlueNormCurf;
					} // else

				} // else if (nRedCurf < nGreenCurf)

				  /////////////////////////////////////////////////////////////////////////////////////////////
				fLightnessCurf = (float)((fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf) / 2.0);

				if (nColorMinf == nColorMaxf)
				{
					fSaturationCurf = 0.0;
					fHueCurf = fUNDEFINEDF;
				} // if (nColorMinf == nColorMaxf)
				else
				{

					fOnePixelIntensNormalizedRangef = fOnePixelIntensNormalizedMaxf - fOnePixelIntensNormalizedMinf;

					//if (fOnePixelIntensNormalizedRangef <= 0.0 || fOnePixelIntensNormalizedRangef > fLarge)
					if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)
					{
						//fOnePixelIntensNormalizedRangef = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);
						fprintf(fout0, "\n\n An error in 'HLS_Fr_RGB': fOnePixelIntensNormalizedRangef = %E <= 0.0 || ...", fOnePixelIntensNormalizedRangef);

						printf("\n\n Please press any key to exit"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
						//getchar(); exit(1);
					} //if (fOnePixelIntensNormalizedRangef <= -feps || fOnePixelIntensNormalizedRangef > fLarge)

					  //saturation
					if (fLightnessCurf < 0.5)
					{
						fSaturationCurf = fOnePixelIntensNormalizedRangef / (fOnePixelIntensNormalizedMinf + fOnePixelIntensNormalizedMaxf);
					} // if (fLightnessCurf < 0.5)
					else // fLightnessCurf >= 0.5
					{
						fDiffCurf = (float)(2.0 - fOnePixelIntensNormalizedMinf - fOnePixelIntensNormalizedMaxf);

						if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)
						{
							fDiffCurf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
							printf("\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);
							fprintf(fout0, "\n\n An error in 'HLS_Fr_RGB': fDiffCurf = %E <= 0.0 || ...", fDiffCurf);

							printf("\n\n Please press any key to exit"); fflush(fout0);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							return UNSUCCESSFUL_RETURN;
							//getchar(); exit(1);
						} //if (fDiffCurf <= 0.0 || fDiffCurf > fLarge)

						fSaturationCurf = fOnePixelIntensNormalizedRangef / fDiffCurf;
					}// else // if (fLightnessCurf >= 0.5)

					 //hue
					if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
					{
						//fHueCurf = (fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef;
						fHueCurf = (float)(fmod((fGreenNormCurf - fBlueNormCurf) / fOnePixelIntensNormalizedRangef, 6.0));

					}// if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
					else // if (fRedNormCurf == fOnePixelIntensNormalizedMaxf)
					{

						if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
						{
							fHueCurf = (float)(2.0 + (fBlueNormCurf - fRedNormCurf) / fOnePixelIntensNormalizedRangef);

						}// if (fGreenNormCurf == fOnePixelIntensNormalizedMaxf)
						else
						{
							if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
							{
								fHueCurf = (float)(4.0 + (fRedNormCurf - fGreenNormCurf) / fOnePixelIntensNormalizedRangef);

							}// if (fBlueNormCurf == fOnePixelIntensNormalizedMaxf)
							else
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								printf("\n\n An error in 'HLS_Fr_RGB': a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
								printf("\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

								fprintf(fout0, "\n\n An error in 'HLS_Fr_RGB':  a mismatch between fOnePixelIntensNormalizedMaxf = %E and the colors", fOnePixelIntensNormalizedMaxf);
								fprintf(fout0, "\n fRedNormCurf = %E, fGreenNormCurf = %E, fBlueNormCurf = %E", fRedNormCurf, fGreenNormCurf, fBlueNormCurf);

								fprintf(fout0, "\n iWidf = %d, nWidthImagef = %d, iLenf = %d, nLenImagef = %d", iWidf, nWidthImagef, iLenf, nLenImagef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								return UNSUCCESSFUL_RETURN;
								//getchar(); exit(1);
							}// else

						} // else if (fGreenNormCurf != fOnePixelIntensNormalizedMaxf)

					} // else if (fRedNormCurf != fOnePixelIntensNormalizedMaxf)

					fHueCurf = (float)(fHueCurf*60.0);

					if (fHueCurf < 0.0)
						fHueCurf = (float)(fHueCurf + 360.0);

				}// else if (nColorMinf != nColorMaxf)

				sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = fHueCurf;
				sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = fLightnessCurf;
				sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = fSaturationCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
				if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
				{
					fprintf(fout0, "\n\n 'HLS_Fr_RGB': iWidf = %d, iLenf = %d", iWidf, iLenf);
					fprintf(fout0, "\n fHueCurf = %E, fLightnessCurf = %E, fSaturationCurf = %E", fHueCurf, fLightnessCurf, fSaturationCurf);
				} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} // for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		return SUCCESSFUL_RETURN;;
	} // int HLS_Fr_RGB(...

	  ///////////////////////////////////////////////////////////////////////////
	  //I.Pitas, pp.36-37
	int RGB_Fr_HLS_1(

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		const HLS_IMAGE *sHLS_Imagef,

		COLOR_IMAGE *sColor_Imagef)
	{
		float RGB_Intermediate(
			float &fN1f,
			float &fN2f,
			float &fHuef);

		int
			nLenImagef = sHLS_Imagef->nLength,
			nWidthImagef = sHLS_Imagef->nWidth,

			//nIndexCurSizef, // = iLenf + (iWidf*nLenImagef);
			nIndexOfPixelCurf,
			nRedCurf,
			nGreenCurf,
			nBlueCurf,

			iLenf,
			iWidf;

		float
			fUNDEFINEDF = fHueUndefinedValue, //-1.0,

			//fDiffCurf,

			fLightnessCurf,
			fSaturationCurf,
			fHueCurf,

			fHueTempf,

			fM1f,
			fM2f,

			fRedNormCurf,
			fGreenNormCurf,
			fBlueNormCurf;

		/////////////////////////////////////////////////////////////////////////////////////////////

		sColor_Imagef->nLength = nLenImagef;
		sColor_Imagef->nWidth = nWidthImagef;

		//printf("\n\n An error in 'RGB_Fr_HLS_1': please choose only one way to calulate Hue");

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			//for (iLenf = 8; iLenf < nLenImagef; iLenf++)
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				//nIndexOfPixelCurf = iLenf + (iWidf*nLen_CoToGr_Max);

				fHueCurf = sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf];

				fLightnessCurf = sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf];
				fSaturationCurf = sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf];

				if (fLightnessCurf <= 0.5)
				{
					fM2f = (float)(fLightnessCurf*(1.0 + fSaturationCurf));
				} //if (fLightnessCurf <= 0.5)
				else
				{
					fM2f = fLightnessCurf + fSaturationCurf - (fLightnessCurf*fSaturationCurf);
				}//else

				fM1f = (float)((2.0*fLightnessCurf) - fM2f);

				if (fSaturationCurf == 0.0)
				{
					if (fHueCurf == fUNDEFINEDF)
					{
						fRedNormCurf = fLightnessCurf;
						fGreenNormCurf = fLightnessCurf;
						fBlueNormCurf = fLightnessCurf;

					} // if (fHueCurf == fUNDEFINEDF)
					else // if (fHueCurf != fUNDEFINEDF)
					{
						fRedNormCurf = 0.0;
						fGreenNormCurf = 0.0;
						fBlueNormCurf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n A possible error in 'RGB_Fr_HLS_1':  fHueCurf = %E at if (fSaturationCurf == 0.0)", fHueCurf);

						printf("\n iWidf = %d, nWidthImagef = %d, iLenf = %d, nLenImagef = %d", iWidf, nWidthImagef, iLenf, nLenImagef);


						fprintf(fout0, "\n\n A possible error in 'RGB_Fr_HLS_1':  fHueCurf = %E at if (fSaturationCurf == 0.0)", fHueCurf);

						fprintf(fout0, "\n iWidf = %d, nWidthImagef = %d, iLenf = %d, nLenImagef = %d", iWidf, nWidthImagef, iLenf, nLenImagef);

						printf("\n\n Please press any key to exit");	fflush(fout0); getchar(); //exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					} // else // if (fHueCurf != fUNDEFINEDF)

				}//if (fSaturationCurf == 0.0)
				else //if (fSaturationCurf != 0.0)
				{

					if (fHueCurf < sParameters_Image_Preprocessing_1f->nHueMinf || fHueCurf > sParameters_Image_Preprocessing_1f->nHueMaxf)
					{
						nRedCurf = nIntensityStatMax;
						nGreenCurf = nIntensityStatMax;
						nBlueCurf = nIntensityStatMax;

						goto MarkColorAssignment;
					} // if (fHueCurf < sParameters_Image_Preprocessing_1f->nHueMinf || fHueCurf > sParameters_Image_Preprocessing_1f->nHueMaxf)

					fHueTempf = (float)(fHueCurf + 120.0);

					fRedNormCurf = RGB_Intermediate(
						fM1f, //float &fN1f,
						fM2f, //float &fN2f,
						fHueTempf); // float &fHuef);

					fHueTempf = fHueCurf; //?
					fGreenNormCurf = RGB_Intermediate(
						fM1f, //float &fN1f,
						fM2f, //float &fN2f,
						fHueCurf); // float &fHuef); //changed fHueCurf?


					fHueTempf = (float)(fHueCurf - 120.0);

					fBlueNormCurf = RGB_Intermediate(
						fM1f, //float &fN1f,
						fM2f, //float &fN2f,
						fHueTempf); // float &fHuef);

				} //if (fSaturationCurf != 0.0)

				nRedCurf = (int)(fRedNormCurf*(float)(nIntensityStatMax));

				nGreenCurf = (int)(fGreenNormCurf*(float)(nIntensityStatMax));
				nBlueCurf = (int)(fBlueNormCurf*(float)(nIntensityStatMax));

				///////////////////////////////////////////
				if (nRedCurf > nIntensityStatMax)
					nRedCurf = nIntensityStatMax;

				if (nRedCurf < 0)
					nRedCurf = 0;

				//////////////////////////////////////////////////
				if (nGreenCurf > nIntensityStatMax)
					nGreenCurf = nIntensityStatMax;

				if (nGreenCurf < 0)
					nGreenCurf = 0;


				////////////////////////////////////////////////////////
				if (nBlueCurf > nIntensityStatMax)
					nBlueCurf = nIntensityStatMax;

				if (nBlueCurf < 0)
					nBlueCurf = 0;

				if (sParameters_Image_Preprocessing_1f->bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf == true)
				{
					if (nRedCurf < sParameters_Image_Preprocessing_1f->nRedMinf || nRedCurf > sParameters_Image_Preprocessing_1f->nRedMaxf)
					{
						nRedCurf = nIntensityStatMax;
						nGreenCurf = nIntensityStatMax;
						nBlueCurf = nIntensityStatMax;

						goto MarkColorAssignment;
					} //if (nRedCurf < sParameters_Image_Preprocessing_1f->nRedMinf || nRedCurf > sParameters_Image_Preprocessing_1f->nRedMaxf)

					if (nGreenCurf < sParameters_Image_Preprocessing_1f->nGreenMinf || nGreenCurf > sParameters_Image_Preprocessing_1f->nGreenMaxf)
					{
						nGreenCurf = nIntensityStatMax;
						nGreenCurf = nIntensityStatMax;
						nBlueCurf = nIntensityStatMax;

						goto MarkColorAssignment;
					} //if (nGreenCurf < sParameters_Image_Preprocessing_1f->nGreenMinf || nGreenCurf > sParameters_Image_Preprocessing_1f->nGreenMaxf)

					if (nBlueCurf < sParameters_Image_Preprocessing_1f->nBlueMinf || nBlueCurf > sParameters_Image_Preprocessing_1f->nBlueMaxf)
					{
						nBlueCurf = nIntensityStatMax;
						nGreenCurf = nIntensityStatMax;
						nBlueCurf = nIntensityStatMax;

						goto MarkColorAssignment;
					} //if (nBlueCurf < sParameters_Image_Preprocessing_1f->nBlueMinf || nBlueCurf > sParameters_Image_Preprocessing_1f->nBlueMaxf)

				} // if (sParameters_Image_Preprocessing_1f->bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf == true)
				  ///////////////////////////////////////////////////////////////////

			MarkColorAssignment: sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nRedCurf;
				sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nGreenCurf;
				sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nBlueCurf;

			} // for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		return SUCCESSFUL_RETURN;;
	} // int RGB_Fr_HLS_1(...

	float RGB_Intermediate(
		float &fN1f,
		float &fN2f,
		float &fHuef)
	{
		if (fHuef > 360.0)
			fHuef -= 360.0;

		if (fHuef < 0.0)
			fHuef += 360.0;

		if (fHuef < 60.0)
		{
			return (float)((fN1f + ((fN2f - fN1f)*fHuef / 60.0)));
		}//if (fHuef < 60.0)

		if (fHuef < 180.0)
		{
			return (fN2f);
		}//if (fHuef < 180.0)

		if (fHuef < 240.0)
		{
			return (float)((fN1f + ((fN2f - fN1f)*(240.0 - fHuef) / 60.0)));
		}//if (fHuef < 240.0)

		return (fN1f);

	}//float RGB_Intermediate(...

	 /////////////////////////////////////////////////////////////////////////////
	void Weighted_HLS_Colors_One_Pixel_1(
		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		float &fHuef,
		float &fLightnessf,
		float &fSaturationf)
	{

		fHuef = (float)(fHuef*sParameters_Image_Preprocessing_1f->fWeightOfHuef / 100.0);
		if (fHuef > 360.0)
			fHuef = 360.0;

		fLightnessf = (float)(fLightnessf*sParameters_Image_Preprocessing_1f->fWeightOfLightnessf / 100.0);
		if (fLightnessf > 1.0)
			fLightnessf = 1.0;


		fSaturationf = (float)(fSaturationf*sParameters_Image_Preprocessing_1f->fWeightOfSaturationf / 100.0);
		if (fSaturationf > 1.0)
			fSaturationf = 1.0;

		//return 1;
	} //void Weighted_HLS_Colors_One_Pixel_1(...
	  ///////////////////////////////////////////////////////////////////

	void Weighting_HLS_Image_1(
		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		HLS_IMAGE *sHLS_Imagef)
	{

		void Weighted_HLS_Colors_One_Pixel_1(
			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			float &fHuef,
			float &fLightnessf,
			float &fSaturationf);

		int
			//nResf,
			nLenImagef = sHLS_Imagef->nLength,
			nWidthImagef = sHLS_Imagef->nWidth,

			//nIndexCurSizef, // = iLenf + (iWidf*nLenImagef);
			nIndexOfPixelCurf,

			iLenf,
			iWidf;

		float
			fLightnessCurf,
			fSaturationCurf,
			fHueCurf;

		/////////////////////////////////////////////////////////////////////////////////////////////

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			//for (iLenf = 8; iLenf < nLenImagef; iLenf++)
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				fHueCurf = sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf];
				fLightnessCurf = sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf];
				fSaturationCurf = sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf];

#ifndef COMMENT_OUT_ALL_PRINTS
				if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
				{

					fprintf(fout0, "\n\n 'Weighting_HLS_Image_1': iWidf = %d, iLenf = %d", iWidf, iLenf);
					fprintf(fout0, "\n fHueCurf = %E, fLightnessCurf = %E, fSaturationCurf = %E", fHueCurf, fLightnessCurf, fSaturationCurf);
				} // if ((iWidf / 50) * 50 == iWidf && (iLenf / 50) * 50 == iLenf)
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

				Weighted_HLS_Colors_One_Pixel_1(

					sParameters_Image_Preprocessing_1f,
					fHueCurf, //float &fHuef,
					fLightnessCurf, //float &fLightnessf,
					fSaturationCurf); // float &fSaturationf);

				sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = fHueCurf;
				sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = fLightnessCurf;
				sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = fSaturationCurf;

			} // for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		//return 1;
	} //void Weighting_HLS_Image_1(

	int RGB_Fr_Weighted_HLS_1(

		const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
		HLS_IMAGE *sHLS_Imagef,

		COLOR_IMAGE *sColor_Imagef)
	{
		int HLS_Fr_RGB(
			const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]

			HLS_IMAGE *sHLS_Imagef);

		void Weighting_HLS_Image_1(
			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			HLS_IMAGE *sHLS_Imagef);

		int RGB_Fr_HLS_1(
			const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			const HLS_IMAGE *sHLS_Imagef,

			COLOR_IMAGE *sColor_Imagef);
		///////////////////////////////////////////////////
		int
			nResf;

		nResf = HLS_Fr_RGB(
			sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax_CoToGr]

			sHLS_Imagef); // HLS_IMAGE *sHLS_Imagef);

		if (nResf < 0)
		{
			return UNSUCCESSFUL_RETURN;
		}//

		Weighting_HLS_Image_1(
			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			sHLS_Imagef); // HLS_IMAGE *sHLS_Imagef);

		nResf = RGB_Fr_HLS_1(
			sParameters_Image_Preprocessing_1f, //const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,
			sHLS_Imagef, //const HLS_IMAGE *sHLS_Imagef,

			sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

		if (nResf < 0)
		{
			return UNSUCCESSFUL_RETURN;
		}//

		return SUCCESSFUL_RETURN;
	} // int RGB_Fr_Weighted_HLS_1(...

	int Initializing_Color_To_CurSize(
		const int nWidthImagef,
		const int nLenImagef,

		COLOR_IMAGE *sColor_Imagef) //[]
	{

		int
			nIndexOfPixelCurf,

			nImageSizeCurf = nWidthImagef * nLenImagef,
			iWidf,
			iLenf;

		sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
		sColor_Imagef->nIntensityOfBackground_Red = -1;
		sColor_Imagef->nIntensityOfBackground_Green = -1;
		sColor_Imagef->nIntensityOfBackground_Blue = -1;

		sColor_Imagef->nWidth = nWidthImagef;
		sColor_Imagef->nLength = nLenImagef;

		sColor_Imagef->nLenObjectBoundary_Arr = new int[nWidthImagef];

		sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
		sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
		sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

		//nLenObjectBoundary_Arr
		sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

		if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
		{
			return UNSUCCESSFUL_RETURN;
		} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
				sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
				sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
				sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

				sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)

		} // for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		return SUCCESSFUL_RETURN;
	} //int Initializing_Color_To_CurSize(...

	int Initializing_HLS_To_CurSize(
		const int nWidthImagef,
		const int nLenImagef,

		HLS_IMAGE *sHLS_Imagef) //[]
	{
		int
			nIndexOfPixelCurf,

			nImageSizeCurf = nWidthImagef * nLenImagef,
			iWidf,
			iLenf;

		sHLS_Imagef->nWidth = nWidthImagef;
		sHLS_Imagef->nLength = nLenImagef;

		//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
		sHLS_Imagef->fHue_Arr = new float[nImageSizeCurf];
		sHLS_Imagef->fLightness_Arr = new float[nImageSizeCurf];
		sHLS_Imagef->fSaturation_Arr = new float[nImageSizeCurf];

		if (sHLS_Imagef->fHue_Arr == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize' 1");
			fprintf(fout0, "\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize' 1");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (sHLS_Imagef->nRed_Arr == fHue_Arr)

		if (sHLS_Imagef->fLightness_Arr == nullptr )
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize'");
			fprintf(fout0, "\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] sHLS_Imagef->fHue_Arr;

			return UNSUCCESSFUL_RETURN;
		} // if (sHLS_Imagef->fLightness_Arr == nullptr )

		if ( sHLS_Imagef->fSaturation_Arr == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize'");
			fprintf(fout0, "\n\n An dynamic memory allocation error in 'Initializing_HLS_To_CurSize'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] sHLS_Imagef->fHue_Arr;
			delete[] sHLS_Imagef->fLightness_Arr;

			return UNSUCCESSFUL_RETURN;
		} // if ( sHLS_Imagef->fSaturation_Arr == nullptr)

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
				sHLS_Imagef->fHue_Arr[nIndexOfPixelCurf] = -1.0;
				sHLS_Imagef->fLightness_Arr[nIndexOfPixelCurf] = -1.0;
				sHLS_Imagef->fSaturation_Arr[nIndexOfPixelCurf] = -1.0;

			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} // for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		return SUCCESSFUL_RETURN;
	} //int Initializing_HLS_To_CurSize(...

	int Initializing_GRAYSCALE_To_CurSize(
		const int nWidthImagef,
		const int nLenImagef,

		GRAYSCALE_IMAGE *sGRAYSCALE_Imagef) //[]
	{
		int
			nIndexOfPixelCurf,

			nImageSizeCurf = nWidthImagef * nLenImagef,
			iWidf,
			iLenf;

		sGRAYSCALE_Imagef->nWidth = nWidthImagef;
		sGRAYSCALE_Imagef->nLength = nLenImagef;

		sGRAYSCALE_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
		sGRAYSCALE_Imagef->nGrayScale_Arr = new int[nImageSizeCurf];

		if (sGRAYSCALE_Imagef->nPixel_ValidOrNotArr == nullptr || sGRAYSCALE_Imagef->nGrayScale_Arr == nullptr)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An dynamic memory allocation error in 'Initializing_GRAYSCALE_To_CurSize'");
			fprintf(fout0, "\n\n An dynamic memory allocation error in 'Initializing_GRAYSCALE_To_CurSize'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} // if (sGRAYSCALE_Imagef->nPixel_ValidOrNotArr == fHue_Arr || sGRAYSCALE_Imagef->nGrayScale_Arr == nullptr)

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				sGRAYSCALE_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for all pixels
				sGRAYSCALE_Imagef->nGrayScale_Arr[nIndexOfPixelCurf] = -1;

			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} // for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		return SUCCESSFUL_RETURN;
	} //int Initializing_GRAYSCALE_To_CurSize(...

	///////////////////////////////////////////////////////////
	void Copying_Grayscale_To_Grayscale(
	
		const GRAYSCALE_IMAGE *sGRAYSCALE_ImageInf,
		GRAYSCALE_IMAGE *sGRAYSCALE_ImageOutf)

	{
		int
			nIndexOfPixelCurf,

			nWidthImagef = sGRAYSCALE_ImageInf->nWidth,
			nLenImagef = sGRAYSCALE_ImageInf->nLength,
			iWidf,
			iLenf;

		for (iWidf = 0; iWidf < nWidthImagef; iWidf++)
		{
			for (iLenf = 0; iLenf < nLenImagef; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nLenImagef);

				sGRAYSCALE_ImageOutf->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = sGRAYSCALE_ImageInf->nPixel_ValidOrNotArr[nIndexOfPixelCurf]; //

				sGRAYSCALE_ImageOutf->nGrayScale_Arr[nIndexOfPixelCurf] = sGRAYSCALE_ImageInf->nGrayScale_Arr[nIndexOfPixelCurf];

			} //for (iLenf = 0; iLenf < nLenImagef; iLenf++)
		} //for (iWidf = 0; iWidf < nWidthImagef; iWidf++)

		//return SUCCESSFUL_RETURN;
	} //void Copying_Grayscale_To_Grayscale(...

	//////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	int nRadius_MedianFilter = 2;

	int nDimOfArr_MedianFilter 
	*/

	int DataFor_MedianFilter_OnePixel(
		const int nWidPixelf,

		const int nLenPixelf,

		const int nRadius_MedianFilterf,
		const int nDimOfArr_MedianFilterf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		int &nNumOfValidRecordsf,
		int nIntensities_LocalArrf[]) // [nDimOfArr_MedianFilterf]
	{
		int
			nWidMinf,
			nWidMaxf,

			nLenMinf,
			nLenMaxf,

			iLenf,
			iWidf,

			i1,
			nIndexOfPixelCurf,

			nIntensityCurf,

			nImageWidthf = sGrayscale_Imagef->nWidth,
			nImageLengthf = sGrayscale_Imagef->nLength;

		if (nLenPixelf - nRadius_MedianFilterf < 0 || nLenPixelf + nRadius_MedianFilterf > nImageLengthf - 1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'DataFor_MedianFilter_OnePixel': nLenPixelf = %d, nImageLengthf = %d", nLenPixelf, nImageLengthf);
			fprintf(fout0, "\n\n An error in 'DataFor_MedianFilter_OnePixel': nLenPixelf = %d, nImageLengthf = %d", nLenPixelf, nImageLengthf);
			//printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;

		}//if (nLenPixelf - nRadius_MedianFilterf < 0 || nLenPixelf + nRadius_MedianFilterf > nImageLengthf - 1)

		if (nWidPixelf - nRadius_MedianFilterf < 0 || nWidPixelf + nRadius_MedianFilterf > nImageWidthf - 1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'DataFor_MedianFilter_OnePixel': nWidPixelf = %d, nImageWidthf = %d", nWidPixelf, nImageWidthf);
			fprintf(fout0, "\n\n An error in 'DataFor_MedianFilter_OnePixel': nWidPixelf = %d, nImageWidthf = %d", nWidPixelf, nImageWidthf);
		//	printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nWidPixelf - nRadius_MedianFilterf < 0 || nWidPixelf + nRadius_MedianFilterf > nImageWidthf - 1)

		 //////////////////////////////////////////////////////////////////////////////
		nWidMinf = nWidPixelf - nRadius_MedianFilterf;

		nWidMaxf = nWidPixelf + nRadius_MedianFilterf;

		nLenMinf = nLenPixelf - nRadius_MedianFilterf;

		nLenMaxf = nLenPixelf + nRadius_MedianFilterf;
	

		for (i1 = 0; i1 < nDimOfArr_MedianFilterf; i1++)
		{
			nIntensities_LocalArrf[i1] = -1; //initially

		}//for (i1 = 0; i1 < nDimOfArr_MedianFilterf; i1++)

		 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		nNumOfValidRecordsf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout0, "\n\n 'Local_HistEqual_Slow_GrayscaleImage': nImageLengthf = %d, nImageWidthf = %d", nImageLengthf, nImageWidthf);

		//fprintf(fout0, "\n nWidMinf = %d, nWidMaxf = %d, nLenMinf = %d, nLenMaxf = %d, the product = %d", nWidMinf, nWidMaxf, nLenMinf, nLenMaxf, (nLenMaxf - nLenMinf + 1)*(nWidMaxf - nWidMinf + 1));
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
		{
			for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)
			{
				//iWid_Glob = iWidf;
				//iLen_Glob = iLenf;
/*
				if (nNumOfValidRecordsf >= nDimOfArr_MedianFilterf)
				{
					printf("\n\n An error in 'DataFor_MedianFilter_OnePixel': nNumOfValidRecordsf = %d >= nDimOfArr_MedianFilterf = %d", nNumOfValidRecordsf, nDimOfArr_MedianFilterf);

					printf("\n nLenPixelf = %d, nWidPixelf = %d, iLenf = %d, iWidf = %d", nLenPixelf, nWidPixelf, iLenf, iWidf);

					fprintf(fout0, "\n\n An error in 'DataFor_MedianFilter_OnePixel': nNumOfValidRecordsf = %d >= nDimOfArr_MedianFilterf = %d",
						nNumOfValidRecordsf, nDimOfArr_MedianFilterf);

					fprintf(fout0, "\n nLenPixelf = %d, nWidPixelf = %d, iLenf = %d, iWidf = %d", nLenPixelf, nWidPixelf, iLenf, iWidf);
					printf("\n\n Please press any key to exit:");	fflush(fout0); getchar(); exit(1);

				} //if (nNumOfValidRecordsf >= nDimLocal_HistEqual_FastMaxf)
*/

				nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);
/*
				if (sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
				{
					continue;
				} // if (sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
*/

				nIntensityCurf = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];

				if (nIntensityCurf <= nIntensityForAoI_Min)
				{
					continue;
				}// if (nIntensityCurf <= nIntensityForAoI_Min)

				nIntensities_LocalArrf[nNumOfValidRecordsf] = nIntensityCurf;
				nNumOfValidRecordsf += 1;

			} //for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)

		} // for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)

		if (nNumOfValidRecordsf <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'DataFor_MedianFilter_OnePixel': nNumOfValidRecordsf = %d, nWidPixelf = %d, nLenPixelf = %d", nNumOfValidRecordsf, nWidPixelf,nLenPixelf);

			fprintf(fout0, "\n\n An error in 'DataFor_MedianFilter_OnePixel': nNumOfValidRecordsf = %d, nWidPixelf = %d, nLenPixelf = %d", nNumOfValidRecordsf, nWidPixelf, nLenPixelf);
			//	printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nNumOfValidRecordsf <= 0)

		return SUCCESSFUL_RETURN;
	} //int DataFor_MedianFilter_OnePixel(...
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int MedianFilter_OnePixel(
		
		const int nWidPixelf,
		const int nLenPixelf,

		const int nRadius_MedianFilterf,
		const int nDimOfArr_MedianFilterf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,
		int nIntensities_LocalArrf[], // [nDimOfArr_MedianFilterf]

		int &nIntensityFilteredf)

	{
		int DataFor_MedianFilter_OnePixel(
			const int nWidPixelf,

			const int nLenPixelf,

			const int nRadius_MedianFilterf,
			const int nDimOfArr_MedianFilterf,

			const GRAYSCALE_IMAGE *sGrayscale_Imagef,

			int &nNumOfValidRecordsf,
			int nIntensities_LocalArrf[]); // [nDimOfArr_MedianFilterf]

		int
			nResf,

			nHalf_OfDimOfArr_MedianFilterf = nDimOfArr_MedianFilterf / 2, 

			nLocationOfMedianInArrf = -1,
			nNumOfValidRecordsf;

		nResf = DataFor_MedianFilter_OnePixel(
			nWidPixelf, //const int nWidPixelf,

			nLenPixelf, //const int nLenPixelf,

			nRadius_MedianFilterf, //const int nRadius_MedianFilterf,
			nDimOfArr_MedianFilterf, //const int nDimOfArr_MedianFilterf,

			sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,

			nNumOfValidRecordsf, //int &nNumOfValidRecordsf,
			nIntensities_LocalArrf); // int nIntensities_LocalArrf[]); // [nDimOfArr_MedianFilterf]
	
/*
		if (nWidPixelf == 445 && nLenPixelf == 211)
		{
			//for nRadius_MedianFilter = 2
			nIntensities_LocalArrf[21] = -1;
			nIntensities_LocalArrf[22] = -1;
			nIntensities_LocalArrf[24] = -1;
			nIntensities_LocalArrf[23] = -1;

			nNumOfValidRecordsf = nNumOfValidRecordsf - 4;

			printf("\n\n Before sorting: nNumOfValidRecordsf = %d", nNumOfValidRecordsf);

			int i;
			for (i = 0; i < nDimOfArr_MedianFilterf; i++)
			{
				printf("\n nIntensities_LocalArrf[%d] = %d, nLenPixelf = %d, nWidPixelf = %d ", i, nIntensities_LocalArrf[i], nLenPixelf, nWidPixelf);
			}// for (i = 0; i < nDimOfArr_MedianFilterf; i++)

			printf("\n\n Please press any key to continue:"); getchar();
		} //if (nWidPixelf == 445 && nLenPixelf == 211)
*/

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout0,"\n\n 'UNSUCCESSFUL_RETURN' in 'MedianFilter_OnePixel', nWidPixelf = %d, nLenPixelf = %d", nWidPixelf, nLenPixelf);
			printf("\n\n 'UNSUCCESSFUL_RETURN' in 'MedianFilter_OnePixel', nWidPixelf = %d, nLenPixelf = %d", nWidPixelf, nLenPixelf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

//from smallest to largest
		qsort(nIntensities_LocalArrf, nDimOfArr_MedianFilterf, sizeof(int), compare);

		if (nNumOfValidRecordsf < nDimOfArr_MedianFilterf)
		{
			nLocationOfMedianInArrf = nNumOfValidRecordsf / 2;
			nLocationOfMedianInArrf = nDimOfArr_MedianFilterf - nLocationOfMedianInArrf -1; 

			nIntensityFilteredf = nIntensities_LocalArrf[nLocationOfMedianInArrf];

		} // if (nNumOfValidRecordsf < nDimOfArr_MedianFilterf)
		else // if (nNumOfValidRecordsf == nDimOfArr_MedianFilterf)
		{
			nIntensityFilteredf = nIntensities_LocalArrf[nHalf_OfDimOfArr_MedianFilterf];
		}//// if (nNumOfValidRecordsf == nDimOfArr_MedianFilterf)

		if (nIntensityFilteredf == -1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout0, "\n\n 'nIntensityFilteredf == -1' in 'MedianFilter_OnePixel', nWidPixelf = %d, nLenPixelf = %d", nWidPixelf, nLenPixelf);
			printf("\n\n 'nIntensityFilteredf == -1' in 'MedianFilter_OnePixel', nWidPixelf = %d, nLenPixelf = %d", nWidPixelf, nLenPixelf);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}//if (nIntensityFilteredf == -1)

/*
		if (nWidPixelf == 445 && nLenPixelf == 211)
		{
			printf("\n\n After sorting: nIntensityFilteredf = %d, nLocationOfMedianInArrf = %d", nIntensityFilteredf, nLocationOfMedianInArrf);
			int i;
			for (i = 0; i < nDimOfArr_MedianFilterf; i++)
			{
				printf("\n nIntensities_LocalArrf[%d] = %d, nLenPixelf = %d, nWidPixelf = %d", i, nIntensities_LocalArrf[i], nLenPixelf, nWidPixelf);
			}// for (i = 0; i < nDimOfArr_MedianFilterf; i++)

			printf("\n\n Please press any key to continue:"); getchar();
		} //if (nWidPixelf == 445 && nLenPixelf == 211)
*/

		return SUCCESSFUL_RETURN;
	} //int MedianFilter_OnePixel(...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int MedianFilter_Image(

		const int nRadius_MedianFilterf,
		const int nDimOfArr_MedianFilterf,

		const GRAYSCALE_IMAGE *sGrayscale_Imagef,

		GRAYSCALE_IMAGE *sGrayscale_ImageByMedianFilterf) //
	{

		int MedianFilter_OnePixel(
			const int nWidPixelf,
			const int nLenPixelf,

			const int nRadius_MedianFilterf,
			const int nDimOfArr_MedianFilterf,

			const GRAYSCALE_IMAGE *sGrayscale_Imagef,
			int nIntensities_LocalArrf[], // [nDimOfArr_MedianFilterf]

			int &nIntensityFilteredf);

		int
			nResf,
			nWidMinf, // = nRadius_MedianFilterf,
			nWidMaxf,

			nLenMinf, // = nRadius_MedianFilterf,
			nLenMaxf,

			nLenPixelf,
			nWidPixelf,

			iLenf,
			iWidf,

			nIndexOfPixelCurf,

			nIntensityCurf,
			nNumOfFilteredPixelsf = 0,

			nIntensityFilteredf, 

			nImageWidthf = sGrayscale_Imagef->nWidth,
			nImageLengthf = sGrayscale_Imagef->nLength;

		int * nIntensities_LocalArrf;
		nIntensities_LocalArrf = new int[nDimOfArr_MedianFilterf];

		if (nIntensities_LocalArrf == nullptr)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\nAn error in dynamic memory allocation for 'nIntensities_LocalArrf'");
			fprintf(fout0, "\n\nAn error in dynamic memory allocation for 'nIntensities_LocalArrf'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (nIntensities_LocalArrf == nullptr)

		 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		nWidMinf = nRadius_MedianFilterf;
		nWidMaxf = nImageWidthf - nRadius_MedianFilterf  - 1;

		nLenMinf = nRadius_MedianFilterf;
		nLenMaxf = nImageLengthf - nRadius_MedianFilterf - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
		//fprintf(fout0, "\n\n 'Local_HistEqual_Slow_GrayscaleImage': nImageLengthf = %d, nImageWidthf = %d", nImageLengthf, nImageWidthf);

		//fprintf(fout0, "\n nWidMinf = %d, nWidMaxf = %d, nLenMinf = %d, nLenMaxf = %d, the product = %d", nWidMinf, nWidMaxf, nLenMinf, nLenMaxf, (nLenMaxf - nLenMinf + 1)*(nWidMaxf - nWidMinf + 1));
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		//for (iWidf = nWidMinf; iWidf < nWidMaxf; iWidf++)
		for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
		{
			//for (iLenf = nLenMinf; iLenf < nLenMaxf; iLenf++)
			for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

				if (iWidf <= nWidMinf || iWidf > nWidMaxf || iLenf <= nLenMinf || iLenf > nLenMaxf)
				{
					sGrayscale_ImageByMedianFilterf->nGrayScale_Arr[nIndexOfPixelCurf] = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];

					continue;
				} // if (iWidf <= nWidMinf || iWidf > nWidMaxf || iLenf <= nLenMinf || iLenf > nLenMaxf)

				nLenPixelf = iLenf;
				nWidPixelf = iWidf;
				if (sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)
				{
					sGrayscale_ImageByMedianFilterf->nGrayScale_Arr[nIndexOfPixelCurf] = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];
					continue;
				} // if (sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] == 0)

				nIntensityCurf = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];

				if (nIntensityCurf <= nIntensityForAoI_Min)
				{
					sGrayscale_ImageByMedianFilterf->nGrayScale_Arr[nIndexOfPixelCurf] = sGrayscale_Imagef->nGrayScale_Arr[nIndexOfPixelCurf];

					continue;
				}// if (nIntensityCurf <= nIntensityForAoI_Min)

				//if (nWidPixelf == 445 && nLenPixelf == 211)

				//nLenPixelf = 211; nWidPixelf = 445;
				//printf("\n\n Before 'MedianFilter_OnePixel': nLenPixelf = %d, nWidPixelf = %d, nDimOfArr_MedianFilterf = %d ", nLenPixelf, nWidPixelf, nDimOfArr_MedianFilterf);
				nResf = MedianFilter_OnePixel(
					nWidPixelf, //const int nWidPixelf,
					nLenPixelf, //const int nLenPixelf,

					nRadius_MedianFilterf, //const int nRadius_MedianFilterf,
					nDimOfArr_MedianFilterf, //const int nDimOfArr_MedianFilterf,

					sGrayscale_Imagef, //const GRAYSCALE_IMAGE *sGrayscale_Imagef,
					nIntensities_LocalArrf, //int nIntensities_LocalArrf[], // [nDimOfArr_MedianFilterf]

					nIntensityFilteredf); // int &nIntensityFilteredf);

				if (nResf == UNSUCCESSFUL_RETURN)
				{

#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'MedianFilter_OnePixel': nIntensityCurf = %d, nNumOfFilteredPixelsf = %d, nWidPixelf = %d, nLenPixelf = %d", 
						nIntensityCurf, nNumOfFilteredPixelsf, nWidPixelf, nLenPixelf);

					fprintf(fout0, "\n\n An error in 'MedianFilter_OnePixel': nIntensityCurf = %d, nNumOfFilteredPixelsf = %d, nWidPixelf = %d, nLenPixelf = %d", 
						nIntensityCurf,	nNumOfFilteredPixelsf, nWidPixelf, nLenPixelf);

					//	printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

					delete[] nIntensities_LocalArrf;

					return UNSUCCESSFUL_RETURN;

				}//if (nResf == UNSUCCESSFUL_RETURN)

				nNumOfFilteredPixelsf += 1;

				sGrayscale_ImageByMedianFilterf->nGrayScale_Arr[nIndexOfPixelCurf] = nIntensityFilteredf;

			} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

		} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

		if (nNumOfFilteredPixelsf <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'MedianFilter_Image': nNumOfFilteredPixelsf = %d, nWidPixelf = %d, nLenPixelf = %d", nNumOfFilteredPixelsf, nWidPixelf, nLenPixelf);

			fprintf(fout0, "\n\n An error in 'MedianFilter_Image': nNumOfFilteredPixelsf = %d, nWidPixelf = %d, nLenPixelf = %d", nNumOfFilteredPixelsf, nWidPixelf, nLenPixelf);
			//	printf("\n\n Please press any key to exit:"); fflush(fout0); getchar(); exit(1);
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

			delete[] nIntensities_LocalArrf;

			return UNSUCCESSFUL_RETURN;
		} //if (nNumOfFilteredPixelsf <= 0)

		delete[] nIntensities_LocalArrf;

		return SUCCESSFUL_RETURN;
	} //int MedianFilter_Image(...
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//printf("\n\n Please press any key:"); getchar();



