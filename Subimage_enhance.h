#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.hpp"
using namespace imago;

#include "Color_image_Defs.h"

////////////////////////////////////////////////////////////////
#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

	#define COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
#define pi 3.14159

	#define PRINTING_SUBIMAGE

#define DEACTIVATE_PRINTING

#define fDeviationFrom_100percentsMax 1.0
////////////////////////////////////////////////////////////////////////////

//The length is from the left to the right; the width is from down to up

#define nLenMin 200 //400 //nLenMax
#define nWidMin 100  // < nWidMax

#define nLenMax 3500 //500 //1300 
#define nWidMax 5500 //4800 //400 
#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////////////
#define nLenSubimageToPrintMin 0 //342 
#define nLenSubimageToPrintMax 20 //426

#define nWidSubimageToPrintMin 0 //814 
#define nWidSubimageToPrintMax 20


//////////////////////////////////////////////////////////////////
//statistics
//#define nIntensityStatMin 10 //85 
//#define nIntensityStatMax 255 

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define nNumOfHistogramBinsStat 17

#define nGrayLevelIntensityToDrawContours 100 //40

#define nGrayLevelIntensityForDenseArea 140
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//ColorToGrayScale

#define nLen_CoToGr_Max 6000 
#define nWid_CoToGr_Max 6000 

#define nLen_CoToGr_Min 50 
#define nWid_CoToGr_Min 50

#define nImageSizeMax_CoToGr (nLen_CoToGr_Max*nWid_CoToGr_Max)
///////////////////////////////////////////////////////////////////

#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255 

//#define nNumOfHistogramBinsStat_CoToGr (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat_CoToGr (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

//#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define fHueUndefinedValue (-1.0)

////////////////////////////////////////////////////////////////////
#define dX_Referf 0.950456
#define dY_Referf 1.0
#define dZ_Referf 1.088754

////////////////////////////////////////////////////////////////////

//bool bBLACK_BACKGROUND_WHITE_CONTOURS = false;

//#define USE_SUMS_OF_IMAGE_GRADIENTS
//bool bUSE_SUMS_OF_IMAGE_GRADIENTS = true;

//#define DIM3x3_SOBEL_FILTER // not together with #define DIM5x5_SOBEL_FILTER
//bool bDIM3x3_SOBEL_FILTER = true;

//#define DIM5x5_SOBEL_FILTER
//bool bVERSION_2_OF_SOBEL_3x3 = true;

//bool bHIST_EQUAL_AFTER_SOBEL = false;
//bool bHIST_EQUAL_AFTER_SOBEL = true;
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//bool bUSING_ONLY_BLACK_AND_WHITE_COLORS = false;

//int nIntensityThresholdForSobelMin = 169;
//int nIntensityThresholdForSobelMax = 170;

//float fSE_FadingFactor = 1.0;

//the weights are in percents
//float fSE_Weight_Red = (float)(80.0); //(100.0) //(100.0/3.0) 


//float fSE_Weight_Green = (float)(10.0); //20.0 //(100.0/2.0) 

//float fSE_Weight_Blue = (float)(10.0); //(100.0/2.0) 

//float fSE_Weight_For_2nd_Version = (float)(2.3); //1.0 //0.5 //0.8 //32.0

//float fNormalizing = (float)(4.0);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
//DogLegTornACL.jpg -- inside the AoI
int nWid_AoI_UpperLeft = 298;
int nLen_AoI_UpperLeft = 1265;

int nWid_AoI_BottomRight = 373;
int nLen_AoI_BottomRight = 1325;

int nIntensityForAoI_Max = 110;
int nIntensityForAoI_Min = 5; //50;


//including the surrounding bones

//DogLegTornACL.jpg -- a bigger AoI, including bones, etc.
int nWid_AoI_UpperLeft = 280;
int nLen_AoI_UpperLeft = 1166;

int nWid_AoI_BottomRight = 415;
int nLen_AoI_BottomRight = 1385;

//int nIntensityForAoI_Max = 24;
//int nIntensityForAoI_Min = 9; //

//less noise
int nIntensityForAoI_Max = 22;
int nIntensityForAoI_Min = 11; 
*/

////////////////////////////////////////////////////
//The mouse

#define nWid_AoI_UpperLeft 873
#define nLen_AoI_UpperLeft 753

#define nWid_AoI_BottomRight 941
#define nLen_AoI_BottomRight 791

#define nIntensityForAoI_Max 245
#define nIntensityForAoI_Min 20 //50



//Normals

/*
//Bacca Summers_Normal_Original ACL-- inside the AoI
int nWid_AoI_UpperLeft = 591;
int nLen_AoI_UpperLeft = 275;

int nWid_AoI_BottomRight = 733;
int nLen_AoI_BottomRight = 441;

int nIntensityForAoI_Max = 180; //110;

//int nIntensityForAoI_Min = 20; //actual;
int nIntensityForAoI_Min = 45; //test
*/

/*
//Binks Summers_Normal_Original_ACL.jpg-- inside the AoI
int nWid_AoI_UpperLeft = 671;
int nLen_AoI_UpperLeft = 401;

int nWid_AoI_BottomRight = 801;
int nLen_AoI_BottomRight = 539;

int nIntensityForAoI_Max = 180; //110;
int nIntensityForAoI_Min = 20; //50;
*/

/*
//Charlie Laughlin_Normal_Original_ACL -- inside the AoI
int nWid_AoI_UpperLeft = 710;
int nLen_AoI_UpperLeft = 554;

int nWid_AoI_BottomRight = 852;
int nLen_AoI_BottomRight = 690;

int nIntensityForAoI_Max = 180; //110;
int nIntensityForAoI_Min = 23; //0;
*/
////////////////////////////////////////////
//Torn ACL
/*
//Sable_ACL Rupture_Original.jpg-- inside the AoI
int nWid_AoI_UpperLeft = 1109;
int nLen_AoI_UpperLeft = 535;

int nWid_AoI_BottomRight = 1334;
int nLen_AoI_BottomRight = 637;

int nIntensityForAoI_Max = 180; //110;
int nIntensityForAoI_Min = 20; //50;
*/

/*
//Queenie_ACL Rupture_Original.jpg-- inside the AoI
int nWid_AoI_UpperLeft = 1231;
int nLen_AoI_UpperLeft = 387;

int nWid_AoI_BottomRight = 1317;
int nLen_AoI_BottomRight = 541;

int nIntensityForAoI_Max = 180; //110;
int nIntensityForAoI_Min = 45; //20;
*/

/*
//Sophie_ACL Rupture_Original.jpg-- inside the AoI
int nWid_AoI_UpperLeft = 401;
int nLen_AoI_UpperLeft = 61;

int nWid_AoI_BottomRight = 558;
int nLen_AoI_BottomRight = 253;


int nIntensityForAoI_Max = 180; //110;
int nIntensityForAoI_Min = 45; //20; // the pixels with intensities < nIntensityForAoI_Min belong to the background
*/

////////////////////////////////////////////////////////////////////

//Unsharp masking
//float fCoefOfSharpening_Inside3x3 = (float)(1.0); // typically, >= 0.3 and <= 0.7

//int nNumOfItersFor_Unsharp_masking_3x3 = 1;

//int nNumOfItersFor_Unsharp_masking_3x3 = 4;

//float fAmount_UnsharpMask = (float)(3.0); // 

//float fAmount_UnsharpMask =  (float)(1.2); // 
//float fAmount_UnsharpMask = - (float)(0.5); // typically, >= 0.3 and <= 0.7
//float fAmount_UnsharpMask =  (float)(0.8); //
//float fAmount_UnsharpMask =  (float)(0.5); // 
//float fAmount_UnsharpMask =  (float)(0.0); // 

//float fCoefGray_UnsharpMask = (float)(1.0);

//float fCoefGray_UnsharpMask = 0.2;

//int nRadius_ForUnsharp_Mask = 3; //4;
//int nRadius_ForUnsharp_Mask = 10;

//int nRadius_ForUnsharp_Mask = 20;

//int nDiffOfIntensities_Threshold_Unsharp_mask = 1;

//int nDiffOfIntensities_Threshold_Unsharp_mask = 5;

//int nDiffOfIntensities_Threshold_Unsharp_mask = 6;

//int nDiffOfIntensities_Threshold_Unsharp_mask = 7;
//int nDiffOfIntensities_Threshold_Unsharp_mask = 10;
//int nDiffOfIntensities_Threshold_Unsharp_mask = 30;

//float fStDev_ForUnsharp_Mask = 0.84089642; //Wiki 

//float fStDev_ForUnsharp_Mask = (float)(nRadius_ForUnsharp_Mask) / (float)(2.5); // fStDev_ForUnsharp_Mask ~ nRadius_ForUnsharp_Mask/3 or nRadius_ForUnsharp_Mask/2.5
///////////////////////////////////////////////////////////////////////////
//
//for 'void Is_Pixel_Suitable_For_Unsharp_Mask('
//int nDist_Unsharp_mask = 2; //<= nRadius_ForUnsharp_Mask
//int nIntensity_Threshold_Unsharp_mask = 5;

//int nNumOfThreshViolationsMin_Unsharp_mask = 1; //>= 1 and <= 8

//////////////////////////////////////////////////////////////////////
//median filter
//int nRadius_MedianFilter = 2;

//int nLenOfSquare_MedianFilter = (2 * nRadius_MedianFilter + 1);

//int nDimOfArr_MedianFilter = nLenOfSquare_MedianFilter * nLenOfSquare_MedianFilter;

//int nHalf_OfDimOfArr_MedianFilter = nDimOfArr_MedianFilter / 2;

//////////////////////////////////////////////////////////////////////////////////////////
//bool ADDITIONAL_AOI_OUTPUT_FOR_FILTER = true;
#define ADDITIONAL_AOI_OUTPUT_FOR_FILTER false

/////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	float fCoefOfSharpening_Inside3x3f = (float)(1.0); // typically, >= 0.3 and <= 0.7

	bool bBLACK_BACKGROUND_WHITE_CONTOURS = false;

//////////////////////////////////////////
	bool bONLY_RGB_WEIGHTSf = true;
	//bool bONLY_RGB_WEIGHTSf = false;

//new
	bool bJUST_COPY_PIXEL_INTENSITIES_FOR_GRAY_INPUT_IMAGEf = true;
	//bool bJUST_COPY_PIXEL_INTENSITIES_FOR_GRAY_INPUT_IMAGEf = false;

	bool bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf = false;
	//bool bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf = true; //Ok -- Sophie


	bool bIS_HISTOGRAM_EQUALIZATION_FOR_AoIs_INCLUDEDf = false;
	//bool bIS_HISTOGRAM_EQUALIZATION_FOR_AoIs_INCLUDEDf = true; //Ok -- Sophie

	bool bIS_UNSHARP_MASK_3x3_INCLUDEDf = false;
	//bool bIS_UNSHARP_MASK_3x3_INCLUDEDf = true;

		bool bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf = false;
		//bool bIS_UNSHARP_MASK_VARIABLE_RADIUS_INCLUDEDf = true;

		bool bRATIONAL_UNSHARPED_MASKING_IS_APPLIED = true;
		//bool bRATIONAL_UNSHARPED_MASKING_IS_APPLIED = false;


	//	bool bIS_MEDIAN_FILTER_TO_FIN_UNSHARP_IMAGE_INCLUDEDf = true;
		bool bIS_MEDIAN_FILTER_TO_FIN_UNSHARP_IMAGE_INCLUDEDf = false;

	/////////////////////////////////////////////////////////////////////////
	float fFadingFactorf = (float)(1.0);

	bool bINCLUDE_EXP_INTO_COLOR_TO_GRAYf = false;
	//bool bINCLUDE_EXP_INTO_COLOR_TO_GRAYf = true; //Ok1 //Ok2 ////Ok -- Sophie

	int nThreshPixelIntensityForExp_ColorToGrayf = 245; //190; //Ok2
	//int nThreshPixelIntensityForExp_ColorToGrayf = 240; //190; //Ok2
	//int nThreshPixelIntensityForExp_ColorToGrayf = 170; //Ok1

	//float fConstForExp_ColorToGrayf = (float)(0.005); //Ok1 //Ok2
	float fConstForExp_ColorToGrayf = (float)(0.02); //0.01  //0.08 //0.04


//exp parameters for the sobel are located above
	//bool bINCLUDE_EXP_INTO_SOBEL_FILTERf = false;
	//bool bINCLUDE_EXP_INTO_SOBEL_FILTERf = true;
	//int nThreshPixelIntensityForExp_SobelFilterf = 235; //190; //20; //50  //30 //10 //70

	//float fConstForExp_SobelFilterf = (float)(0.005); //0.01 //0.02 //0.08 //0.04
	
	////////////////////////////////////////////////

//#define fWeight_Convert_RedToGrayscale (100.0/3) //
//#define fWeight_Red 4.0 // 

//the weights must add up tp 100%
//float fWeight_Convert_RedToGrayscalef= (float)(100.0); //60); //= (float)((100.0 / 3));
	float fWeight_Convert_RedToGrayscalef= (float)(50.0); //= (float)((100.0 / 3));
	//float fWeight_Convert_RedToGrayscalef= (float)(0.0); //= (float)((100.0 / 3));

										   // The weight of Green (percents)
										   //#define fWeight_Convert_GreenToGrayscale 48.0 //80.0 //40.0 //20.0 //(100.0/2.0) 

	//float fWeight_Convert_GreenToGrayscalef= (float)(100.0); //40); //= (float)((100.0 / 3));
	float fWeight_Convert_GreenToGrayscalef= (float)(50.0); //40); //= (float)((100.0 / 3));
	//float fWeight_Convert_GreenToGrayscalef= (float)(0); //= (float)((100.0 / 3));

											 // The weight of Blue (percents)

											 //#define fWeight_Convert_BlueToGrayscale  (100.0/3) //(100.0/2.0) 
											 //#define fWeight_Convert_BlueToGrayscale 48.0 //20.0 //40.0 //(100.0/2.0) 

	float fWeight_Convert_BlueToGrayscalef= (float)(0.0); //= (float)((100.0 / 3));
	//float fWeight_Convert_BlueToGrayscalef= (float)(100); //= (float)((100.0 / 3));
	////////////////////////////////////////////////////////////////////////
	// The weight of Red (percents)

	float fWeight_Redf= (float)(10.0); //20.0); //20.0);
	//float fWeight_Redf= (float)(100.0); //20.0);
	//float fWeight_Redf= (float)(fWeight_Convert_RedToGrayscalef); //100.0);

	// The weight of Green (percents)
	float fWeight_Greenf= (float)(10.0); //20.0); //20.0); //
	//float fWeight_Greenf= (float)(100.0); //20.0); //
	//float fWeight_Greenf= (float)(fWeight_Convert_GreenToGrayscalef);

	// The weight of Blue 
	float fWeight_Bluef= (float)(0.0); // 
	//float fWeight_Bluef= (float)(100.0); //0.0); //30.0); // =
	//float fWeight_Bluef= (float)(fWeight_Convert_BlueToGrayscalef); //= (float)(100.0);
						 //////////////////////////////////////////////////////////////////

						 // The weight of Cyan (percents)
	float fWeight_Cyanf = (float)(0.0);

	// The weight of Magenta (percents)
	float fWeight_Magentaf = (float)(0.0);

	// The weight of Yellow (percents)
	float fWeight_Yellowf = (float)(0.0); // 200.0);
	//////////////////////////////////////////////////////////////////
	// The weights for Lab

	// The weight of L (percents)
	float fWeight_Lf = (float)(0.0);

	// The weight of A (percents)
	float fWeight_Af = (float)(0.0);

	// The weight of B (percents)
	float fWeight_Bf = (float)(0.0);

/////////////////////////////////////////////////////////////////
											//#define INCLUDE_EXP

						 //#define INCLUDE_PIXEL_INTENSITY_SUBSTITUTION
	bool bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf = false;

	int nPixelIntensToBeSubstituted_Minf = 50; // = 60; //80 //20 //40

	int nPixelIntensToBeSubstituted_Maxf = 140; // = 120; //80 //

	int nPixelIntensToSubstitutef = 50; // = 180; //160

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	 bINCLUDE_RGB_FROM_WIGHTED_HLS_BEFORE_PROCESSINGf
	bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf = false;

	bool bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf = false;

	float fWeightOfHuef = 50; // = 100.0;
	float fWeightOfLightnessf = 140; // = 100.0;
	float fWeightOfSaturationf = 50; // = 100.0; //0.0

								//#define nHueMin 0 //0 //10
								//#define nHueMax 60 //240 //80 //360 //80
	int nHueMinf = 300; //340
	int nHueMaxf = 360; //240 //80 //360 //80

	bool bSELECTING_COLOR_RANGES_AT_THE_READINGf = false; //true;

	bool bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf = false;

	//#define nRedMin 0 //0 //10
	//#define nRedMax 255 //
	int nRedMinf = 140; //0 //10
	int nRedMaxf = 180; //

				  //#define nGreenMin 0 //0 //10
				  //#define nGreenMax 255 //
	int nGreenMinf = 10; //0 //10
	int nGreenMaxf = 60; //

					//#define nBlueMin 0 //0 //10
					//#define nBlueMax 255 //
	int nBlueMinf = 30; //0 //10
	int nBlueMaxf = 80; //
//////////////////////////////////////////////////////////////////////////////

	int nNumOfItersFor_Unsharp_masking_3x3f = 1;
	//int nNumOfItersFor_Unsharp_masking_3x3 = 4;

	float fAmount_UnsharpMaskf = (float)(3.0); // 
	//float fAmount_UnsharpMask =  (float)(1.2); // 
	//float fAmount_UnsharpMask = - (float)(0.5); // typically, >= 0.3 and <= 0.7
	//float fAmount_UnsharpMask =  (float)(0.8); //
	//float fAmount_UnsharpMask =  (float)(0.5); // 
	//float fAmount_UnsharpMask =  (float)(0.0); // 

	float fCoefGray_UnsharpMaskf = (float)(1.0);
	//float fCoefGray_UnsharpMask = 0.2;

	//int nRadius_ForUnsharp_Mask = 3; //4;
	//int nRadius_ForUnsharp_Mask = 10;
	int nRadius_ForUnsharp_Maskf = 20;

	//int nDiffOfIntensities_Threshold_Unsharp_mask = 1;
	int nDiffOfIntensities_Threshold_Unsharp_maskf = 5;
	//int nDiffOfIntensities_Threshold_Unsharp_mask = 6;

	//int nDiffOfIntensities_Threshold_Unsharp_mask = 7;
	//int nDiffOfIntensities_Threshold_Unsharp_mask = 10;
	//int nDiffOfIntensities_Threshold_Unsharp_mask = 30;

	//float fStDev_ForUnsharp_Mask = 0.84089642; //Wiki 
	float fStDev_ForUnsharp_Maskf = (float)(nRadius_ForUnsharp_Maskf) / (float)(2.5); // fStDev_ForUnsharp_Mask ~ nRadius_ForUnsharp_Mask/3 or nRadius_ForUnsharp_Mask/2.5
	///////////////////////////////////////////////////////////////////////////
	//
	//for 'void Is_Pixel_Suitable_For_Unsharp_Mask('
	int nDist_Unsharp_maskf = 2; //<= nRadius_ForUnsharp_Mask
	int nIntensity_Threshold_Unsharp_maskf = 5;

	int nNumOfThreshViolationsMin_Unsharp_maskf = 1; //>= 1 and <= 8


//rational unsharp masking
//controlled by
//bool bRATIONAL_UNSHARPED_MASKING_IS_APPLIED 
//float fGamma = (float)(0.001);
//float fGamma = (float)(0.01);
//float fGamma = (float)(0.5);
	float fGammaf = (float)(1.0);
	//float fGamma = (float)(2.0);
	//float fGamma = (float)(5.0);
	//float fGamma = (float)(10.0);
	//float fGamma = (float)(20.0);
	//float fGamma = (float)(40.0);
	//float fGamma = (float)(100.0);
	//float fGamma = (float)(200.0);

	float fG_zerof = (float)(500.0);
	//float fG_zero = (float)(200.0);
	//float fG_zero = (float)(100.0);
	//float fG_zero = (float)(50.0);
	//float fG_zero = (float)(10.0);
	//float fG_zero = (float)(2.0);

	float fK_constf = (float)(1.0 / (2.0*fG_zerof));

	float fH_constf = fG_zerof / (float)(2.0);

	float fCoefGray_UnsharpMask_Rationf = (float)(1.0);
	//float fCoefGray_UnsharpMask_Ration =  (float)(0.5);
	//float fCoefGray_UnsharpMask_Ration =  (float)(0.2);

//median filter
// change  bIS_MEDIAN_FILTER_TO_FIN_UNSHARP_IMAGE_INCLUDEDf to 'true'!
//int nNumofIters_ForMedianFilter_After_Unsharp_Mask = 1;
//int nNumofIters_ForMedianFilter_After_Unsharp_Mask = 2;
	int nNumofIters_ForMedianFilter_After_Unsharp_Maskf = 5;

	int nRadius_MedianFilterf = 2;

	int nLenOfSquare_MedianFilterf = (2 * nRadius_MedianFilterf + 1);
	
	int nDimOfArr_MedianFilterf = nLenOfSquare_MedianFilterf * nLenOfSquare_MedianFilterf;

	int nHalf_OfDimOfArr_MedianFilterf = nDimOfArr_MedianFilterf / 2;

	bool bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKINGf = false;
	//bool bWHITE_PIXELS_FOR_EDGES_IN_UNSHARP_MASKING = true;

} PARAMETERS_IMAGE_PREPROCESSING_1;

//////////////////////////////////////////////////////////////////////
typedef struct
{
	bool bUSE_SUMS_OF_IMAGE_GRADIENTSf = true;

	bool bVERSION_2_OF_SOBEL_3x3f = true;

	bool bUSING_ONLY_BLACK_AND_WHITE_COLORSf = false;

	bool bBLACK_BACKGROUND_WHITE_CONTOURSf = false;

		bool bHIST_EQUAL_AFTER_SOBELf = false;

	int nIntensityThresholdForSobelMinf = 169;
	int nIntensityThresholdForSobelMaxf= 170;

	float fFadingFactorf = (float)(1.0);

	float fSE_Weight_For_2nd_Versionf = (float)(2.3);

	float fNormalizingf = (float)(4.0);

	float fSE_Weight_Redf = (float)(80.0);
	float fSE_Weight_Greenf = (float)(10.0);
	float fSE_Weight_Bluef = (float)(10.0);

	////////////////////////////////////////////////////////
//bool bINCLUDE_EXP_INTO_SOBEL_FILTERf = true; //Ok1//Ok2
	bool bINCLUDE_EXP_INTO_SOBEL_FILTERf = false;

	//int nThreshPixelIntensityForExp_SobelFilterf = 100; //
	//int nThreshPixelIntensityForExp_SobelFilterf = 235;
	int nThreshPixelIntensityForExp_SobelFilterf = 245;

	//float fConstForExp_SobelFilterf = (float)(0.001); 
	//float fConstForExp_SobelFilterf = (float)(0.005); //0.01 //0.02 //0.08 //0.04
	float fConstForExp_SobelFilterf = (float)(0.04); //
	//float fConstForExp_SobelFilterf = (float)(0.1); //Too white

//The Sobel filter
//bool bIS_MEDIAN_FILTER_TO_FILTERED_IMAGE_INCLUDEDf = true;
	bool bIS_MEDIAN_FILTER_TO_FILTERED_IMAGE_INCLUDEDf = false;

	int nNumofIters_ForMedianFilter_After_Filteringf = 1;
	
	int nNumofIters_ForMedianFilter_After_Unsharp_Maskf = 5;

	int nRadius_MedianFilterf = 2;

	int nLenOfSquare_MedianFilterf = (2 * nRadius_MedianFilterf + 1);


	int nDimOfArr_MedianFilterf = nLenOfSquare_MedianFilterf * nLenOfSquare_MedianFilterf;

	//int nHalf_OfDimOfArr_MedianFilterf = nDimOfArr_MedianFilterf / 2;


} PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1;
/////////////////////////////////////////////////////////


typedef struct
{
	int
		nCyan;
	int
		nMagenta;
	int
		nYellow;
	int
		nBlack;

	float
		fWeightCyan;
	float
		fWeightMagenta;
	float
		fWeightYellow;

	int
		nRed_FrWeightedCyan;

	int
		nGreen_FrWeightedMagenta;

	int
		nBlue_FrWeightedYellow;

} WEIGHTED_CMYK_COLORS;

typedef struct
{
	//no weighting for XYZ
	float
		fX;
	float
		fY;
	float
		fZ;

} XYZ_COLORS;


typedef struct
{

	float fL_FrXYZf;
	float fA_FrXYZf;
	float fB_FrXYZf;

	float
		fWeighted_L;
	float
		fWeighted_A;
	float
		fWeighted_B;

} WEIGHTED_LAB_COLORS;


typedef struct
{
	int nWidth;
	int nLength;

	float *fHue_Arr;
	float *fLightness_Arr;
	float *fSaturation_Arr;

} HLS_IMAGE;

/*
void Sobel_Filtering_3x3_With_Preprocessing_1(
	const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,
	const GRAYSCALE_IMAGE *sOne_Orig_Imagef,
	GRAYSCALE_IMAGE *sOne_Filtered_Imagef);
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The function takes the input image "image_in" and filters it according to parameters of the structure "sParameters_Sobel_3x3_With_Preprocessing_1f"
//Some image preprocessing steps could be used as well 
int doSobel_3x3_With_Preprocessing_1(

	const Image& image_in,

	const PARAMETERS_SOBEL_3x3_WITH_PREPROCESSING_1 &sParameters_Sobel_3x3_With_Preprocessing_1f,

	Image& image_out);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The function takes the input image "image_in" and transfoms it according to parameters of the structure "sParameters_Image_Preprocessing_1f"
int doImage_Preprocessing_1(
	const Image& image_in,

	const PARAMETERS_IMAGE_PREPROCESSING_1 *sParameters_Image_Preprocessing_1f,

	Image& image_out);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The function takes the input image "image_in" and produces an area of interest "AoI_image_out". 
//The coordinates of the upper left and the bottom right corners of AoI are specified by the pairs
//(nWid_AoI_UpperLeftf, nLen_AoI_UpperLeftf) and (nWid_AoI_BottomRightf,nLen_AoI_BottomRightf), respectively.

int AoI_From_AnImage(

	const Image& image_in,

	int nWid_AoI_UpperLeftf, //
	int nLen_AoI_UpperLeftf,

	int nWid_AoI_BottomRightf,
	int nLen_AoI_BottomRightf,

	//const GRAYSCALE_IMAGE *sGrayscale_Image,

	Image& AoI_image_out);